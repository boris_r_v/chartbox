#include "chartbox.h"
#include <cstdio>
#include <cstring>
#define INIT_X_VALUE 1
#define INIT_Y_VALUE -1
#if (GTK_MAJOR_VERSION == 3)
#include "portable_layer.h"
#endif

/*---CHARTBOX_LINE_CALLBACKS---*/
void chartbox_line_menu_hideLine (GtkCheckMenuItem *checkmenuitem, gpointer data )
{
    bool state = gtk_check_menu_item_get_active( checkmenuitem  );
    static_cast<chart::chartbox_line*> ( data ) -> line_show_hide( state );
}

void chartbox_line_menu_smoothLine(GtkCheckMenuItem *checkmenuitem, gpointer data )
{
    bool state = gtk_check_menu_item_get_active( checkmenuitem  );    
    static_cast<chart::chartbox_line*> ( data ) -> smooth_line( state );
}

void chartbox_line_menu_clear_data(GtkCheckMenuItem *checkmenuitem, gpointer data )
{
    static_cast<chart::chartbox_line*> ( data ) -> clear( );
}

void chartbox_line_menu_set_ma_factor(GtkCheckMenuItem *checkmenuitem, gpointer data )
{
    
    chart::chartbox_line* line = static_cast<chart::chartbox_line*> ( data );
    guint _factor = line -> get_ma_factor( );

    GtkWidget *dialog, *content_area, *spin_button;
    dialog = gtk_dialog_new_with_buttons ("Установть основание скользящего среднего",
                                         NULL,
                                         GTK_DIALOG_MODAL,
                                         "OK",
                                         GTK_RESPONSE_NONE,
                                         NULL);
    content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog) );
    spin_button = gtk_spin_button_new_with_range( 3, 1000, 1 );
    gtk_spin_button_set_value( GTK_SPIN_BUTTON(spin_button), _factor );
    gtk_widget_show( spin_button );
    gtk_container_add ( GTK_CONTAINER (content_area), spin_button );
    gint resp = gtk_dialog_run ( GTK_DIALOG (dialog) );
    _factor = gtk_spin_button_get_value( GTK_SPIN_BUTTON(spin_button) );
    gtk_widget_destroy (dialog);

    line->set_ma_factor( _factor );
}

void chartbox_line_menu_hideLimitsLines(GtkCheckMenuItem *checkmenuitem, gpointer data )
{
    bool state = gtk_check_menu_item_get_active( checkmenuitem  );    
    static_cast<chart::chartbox_line*> ( data ) -> limits_show_hide( state );
}

void chartbox_line_menu_selectLineColor(GtkMenuItem* _mi, gpointer data )
{
     static_cast<chart::chartbox_line*> ( data ) -> select_line_color( );
}

void chartbox_line_colorSelectionDialogResponse( GtkDialog* dialog, gint resp_id, gpointer data )
{
    GdkColor _color;
    GtkColorSelection *colorsel = GTK_COLOR_SELECTION ( gtk_color_selection_dialog_get_color_selection ( GTK_COLOR_SELECTION_DIALOG(dialog) ) );
    gtk_color_selection_get_current_color ( colorsel, &_color );
    gtk_widget_destroy( GTK_WIDGET( dialog ) );
    if ( resp_id == GTK_RESPONSE_OK ) static_cast<chart::chartbox_line*> ( data ) -> set_color( _color );
}

void chartbox_line_lineShowHide(GtkWidget* _w , gpointer _data)
{
    bool state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON( _w ) );
    gtk_button_set_label( GTK_BUTTON(_w), state ? "Показать" : "Скрыть"  );
    static_cast<chart::chartbox_line*> (_data ) -> line_show_hide( state );
}

void chartbox_line_eventBoxPressButton( GtkWidget *_widget, GdkEvent *_event, gpointer _data )
{
    static_cast<chart::chartbox_line*> (_data ) -> event_box_press_button ( _widget, _event );
}

void chartbox_line_limitsShowHide(GtkWidget* _w , gpointer _data)
{
    bool state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON( _w ) );
    static_cast<chart::chartbox_line*> (_data ) -> limits_show_hide( state );
}

void chartbox_line_lineSmooth(GtkWidget* _w , gpointer _data)
{
    bool state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON( _w ) );
    static_cast<chart::chartbox_line*> (_data ) -> smooth_line( state );
}

void lineValChange(GtkChartBoxGraph* _graph, gpointer _data )
{
        float val=0.0;
	val = gtk_chartbox_graph_get_value( _graph );
        gchar buff[10];
	g_snprintf(buff, sizeof(buff), "%2.3f", val);
        gtk_label_set_text(GTK_LABEL(_data), buff);
}

/*---CHARTBOX_LINE---*/
chart::chartbox_line& chart::chartbox_line::operator= ( const chart::chartbox_line&  _l )
{
    if ( this == &_l )
        return *this;
    this->~chartbox_line();
    return *new(this) chartbox_line(_l);
}
chart::chartbox_line::chartbox_line( const chart::chartbox_line& _l ):
	title( _l.title ), color_str( _l.color_str ), mark_title(_l.mark_title ), chbox( _l.chbox ), line_weigth ( _l.line_weigth ), 
	line( _l.line ), frame( _l.frame ), edit( _l.edit ), smooth( _l.smooth ), limits( _l.limits ), show_hide( _l.show_hide ), len( _l.len), X( _l.X ), Y( _l.Y ), 
	markers_len(_l.markers_len ), Ym (_l.Ym ), Xm(_l.Xm ), color(_l.color ), markers_line( _l.markers_line ), event_box( _l.event_box ), label( _l.label )
{
}

chart::chartbox_line::chartbox_line( const chartbox& _chx, const std::string& _title, const std::string& _color, unsigned _line_weigth ):
	title( _title ), color_str( _color ), chbox( _chx ), line_weigth ( _line_weigth ), line( NULL ), frame( NULL ), edit( NULL ), smooth( NULL ), limits( NULL ), show_hide( NULL ),
	len(0), X( NULL ), Y( NULL ), markers_len(0), Ym ( NULL ), Xm(0), event_box( NULL ), label ( NULL )
{
    try 
    {
	len = 1;
	X = g_new0 ( guint, len );
	Y = g_new0 ( gfloat, len );
	X[0] = INIT_X_VALUE;
	Y[0] = INIT_Y_VALUE;
    }
    catch ( std::bad_alloc& ba )    
    { 
	std::cout << "chart::chartbox_line() " << _title << " не могу выделить памать под массивы данных." << ba.what() << std::endl;
    }
    make_box ( false );
    make_line( false );    
}

chart::chartbox_line::chartbox_line( const chartbox& _chx, const FV& _markers, const std::string& _title, const std::string& _color, const std::string& _mt, unsigned _line_weigth ):
	title( _title ), color_str( _color ), mark_title( _mt ), chbox( _chx ), line_weigth ( _line_weigth ), 
	line( NULL ), frame( NULL ), edit( NULL ), smooth( NULL ), limits( NULL ), show_hide( NULL ), event_box( NULL ), label ( NULL ),
	len(0), X(NULL), Y( NULL ), markers_len(0), Ym ( NULL ), Xm( 0 )	
{
    try 
    {
	len = 1;
	X = g_new0 ( guint, len );
	Y = g_new0 ( gfloat, len );
	X[0] = INIT_X_VALUE;
	Y[0] = INIT_Y_VALUE;
	markers_len = _markers.size();
	Ym = g_new0 ( gfloat, markers_len );
	Xm = 1234657890;    
	
	for ( unsigned i = 0; i < markers_len; ++i )
	    Ym[i] = _markers[i];
	
    }
    catch ( std::bad_alloc& ba )    
    { 
	std::cout << "chart::chartbox_line() " << _title << " не могу выделить памать под массивы данных." << ba.what() << std::endl;
    }
    make_box ( true );
    make_line( true );    
}

chart::chartbox_line::chartbox_line(const chartbox& _chx, const LV& _dataX, const FV& _dataY,  const std::string& _title, const std::string& _color, unsigned _line_weigth ): 
	title( _title ), color_str( _color ), chbox( _chx ), line_weigth ( _line_weigth ), line( NULL ), frame( NULL ), edit( NULL ), smooth( NULL ), limits( NULL ), show_hide( NULL ),
	len(0), X( NULL ), Y( NULL ), markers_len(0), Ym ( NULL ), Xm( 0 ), event_box( NULL ), label ( NULL )
{
    if ( _dataX.size() !=  _dataY.size() )
    {
	std::cout << "chart::chartbox_line() разные длиння массивов данных по оси X и Y " << _title << " график не будет создан" << std::endl;
	return;
    }
    try 
    {
	len = _dataY.size();
	X = g_new0 ( guint, len );
	Y = g_new0 ( gfloat, len );
	for ( unsigned i = 0; i < len; ++i )
	{
	    X[i] = _dataX[i];
	    Y[i] = _dataY[i];
//std::cout << "i:" << i << " X[i]:" << X[i] << " Y[i]:" << Y[i] << std::endl;
	}
    }
    catch ( std::bad_alloc& ba )    
    { 
	std::cout << "chart::chartbox_line() " << _title << " не могу выделить памать под массивы данных. " << ba.what() <<  std::endl;
    }
    make_box ( false );
    make_line( false );

}

chart::chartbox_line::chartbox_line(const chartbox& _chx, const LV& _dataX, const FV& _dataY, const FV& _markers, const std::string& _title, const std::string& _color, const std::string& _mt, unsigned _line_weigth ):
	title( _title ), color_str( _color ), mark_title( _mt ), chbox( _chx ), line_weigth ( _line_weigth ), 
	line( NULL ), frame( NULL ), edit( NULL ), smooth( NULL ), limits( NULL ), show_hide( NULL ), event_box( NULL ), label ( NULL ),
	len(0), X(NULL), Y( NULL ), markers_len(0), Ym ( NULL ), Xm( 0 )	
{
    if ( _dataX.size() !=  _dataY.size() )
    {
	std::cout << "chart::chartbox_line() разные длиння массивов данных по оси X и Y " << _title << " график не будет создан" << std::endl;
	return;
    }
    try 
    {
	len = _dataY.size();
	X = g_new0 ( guint, len );
	Y = g_new0 ( gfloat, len );
	markers_len = _markers.size();
	Ym = g_new0 ( gfloat, markers_len );
	Xm = 1234657890;    
	
	for ( unsigned i = 0; i < len; ++i )
	{
	    X[i] = _dataX[i];
	    Y[i] = _dataY[i];
	}
	for ( unsigned i = 0; i < markers_len; ++i )
	    Ym[i] = _markers[i];
	
    }
    catch ( std::bad_alloc& ba )    
    { 
	std::cout << "chart::chartbox_line() " << _title << " не могу выделить памать под массивы данных." << ba.what() << std::endl;
    }
    make_box ( true );
    make_line( true );    
}

chart::chartbox_line::~chartbox_line( )
{

}

void chart::chartbox_line::destroy( ) const
{
    if ( Y ) g_free( Y );    
    if ( X ) g_free( X );        
    if ( Ym ) g_free( Ym );
}

void chart::chartbox_line::clear()
{
    len = 1;
    X = g_renew ( guint, X, len );
    Y = g_renew ( gfloat, Y,  len );
    X[0] = INIT_X_VALUE;
    Y[0] = INIT_Y_VALUE;
    gtk_chartbox_graph_set_X( line, X );
    gtk_chartbox_graph_set_Y( line, Y );
    gtk_chartbox_graph_set_length( line, len );
}

void chart::chartbox_line::make_line ( bool _b )
{
    if( !gdk_color_parse ( color_str.c_str(), &color ) )
	 gdk_color_parse ( "#000", &color );

    /*Создали GtkDataboxGraph */
    line = gtk_chartbox_lines_new(len, X, Y, &color, line_weigth );
    /*Подвязали событие обновления текущего значения графика */
    g_signal_connect(G_OBJECT( line ),"value-change", G_CALLBACK(lineValChange), edit);		


    /*Установили название величины графика*/
    gtk_chartbox_graph_set_text(GTK_CHARTBOX_GRAPH( line ), title.c_str() );    

    /*Нарисуем линии пороговых значений*/
    if ( _b )
    {
	GdkColor c;
	//gdk_color_parse("#212178", &c );
	gdk_color_parse("#ff001f", &c );
	for ( unsigned i = 0; i < markers_len; ++i )
	{
	    /*Если очень большая подпись к пороговой линии, - то она не влазиет в буфер и линиия то рисуется то нет, поэтому обрезаем ее на 15 символов*/
	    if ( mark_title.size() > 15 ) mark_title = mark_title.substr(0, 14);
	    GtkChartBoxMarker* _mark = gtk_chartbox_marker_new(&Xm, &Ym[i], &c, 12, (char*)mark_title.c_str() );
	    gtk_chartbox_marker_set_line( _mark, TRUE );
	    markers_line.push_back( _mark );
	}
    }


}

void chart::chartbox_line::connect_signals()
{
    g_signal_connect(G_OBJECT( show_hide ),"toggled", G_CALLBACK(chartbox_line_lineShowHide), this );		
    g_signal_connect(G_OBJECT( smooth ),"toggled", G_CALLBACK(chartbox_line_lineSmooth), this );		
    g_signal_connect(G_OBJECT( event_box ), "button_press_event", G_CALLBACK(chartbox_line_eventBoxPressButton), this);
    if ( limits ) g_signal_connect(G_OBJECT( limits ),"toggled", G_CALLBACK(chartbox_line_limitsShowHide), this );		
}

void chart::chartbox_line::make_box( bool _b )
{
    event_box = gtk_event_box_new();
    frame = gtk_frame_new( NULL );
    GtkWidget* lab_frame = gtk_frame_new( NULL );
    gtk_frame_set_shadow_type( GTK_FRAME( lab_frame), GTK_SHADOW_IN );
    GtkWidget* vbox = gtk_vbox_new (0, 0);
    
    /*title*/
    label = gtk_label_new( "none" );
    gchar buff[100];
    g_snprintf(buff, sizeof(buff), "<span  foreground=\"%s\" >%s</span>", color_str.c_str(), title.c_str() );
    gtk_label_set_markup (GTK_LABEL(label), buff);
    gtk_widget_set_tooltip_text( label, "Имя графика. Нажмите для открытия меню." );
    gtk_container_add ( GTK_CONTAINER( event_box ), label );
    gtk_container_add ( GTK_CONTAINER( lab_frame ), event_box );
    gtk_box_pack_start( GTK_BOX( vbox ), lab_frame, FALSE, FALSE, 0 );
    /*show_hide*/	
    show_hide = gtk_check_button_new_with_label("Скрыть");
    gtk_widget_set_tooltip_text( show_hide, "Показать/скрыть график.");
    gtk_box_pack_start( GTK_BOX( vbox), show_hide, FALSE, FALSE, 0 );
    /*smooth*/	
    smooth = gtk_check_button_new_with_label("Сгладить");
    gtk_widget_set_tooltip_text( smooth, "Сглаживание графика по алгоритму \"Скользящая средняя по основанию 5\"");
    gtk_box_pack_start( GTK_BOX( vbox), smooth, FALSE, FALSE, 0 );
    /*limit*/
    if ( _b )
    {
	limits = gtk_check_button_new_with_label("Пороги");
	gtk_widget_set_tooltip_text( limits, "Скрыть/показать пороги допустимых колебаний значений величины.");
	gtk_box_pack_start( GTK_BOX( vbox), limits, FALSE, FALSE, 0 );
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON( limits), DEFAULT_LIMITS_VISIBLE );
    }
    GtkWidget *fr = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(fr), GTK_SHADOW_OUT );
    edit = gtk_label_new ("0.0"); 
    gtk_widget_set_tooltip_text( edit, "Значение величины в позиции показаной курсором");
    gtk_container_add(GTK_CONTAINER(fr), edit );
    gtk_box_pack_start( GTK_BOX( vbox), fr, FALSE, FALSE, 0 );

    gtk_widget_show_all( vbox );    
    
    gtk_container_add ( GTK_CONTAINER( frame), vbox );
}

GtkWidget* chart::chartbox_line::get_main_box() const
{
    return frame;
}
GtkChartBoxGraph* chart::chartbox_line::get_line() const
{
    return line;
}
void chart::chartbox_line::smooth_button_set_visible ( gboolean _b )
{
    gtk_widget_set_visible( smooth, _b );
}

void chart::chartbox_line::line_show_hide ( gboolean _b )
{
    gtk_button_set_label( GTK_BUTTON(show_hide), _b ? "Показать" : "Скрыть"  );
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(show_hide), _b );

    /*скроем/покажем график*/
    gtk_chartbox_graph_set_hide( line, _b );
    /*сделаем недоступной кнопку сглаживания*/
    gtk_widget_set_sensitive( smooth, !_b );					
    /*сделаем недоступной кнопку порогов*/
    if ( limits )
    {
	gtk_widget_set_sensitive( limits, !_b );					
	//gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON( limits), FALSE );
	/*Если есть графики порогов скроем их тоже*/
	for (unsigned i = 0; i < markers_line.size(); ++i )
	    if ( gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON( limits) ) ) gtk_chartbox_marker_set_hide( markers_line[i], _b );
    }
    /*Обновим график*/
    chbox.self_update();
}
void chart::chartbox_line::limits_show_hide ( gboolean _b )
{
    if ( limits ) gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(limits), _b );
    for (unsigned i = 0; i < markers_line.size(); ++i )
    {
	gtk_chartbox_marker_set_hide( markers_line[i], !_b );
    }	
    chbox.self_update();
}
void chart::chartbox_line::smooth_line ( gboolean _b )
{
    gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(smooth), _b );
    gtk_chartbox_graph_set_smooth( line, _b );
    chbox.self_update();
}
void chart::chartbox_line::set_ma_factor ( guint _b )
{
    gtk_chartbox_graph_set_ma_factor( line, _b );
    chbox.self_update();
}
guint chart::chartbox_line::get_ma_factor ( )
{
    return gtk_chartbox_graph_get_ma_factor( line );
}

void chart::chartbox_line::event_box_press_button ( GtkWidget *widget, GdkEvent *event )
{
    if( event->type == GDK_BUTTON_PRESS )
    {
	if( event->button.button == 1) select_line_color();
    	if( event->button.button == 3) make_control_menu();
    }
}

void chart::chartbox_line::make_control_menu()
{
    GtkWidget* _menu = gtk_menu_new();
    GtkWidget* menuItem = gtk_menu_item_new_with_label( "Выбор цвета графика." );
    gtk_widget_show(menuItem);
    gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
    g_signal_connect( G_OBJECT(menuItem), "activate", G_CALLBACK( chartbox_line_menu_selectLineColor ), this );
    
    menuItem = gtk_check_menu_item_new_with_label("Скрыть график.");
    gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(menuItem), gtk_chartbox_graph_get_hide ( line ) );
    gtk_widget_show(menuItem);
    gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
    g_signal_connect( G_OBJECT(menuItem), "toggled", G_CALLBACK( chartbox_line_menu_hideLine ), this );

    menuItem = gtk_check_menu_item_new_with_label("Сгладить график.");
    gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(menuItem), gtk_chartbox_graph_get_smooth ( line ) );
    gtk_widget_show(menuItem);
    gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
    g_signal_connect( G_OBJECT(menuItem), "toggled", G_CALLBACK( chartbox_line_menu_smoothLine ), this );

    if ( limits )
    {
	menuItem = gtk_check_menu_item_new_with_label("Пороги");
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(menuItem), gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( limits ) ) );
	gtk_widget_show(menuItem);
	gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
	g_signal_connect( G_OBJECT(menuItem), "toggled", G_CALLBACK( chartbox_line_menu_hideLimitsLines ), this );
    }
    menuItem = gtk_check_menu_item_new_with_label("Установить степень сглаживания");
    gtk_widget_show(menuItem);
    gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
    g_signal_connect( G_OBJECT(menuItem), "toggled", G_CALLBACK( chartbox_line_menu_set_ma_factor ), this );

    menuItem = gtk_check_menu_item_new_with_label("Очистить данные графика");
    gtk_widget_show(menuItem);
    gtk_menu_shell_append ( GTK_MENU_SHELL(_menu),menuItem);
    g_signal_connect( G_OBJECT(menuItem), "toggled", G_CALLBACK( chartbox_line_menu_clear_data ), this );

    gtk_menu_popup(GTK_MENU(_menu),0,0,0,0,0,gtk_get_current_event_time());

}

void chart::chartbox_line::select_line_color()
{
    GtkWidget* dialog = gtk_color_selection_dialog_new("Выбор цвета линии");
    GtkColorSelection *colorsel = GTK_COLOR_SELECTION ( gtk_color_selection_dialog_get_color_selection ( GTK_COLOR_SELECTION_DIALOG(dialog) ) );
    gtk_color_selection_set_previous_color (colorsel, &color);
    gtk_color_selection_set_current_color (colorsel, &color);
    gtk_color_selection_set_has_palette (colorsel, TRUE);
    g_signal_connect (dialog, "response", G_CALLBACK (chartbox_line_colorSelectionDialogResponse), this );
    gtk_widget_show( dialog );
}

bool chart::chartbox_line::have_markers()
{
    return !markers_line.empty();
}

const std::vector<GtkChartBoxMarker*> chart::chartbox_line::get_markers()
{
    return markers_line;
}

const std::string& chart::chartbox_line::get_title() const
{
    return title;
}

bool chart::chartbox_line::add_one_point( unsigned long _x, float _y )
{
    
    if ( X[0] != INIT_X_VALUE && Y[0] != INIT_Y_VALUE ) ++len;
    X = g_renew ( guint, X, len );
    Y = g_renew ( gfloat, Y,  len );
    X[len-1] = _x;
    Y[len-1] = _y;
    gtk_chartbox_graph_set_X( line, X );
    gtk_chartbox_graph_set_Y( line, Y );
    gtk_chartbox_graph_set_length( line, len );
    
    return RET_OK;
}

bool chart::chartbox_line::add_points( unsigned long* _x, float* _y, unsigned size )
{
    X = g_renew ( guint, X, len+size );
    Y = g_renew ( gfloat, Y,  len+size );
    memcpy ( X + len, _x, size );
    memcpy ( Y + len, _y, size );
    len += size;
    gtk_chartbox_graph_set_X( line, X );
    gtk_chartbox_graph_set_Y( line, Y );
    gtk_chartbox_graph_set_length( line, len );
    
    return RET_OK;
}


void chart::chartbox_line::set_color( const GdkColor& _color )
{
    color = _color;
    gtk_chartbox_graph_set_color ( line, &color );
    chbox.self_update();
    color_str = gdk_color_to_string ( &color );
    gchar buff[100];
    g_snprintf( buff, sizeof(buff), "<span  foreground=\"%s\" >%s</span>", color_str.c_str(), gtk_label_get_text(GTK_LABEL( label ) ) );
    gtk_label_set_markup (GTK_LABEL(label), buff);
}

/*---CHARTBOX_CALLBACKS---*/
void butSaveClick( GtkButton* _button, gpointer _data )
{
	gtk_chartbox_save(GTK_CHARTBOX(_data) );
}

void butZoomInClick( GtkButton* _button, gpointer _data )
{
	gtk_chartbox_zoom_in(GTK_CHARTBOX(_data) );
}

void butZoomOutClick( GtkButton* _button, gpointer _data )
{
	gtk_chartbox_zoom_out(GTK_CHARTBOX(_data) );
}

void butZoomHomeClick( GtkButton* _button, gpointer _data )
{
	gtk_chartbox_zoom_home(GTK_CHARTBOX(_data) );
}

void butReDrawToggled ( GtkToggleButton *_togglebutton, gpointer _data )
{
    chart::chartbox* _cbx = static_cast<chart::chartbox*>( _data );
    _cbx->set_redraw_state( !gtk_toggle_button_get_active( _togglebutton ) ); 
    if ( !gtk_toggle_button_get_active( _togglebutton ) )
    {
	GtkWidget* imgReDraw = gtk_image_new_from_stock(GTK_STOCK_REFRESH, GTK_ICON_SIZE_BUTTON);
	gtk_button_set_image(GTK_BUTTON( _togglebutton ), imgReDraw );
    }
    else
    {
	GtkWidget* imgReDraw = gtk_image_new_from_stock(GTK_STOCK_STOP, GTK_ICON_SIZE_BUTTON);
	gtk_button_set_image(GTK_BUTTON( _togglebutton ), imgReDraw );
    }

}

/*---CHARTBOX---*/
chart::chartbox::chartbox( ):
  main_box( NULL ), left_box( NULL ), central_box( NULL ), right_box( NULL ), graph_box( NULL ), self_box (NULL), sibl_box( NULL ), redraw_state( true )
{
} 
chart::chartbox::~chartbox()
{
    for (std::map<std::string, chartbox_line>::const_iterator b = _in_lines.begin(); b != _in_lines.end(); ++b )
	b->second.destroy();
    _in_lines.clear();
}

chart::chartbox::chartbox( const chart::chartbox& _c ):
  main_box( _c.main_box ), left_box( _c.left_box ), central_box( _c.central_box ), right_box( _c.right_box ), graph_box( _c.graph_box ), self_box ( _c.self_box ), sibl_box( _c.sibl_box ),
  redraw_state( _c.redraw_state )
{
}

chart::chartbox& chart::chartbox::operator=(const chart::chartbox& _c )
{
    if ( &_c == this ) 
	return *this;
     main_box = _c.main_box;
     left_box = _c.left_box;
     central_box = _c.central_box;
     right_box = _c.right_box;
     graph_box = _c.graph_box;
     self_box =_c.self_box;
     sibl_box = _c.sibl_box;
     return *this;
    	
}
void chart::chartbox::init_simple( bool is_time_x_ruller )
{
    make_simple( is_time_x_ruller );
    make_buttons( false ); // - не создавать кнопку запрета перерисовки графика
} 

void chart::chartbox::init_with_stop_update_button( bool is_time_x_ruller )
{
    make_simple( is_time_x_ruller );
    make_buttons( true );  // - создать кнопку запрета перерисовки графика
}

void chart::chartbox::init_vigil( const std::string& _self_tip, const std::string& _sibl_tip ) 
{
    make_vigil( _self_tip, _sibl_tip );
    make_buttons( false ); // - не создавать кнопку запрета перерисовки графика
}

void chart::chartbox::make_simple( bool is_time_x_ruller )
{
    gtk_chartbox_create_all (&graph_box, &central_box, TRUE, TRUE, TRUE, is_time_x_ruller );
    left_box = gtk_vbox_new( 0, 0 );
    right_box = gtk_vbox_new( 0, 0 );    
    main_box = gtk_hbox_new( 0, 0 );    
}
void chart::chartbox::make_vigil( const std::string& _self_tip, const std::string& _sibl_tip )
{
    gtk_chartbox_create_vigil (&graph_box, &central_box, &self_box, &sibl_box, _self_tip.c_str(), _sibl_tip.c_str() );
    left_box = gtk_vbox_new( 0, 0 );
    right_box = gtk_vbox_new( 0, 0 );    
    main_box = gtk_hbox_new( 0, 0 );    

    gchar buff[300];
    g_snprintf( buff, sizeof(buff), "Моменты занятия/освобождения секции %s", _self_tip.c_str() );
    gtk_widget_set_tooltip_text(self_box, buff );
    g_snprintf( buff, sizeof(buff), "Моменты занятия/освобождения секции %s", _sibl_tip.c_str() );
    gtk_widget_set_tooltip_text(sibl_box, buff );
}

GtkWidget* chart::chartbox::get_main_box()
{
    gtk_box_pack_start (GTK_BOX ( main_box ), left_box, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX ( main_box ), central_box, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX ( main_box ), right_box, FALSE, FALSE, 0);
    return main_box;
}

void chart::chartbox::make_buttons( bool need_redraw_button )
{
    GtkWidget* butSave = gtk_button_new();
    GtkWidget* imgSave = gtk_image_new_from_stock(GTK_STOCK_SAVE, GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(butSave), imgSave);
    gtk_widget_set_tooltip_text(butSave, "Сохранить график в файл.");
    g_signal_connect(G_OBJECT(butSave),"clicked", G_CALLBACK(butSaveClick), graph_box );

	/*Создаем кнопку "Увеличить" */
    GtkWidget* butZoomIn = gtk_button_new();
    GtkWidget* imgZoomIn = gtk_image_new_from_stock(GTK_STOCK_ZOOM_IN, GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(butZoomIn), imgZoomIn);
    gtk_widget_set_tooltip_text(butZoomIn, "Увеличить график.");
    g_signal_connect(G_OBJECT(butZoomIn),"clicked", G_CALLBACK(butZoomInClick), graph_box );

	/*Создаем кнопку "Уменьшить" */
    GtkWidget* butZoomOut = gtk_button_new();
    GtkWidget* imgZoomOut = gtk_image_new_from_stock(GTK_STOCK_ZOOM_OUT, GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(butZoomOut), imgZoomOut);
    gtk_widget_set_tooltip_text(butZoomOut, "Уменьшить график.");
    g_signal_connect(G_OBJECT(butZoomOut),"clicked", G_CALLBACK(butZoomOutClick), graph_box );

	/*Создаем кнопку к истиному размеру */
    GtkWidget* butZoomHome = gtk_button_new();
    GtkWidget* imgZoomHome = gtk_image_new_from_stock(GTK_STOCK_ZOOM_100, GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(butZoomHome), imgZoomHome);
    gtk_widget_set_tooltip_text(butZoomHome, "К истинному размеру.");
    g_signal_connect(G_OBJECT(butZoomHome),"clicked", G_CALLBACK(butZoomHomeClick), graph_box );
    
	/*Создадим кнопку запрета/рахрешения перерисовки графика*/
    GtkWidget* butReDraw = NULL;
    if ( need_redraw_button )
    {
	butReDraw = gtk_toggle_button_new();
	GtkWidget* imgReDraw = gtk_image_new_from_stock(GTK_STOCK_REFRESH, GTK_ICON_SIZE_BUTTON);
	gtk_button_set_image(GTK_BUTTON( butReDraw ), imgReDraw );
	gtk_widget_set_tooltip_text( butReDraw, "Разрешить/запретить обновлять графики.");
	g_signal_connect(G_OBJECT( butReDraw ),"toggled", G_CALLBACK(butReDrawToggled), this );
    }	
	/*Запаковываем кнопки в контейнер*/
    gtk_box_pack_start (GTK_BOX (right_box), butZoomIn, FALSE, FALSE, 1);
    gtk_box_pack_start (GTK_BOX (right_box), butZoomOut, FALSE, FALSE, 1);
    gtk_box_pack_start (GTK_BOX (right_box), butZoomHome, FALSE, FALSE, 2);
    gtk_box_pack_start (GTK_BOX (right_box), gtk_hseparator_new (), FALSE, FALSE, 2);
    gtk_box_pack_start (GTK_BOX (right_box), butSave, FALSE, FALSE, 2);
    gtk_box_pack_start (GTK_BOX (right_box), gtk_hseparator_new (), FALSE, FALSE, 2);
    if ( butReDraw ) gtk_box_pack_start (GTK_BOX (right_box), butReDraw, FALSE, FALSE, 2);
}

void chart::chartbox::set_store_path( std::string const&  _s)
{
    gtk_chartbox_set_store_path(GTK_CHARTBOX( graph_box ), _s.c_str() );
}

void chart::chartbox::set_label_text( std::string const& _s )
{
    if( !_s.empty() ) gtk_chartbox_set_label_text( GTK_CHARTBOX( graph_box ), _s.c_str() );
}

void chart::chartbox::update() const 
{
    if ( redraw_state ) gtk_chartbox_update( GTK_CHARTBOX( graph_box ) );
}

void chart::chartbox::self_update() const 
{
    gtk_chartbox_update( GTK_CHARTBOX( graph_box ) );
}

bool chart::chartbox::has_line( const std::string& _name )
{
    return _in_lines.find( _name ) != _in_lines.end();
}

std::map<std::string, chart::chartbox_line>& chart::chartbox::get( )
{
    return _in_lines;
}


void chart::chartbox::add_line( const chartbox_line& _line)
{
    _in_lines.insert( std::pair<std::string, chartbox_line>(_line.get_title(), _line ) );
    chartbox_line& graph = _in_lines.find( _line.get_title() )->second;
    gtk_box_pack_start (GTK_BOX ( left_box ), graph.get_main_box(), FALSE, FALSE, 0);
    gtk_chartbox_graph_add ( GTK_CHARTBOX ( graph_box ), graph.get_line() );
    graph.connect_signals();

    if ( graph.have_markers() )
        for( unsigned i = 0; i <graph.get_markers().size(); ++i )
        {
	    gtk_chartbox_marker_add(GTK_CHARTBOX( graph_box ), graph.get_markers()[i] );
	    gtk_chartbox_marker_set_hide( graph.get_markers()[i], TRUE );		    
	}
}

void chart::chartbox::add_self_line( const chartbox_line& _line )
{
    if (!self_box) return;
    _in_lines.insert( std::pair<std::string, chartbox_line>(_line.get_title(), _line ) );
    chartbox_line& graph = _in_lines.find( _line.get_title() )->second;
    gtk_chartbox_graph_add ( GTK_CHARTBOX ( self_box ), graph.get_line() );
}

void chart::chartbox::add_sibl_line( const chartbox_line& _line )
{
    if (!sibl_box) return;
    //_in_lines[_line.get_title()] = _line;
    _in_lines.insert( std::pair<std::string, chartbox_line>(_line.get_title(), _line ) );
    chartbox_line& graph = _in_lines.find( _line.get_title() )->second;
    gtk_chartbox_graph_add ( GTK_CHARTBOX ( sibl_box ), graph.get_line() );
}

bool chart::chartbox::add_one_point_to_line( const std::string& _title, unsigned long _x, float _y )
{
    std::map<std::string, chartbox_line>::iterator fnd = _in_lines.find ( _title );
    if ( fnd == _in_lines.end() ) return RET_ERROR;
    return fnd->second.add_one_point( _x, _y );
}
bool chart::chartbox::add_points_to_line( const std::string& _title, unsigned long* _x, float* _y, unsigned _size )
{
    std::map<std::string, chartbox_line>::iterator fnd = _in_lines.find ( _title );
    if ( fnd == _in_lines.end() ) return RET_ERROR;
    return fnd->second.add_points( _x, _y, _size );
}

bool chart::chartbox::add_one_point_to_line_update_chart( const std::string& _title, unsigned long _x, float _y )
{
    std::map<std::string, chartbox_line>::iterator fnd = _in_lines.find ( _title );
    if ( fnd == _in_lines.end() ) return RET_ERROR;
    bool ret = fnd->second.add_one_point( _x, _y );
    if ( ret == RET_OK ) update();
    return ret;
}

void chart::chartbox::set_redraw_state( bool _b )
{
    redraw_state = _b;
}

void chart::chartbox::set_negative_value_flag( bool _nv)
{
    gtk_chartbox_set_negative_value( GTK_CHARTBOX( graph_box ),  _nv );
}

bool chart::chartbox::get_negative_value_flag( )
{
    return gtk_chartbox_get_negative_value( GTK_CHARTBOX( graph_box ) );
}

void chart::chartbox::clear()
{
    for ( std::map<std::string, chartbox_line>::iterator b = _in_lines.begin( ); b != _in_lines.end(); ++b )
	b->second.clear();
    self_update();
}

void chart::chartbox::lines_smooth_buttons_set_visible ( bool _b )
{
    for ( std::map<std::string, chartbox_line>::iterator b = _in_lines.begin( ); b != _in_lines.end(); ++b )
	b->second.smooth_button_set_visible( _b );
}

