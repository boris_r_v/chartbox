#ifndef __GTK_CHARTBOX_BARS_H__
#define __GTK_CHARTBOX_BARS_H__

#include "gtkchartbox_graph.h"

G_BEGIN_DECLS
#define GTK_CHARTBOX_TYPE_BARS		  (gtk_chartbox_bars_get_type ())
#define GTK_CHARTBOX_BARS(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_BARS, GtkChartBoxBars))
#define GTK_CHARTBOX_BARS_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass),  GTK_CHARTBOX_TYPE_BARS, GtkChartBoxBarsClass))
#define GTK_IS_CHARTBOX_BARS(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_CHARTBOX_TYPE_BARS ))
#define GTK_IS_CHARTBOX_BARS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_CHARTBOX_TYPE_BARS))
#define GTK_CHARTBOX_BARS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE_BARS, GtkChartBoxBarsClass))

   typedef struct _GtkChartBoxBars GtkChartBoxBars;

   typedef struct _GtkChartBoxBarsClass GtkChartBoxBarsClass;

   typedef struct _GtkChartBoxBarsPrivate GtkChartBoxBarsPrivate;

   struct _GtkChartBoxBars
   {
      /*< private >*/
      GtkChartBoxGraph parent;

      GtkChartBoxBarsPrivate *priv;
   };

   struct _GtkChartBoxBarsClass
   {
      GtkChartBoxGraphClass parent_class;
   };

   GType gtk_chartbox_bars_get_type (void);

   GtkChartBoxGraph *gtk_chartbox_bars_new (guint len, guint * X, gfloat * Y, GdkColor * color, guint size);

G_END_DECLS
#endif				/* __GTK_CHARTBOX_BARS_H__ */
