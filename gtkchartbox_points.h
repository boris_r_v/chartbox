#ifndef __GTK_CHARTBOX_POINTS_H__
#define __GTK_CHARTBOX_POINTS_H__

#include "gtkchartbox_graph.h"

G_BEGIN_DECLS
#define GTK_CHARTBOX_TYPE_POINTS		  (gtk_chartbox_points_get_type ())
#define GTK_CHARTBOX_POINTS(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_POINTS, GtkChartBoxPoints))
#define GTK_CHARTBOX_POINTS_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass),  GTK_CHARTBOX_TYPE_POINTS, GtkChartBoxPointsClass))
#define GTK_IS_CHARTBOX_POINTS(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_CHARTBOX_TYPE_POINTS ))
#define GTK_IS_CHARTBOX_POINTS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_CHARTBOX_TYPE_POINTS))
#define GTK_CHARTBOX_POINTS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE_POINTS, GtkChartBoxPointsClass))

   typedef struct _GtkChartBoxPoints GtkChartBoxPoints;

   typedef struct _GtkChartBoxPointsClass GtkChartBoxPointsClass;

   typedef struct _GtkChartBoxPointsPrivate GtkChartBoxPointsPrivate;

   struct _GtkChartBoxPoints
   {
      /*< private >*/
      GtkChartBoxGraph parent;

      GtkChartBoxPointsPrivate *priv;
   };

   struct _GtkChartBoxPointsClass
   {
      GtkChartBoxGraphClass parent_class;
   };

   GType gtk_chartbox_points_get_type (void);

   GtkChartBoxGraph *gtk_chartbox_points_new (guint len, guint * X, gfloat * Y, GdkColor * color, guint size);

G_END_DECLS
#endif				/* __GTK_CHARTBOX_POINTS_H__ */
