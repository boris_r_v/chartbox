#ifndef __GTK_CHARTBOX_GRAPH_H__
#define __GTK_CHARTBOX_GRAPH_H__

#include "gtkchartbox.h"

G_BEGIN_DECLS
#define GTK_CHARTBOX_TYPE_GRAPH		  (gtk_chartbox_graph_get_type ())
#define GTK_CHARTBOX_GRAPH(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_GRAPH, GtkChartBoxGraph))
#define GTK_CHARTBOX_GRAPH_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_CHARTBOX_TYPE_GRAPH, GtkChartBoxGraphClass))
#define GTK_IS_CHARTBOX_GRAPH(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_CHARTBOX_TYPE_GRAPH))
#define GTK_IS_CHARTBOX_GRAPH_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_CHARTBOX_TYPE_GRAPH))
#define GTK_CHARTBOX_GRAPH_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE_GRAPH, GtkChartBoxGraphClass))

typedef struct _GtkChartBoxGraphClass GtkChartBoxGraphClass;
typedef struct _GtkChartBoxGraphPrivate GtkChartBoxGraphPrivate;

struct _GtkChartBoxGraph
   {
      /*< private >*/
      GObject parent;

      GtkChartBoxGraphPrivate *priv;
   };

struct _GtkChartBoxGraphClass
   {
      GObjectClass parent_class;

      /*
       * public virtual drawing function 
       */
	void (*draw) (GtkChartBoxGraph * graph, GtkChartBox * box);

	gint (*calculate_extrema) (GtkChartBoxGraph * graph, guint * min_x, guint * max_x, gfloat * min_y, gfloat * max_y);
	gint (*calculate_value) (GtkChartBoxGraph * graph, guint * time, gfloat *result);
	void (*valued) (GtkChartBoxGraph * graph);
   };
    GType gtk_chartbox_graph_get_type (void);
    void gtk_chartbox_graph_set_ma_factor (GtkChartBoxGraph * graph, guint ma_factor);
    guint gtk_chartbox_graph_get_ma_factor (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_smooth (GtkChartBoxGraph * graph, gboolean smooth);
    gboolean gtk_chartbox_graph_get_smooth (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_hide (GtkChartBoxGraph * graph, gboolean hide);
    gboolean gtk_chartbox_graph_get_hide (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_color (GtkChartBoxGraph * graph, GdkColor * color);
    GdkColor *gtk_chartbox_graph_get_color (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_size (GtkChartBoxGraph * graph, guint size);
    gint gtk_chartbox_graph_get_size (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_length (GtkChartBoxGraph * graph, guint len);
    guint gtk_chartbox_graph_get_length (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_X (GtkChartBoxGraph * graph, guint * X);
    guint *gtk_chartbox_graph_get_X (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_Y (GtkChartBoxGraph * graph, gfloat * Y);
    gfloat *gtk_chartbox_graph_get_Y (GtkChartBoxGraph * graph);

    void gtk_chartbox_graph_set_value (GtkChartBoxGraph * graph, gfloat *value);    
    gfloat gtk_chartbox_graph_get_value (GtkChartBoxGraph * graph );

    void gtk_chartbox_graph_set_text (GtkChartBoxGraph * graph, const gchar *text);    
    const gchar* gtk_chartbox_graph_get_text (GtkChartBoxGraph * graph );

    void gtk_chartbox_graph_set_together (GtkChartBoxGraph * graph, gboolean tog);    
    gboolean gtk_chartbox_graph_get_together (GtkChartBoxGraph * graph);
    
    gint gtk_chartbox_graph_calculate_extrema (GtkChartBoxGraph * graph,
					     guint * min_x, guint * max_x,
					     gfloat * min_y, gfloat * max_y);
    gint gtk_chartbox_graph_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat* result );
    
   /* This function is called by GtkChartBox */
    void gtk_chartbox_graph_draw (GtkChartBoxGraph * graph, GtkChartBox * box);


G_END_DECLS
#endif				/* __GTK_CHARTBOX_GRAPH_H__ */
