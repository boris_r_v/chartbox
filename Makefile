LINKc.shared = $(LINK.c) -shared -w
LINKcpp.shared = $(LINK.cpp) -shared -w
GTK_VER=3

TARGET_LIB = libgtkchartbox.so
TARGET_OBJECTS = gtkchartbox.o gtkchartbox_ruler.o gtkchartbox_graph.o gtkchartbox_lines.o gtkchartbox_points.o gtkchartbox_bars.o gtkchartbox_marker.o gtkchartbox_marshal.o

TARGET_CFFLAGS = ` pkg-config --cflags gtk+-$(GTK_VER).0 `
TARGET_LDFLAGS = `pkg-config --libs gtk+-$(GTK_VER).0 pkg-config --libs gthread-2.0`

DEPREC=-w -Wdeprecated-declarations
CPPFLAGS = -I. -DPREFIX=$(PREFIX) $(TARGET_CFFLAGS)
CXXFLAGS = -w -fpic
CFLAGS =  -w -fpic -I./  $(TARGET_CFFLAGS)

#CFLAGS+="-DGTK_DISABLE_SINGLE_INCLUDES"
#CFLAGS+="-DGDK_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED"
#CFLAGS+="-DGSEAL_ENABLE"

RUNNER = libgtkchartbox-test
RUNNER_OBJECTS = main.o
RUNNER_LDFLAGS = -L. -lgtkchartbox `pkg-config --libs gtk+-$(GTK_VER).0`

TARGETMM_LIB = libchartbox.so
TARGETMM_OBJECTS = chartbox.o 
TARGETMM_LDFLAGS = -L. -lgtkchartbox `pkg-config --libs gtk+-$(GTK_VER).0`

RUNNERMM = libchartbox-test
RUNNERMM_OBJECTS = main1.o
RUNNERMM_LDFLAGS = -L. -lchartbox -lgtkchartbox `pkg-config --libs gtk+-$(GTK_VER).0` 

all: $(TARGET_LIB) $(TARGETMM_LIB) $(RUNNER)  #$(RUNNERMM)

$(TARGET_LIB): $(TARGET_OBJECTS)
	$(LINKc.shared)  $(TARGET_OBJECTS)  -o $(TARGET_LIB) $(TARGET_LDFLAGS)

$(RUNNER): $(TARGET_LIB) $(RUNNER_OBJECTS)
	$(LINK.c) $(RUNNER_OBJECTS) -o $(RUNNER) $(RUNNER_LDFLAGS)

$(TARGETMM_LIB): $(TARGET_LIB) $(TARGETMM_OBJECTS)
	$(LINKcpp.shared)  $(TARGETMM_OBJECTS) -o $(TARGETMM_LIB)  $(TARGETMM_LDFLAGS)

#$(TARGETMM_LIB): $(TARGET_OBJECTS) $(TARGETMM_OBJECTS)
#	$(LINKcpp.shared)  $(TARGETMM_OBJECTS) $(TARGET_OBJECTS) -o $(TARGETMM_LIB) #$(TARGET_LDFLAGS)

$(RUNNERMM): $(TARGETMM_LIB) $(RUNNERMM_OBJECTS)
	$(LINK.cpp) $(RUNNERMM_OBJECTS)  -o $(RUNNERMM)  $(RUNNERMM_LDFLAGS)

OBJECTS = $(TARGET_OBJECTS) $(RUNNER_OBJECTS) $(TARGETMM_OBJECTS) $(RUNNERMM_OBJECTS)
TARGETS = $(TARGET_LIB) $(RUNNER) $(TARGETMM_LIB) $(RUNNERMM)

clean:
	@rm -fv $(OBJECTS) $(TARGETS)


PREFIX=/usr/local
INCDIR=$(PREFIX)/include/gtkchartbox
LIBDIR=$(PREFIX)/lib

install: all
	@echo "--- Make dirs ---"
	@[ -d $(INCDIR) ] || sudo mkdir -pv $(INCDIR)
	@sudo chown $(USER):users $(INCDIR)
	@[ -d $(LIBDIR) ] || sudo mkdir -pv $(LIBDIR)
	@[ -d $(CRTC_INCDIR) ] || sudo mkdir -pv $(CRTC_INCDIR)
	@sudo chown $(USER):users $(CRTC_INCDIR)
	@[ -d $(CRTC_LIBDIR) ] || sudo mkdir -pv $(CRTC_LIBDIR)
	@sudo chown $(USER):users $(CRTC_LIBDIR)
	@echo "--- Copy into system ---"
	@cp -v *.h $(INCDIR)
	@cp -v $(TARGET_LIB) $(LIBDIR)
	@cp -v $(TARGETMM_LIB) $(LIBDIR)
	@sudo cp -vf ld.so.cbx.conf /etc/ld.so.conf.d/gtkchartbox.conf
	@sudo cp -vf *.pc /usr/lib/pkgconfig

remake: clean all install

CRTC_LIBDIR = /usr/lib/crtc/modules
CRTC_INCDIR = /usr/include/crtc/modules/Amt/gtkchartbox/
install_crtc: all
	@echo "--- Copy for crtc purpuse ---"
	@cp -v *.h $(CRTC_INCDIR)
	@cp -v $(TARGET_LIB) $(CRTC_LIBDIR)
	@cp -v $(TARGETMM_LIB) $(CRTC_LIBDIR)


