#include "gtkchartbox_graph.h"
#include "gtkchartbox_marshal.h"
static void gtk_chartbox_graph_real_draw (GtkChartBoxGraph * graph, GtkChartBox * draw);
static gint gtk_chartbox_graph_real_calculate_extrema (GtkChartBoxGraph * graph, guint * min_x, guint * max_x, gfloat * min_y,gfloat * max_y);
static gint gtk_chartbox_graph_real_calculate_value (GtkChartBoxGraph * graph, guint * time, gfloat *result);
static void gtk_chartbox_graph_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);
static void gtk_chartbox_graph_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);

enum
{
   GRAPH_COLOR = 1,
   GRAPH_SIZE,
   GRAPH_HIDE,
   PROP_X,
   PROP_Y,
   PROP_LEN
   
   
};
enum
{
    VALUE_CHANGE,
    LAST_SIGNAL
};
static gint gtk_chartbox_graph_signals[LAST_SIGNAL] = { 0 };
struct _GtkChartBoxGraphPrivate
{
    GdkColor color;
    guint size;
    gboolean hide;
    guint len;
    guint *X;
    gfloat *Y;
    gfloat value;
    const gchar *text;
    gboolean together; 
    gboolean smooth; 
    guint ma_factor;
};

static gpointer parent_class = NULL;

static void
graph_finalize (GObject * object){

   GtkChartBoxGraph *graph = GTK_CHARTBOX_GRAPH (object );

   g_free (graph->priv->X);
   g_free (graph->priv->Y);
   g_free (graph->priv);
   
   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_chartbox_graph_class_init (gpointer g_class )
{
   GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
   GtkChartBoxGraphClass *klass = GTK_CHARTBOX_GRAPH_CLASS (g_class);
   GParamSpec *graph_param_spec;

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->set_property = gtk_chartbox_graph_set_property;
   gobject_class->get_property = gtk_chartbox_graph_get_property;
   gobject_class->finalize = graph_finalize;

   graph_param_spec = g_param_spec_pointer ("color",
					    "Graph color",
					    "Color of graph",
					    G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    GRAPH_COLOR, graph_param_spec);

   graph_param_spec = g_param_spec_int ("size", "Graph size", "Size of displayed items", G_MININT, G_MAXINT, 0,	/* default value */
					G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    GRAPH_SIZE, graph_param_spec);

   graph_param_spec = g_param_spec_boolean ("hide", "Graph hidden", "Determine if graph is hidden or not", FALSE,	/* default value */
					    G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    GRAPH_SIZE, graph_param_spec);

   graph_param_spec = g_param_spec_pointer ("X-Values",
						"X coordinates",
						"X values of data",
						G_PARAM_CONSTRUCT_ONLY |
						G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    PROP_X, graph_param_spec);

   
   graph_param_spec = g_param_spec_pointer ("Y-Values",
						"Y coordinates",
						"Y values of data",
						G_PARAM_CONSTRUCT_ONLY |
						G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    PROP_Y, graph_param_spec);

   graph_param_spec = g_param_spec_int ("length", "length of X and Y", "number of data points", G_MININT, G_MAXINT, 0,	/* default value */
					    G_PARAM_CONSTRUCT_ONLY |
					    G_PARAM_READWRITE);

   g_object_class_install_property (gobject_class,
				    PROP_LEN, graph_param_spec);

   klass->draw = gtk_chartbox_graph_real_draw;
   klass->calculate_extrema = gtk_chartbox_graph_real_calculate_extrema;
   klass->calculate_value = gtk_chartbox_graph_real_calculate_value;   

/*--------------------------------Signal description-------------------------------*/
   gtk_chartbox_graph_signals[VALUE_CHANGE] = 
      g_signal_new ("value-change", 
            G_TYPE_FROM_CLASS (gobject_class), 
            G_SIGNAL_RUN_FIRST, 
            G_STRUCT_OFFSET (GtkChartBoxGraphClass, valued), 
            NULL,	/* accumulator */
	    NULL,	/* accumulator_data */
	    gtk_chartbox_marshal_VOID__VOID,
	    G_TYPE_NONE, 0);
    klass->valued = NULL;
}

static void
gtk_chartbox_graph_instance_init (GTypeInstance * instance
				 /*, gpointer g_class */ )
{
   GtkChartBoxGraph *graph = GTK_CHARTBOX_GRAPH (instance);

   graph->priv = g_new0 (GtkChartBoxGraphPrivate, 1);
   graph->priv->together = FALSE;
   graph->priv->smooth = FALSE;   
   graph->priv->ma_factor = 5;   
    
}

GType
gtk_chartbox_graph_get_type (void)
{
   static GType type = 0;

   if (type == 0)
   {
      static const GTypeInfo info = {
	 sizeof (GtkChartBoxGraphClass),
	 NULL,			/* base_init */
	 NULL,			/* base_finalize */
	 (GClassInitFunc) gtk_chartbox_graph_class_init,	/* class_init */
	 NULL,			/* class_finalize */
	 NULL,			/* class_data */
	 sizeof (GtkChartBoxGraph),	/* instance_size */
	 0,			/* n_preallocs */
	 (GInstanceInitFunc) gtk_chartbox_graph_instance_init,	/* instance_init */
	 NULL,			/* value_table */
      };
      type = g_type_register_static (G_TYPE_OBJECT,
				     "GtkChartBoxGraph", &info, 0);
   }

   return type;
}
/*----------------Setters ----------------*/
void 
gtk_chartbox_graph_set_ma_factor (GtkChartBoxGraph * graph, guint ma_factor){
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   graph->priv->ma_factor = ma_factor;
}
void 
gtk_chartbox_graph_set_smooth (GtkChartBoxGraph * graph, gboolean smooth){
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   graph->priv->smooth = smooth;
}
void 
gtk_chartbox_graph_set_together (GtkChartBoxGraph * graph, gboolean tog){
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   graph->priv->together = tog;
}
void
gtk_chartbox_graph_set_text (GtkChartBoxGraph * graph, const gchar  *text)
{
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   g_return_if_fail (text);
   graph->priv->text = text;
}

void
gtk_chartbox_graph_set_X (GtkChartBoxGraph * graph, guint * X)
{
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   g_return_if_fail (X);

   graph->priv->X = X;

   g_object_notify (G_OBJECT (graph), "X-Values");
}
void
gtk_chartbox_graph_set_Y (GtkChartBoxGraph * graph, gfloat * Y)
{
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH(graph));
   g_return_if_fail (Y);

   graph->priv->Y = Y;

   g_object_notify (G_OBJECT (graph), "Y-Values");
}

void
gtk_chartbox_graph_set_length (GtkChartBoxGraph * graph, guint len)
{
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH (graph));
   g_return_if_fail (len > 0);

   graph->priv->len = len;

   g_object_notify (G_OBJECT (graph), "length");
}

void
gtk_chartbox_graph_set_value (GtkChartBoxGraph * graph, gfloat *value)
{
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH (graph));

   graph->priv->value = *value;

   g_signal_emit (G_OBJECT (graph), gtk_chartbox_graph_signals[VALUE_CHANGE], 0 );

}

void 
gtk_chartbox_graph_set_hide (GtkChartBoxGraph * graph, gboolean hide){
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH (graph));
   graph->priv->hide =  hide;
   g_object_notify (G_OBJECT (graph), "hide");

}


void 
gtk_chartbox_graph_set_color (GtkChartBoxGraph * graph, GdkColor * color){
    g_return_if_fail (GTK_IS_CHARTBOX_GRAPH (graph));
    graph->priv->color = *color;
    g_object_notify (G_OBJECT (graph), "color");

}


void
gtk_chartbox_graph_set_size (GtkChartBoxGraph * graph, guint size){
   g_return_if_fail (GTK_IS_CHARTBOX_GRAPH (graph));
   graph->priv->size =  size;
   g_object_notify (G_OBJECT (graph), "size");

}

static void
gtk_chartbox_graph_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec){
   GtkChartBoxGraph *graph = GTK_CHARTBOX_GRAPH (object);

   switch (property_id)
   {
   case GRAPH_COLOR:
      {
	 gtk_chartbox_graph_set_color (graph,
				      (GdkColor *)
				      g_value_get_pointer (value));
      }
      break;
   case GRAPH_SIZE:
      {
	 gtk_chartbox_graph_set_size (graph, g_value_get_int (value));
      }
      break;
   case GRAPH_HIDE:
      {
	 gtk_chartbox_graph_set_hide (graph, g_value_get_boolean (value));
      }
      break;
   case PROP_X:
      {
	 gtk_chartbox_graph_set_X (graph,
				      (guint *) g_value_get_pointer (value));
      }
      break;
   case PROP_Y:
      {
	 gtk_chartbox_graph_set_Y (graph,
				      (gfloat *) g_value_get_pointer (value));
      }
      break;
   case PROP_LEN:
      {
	 gtk_chartbox_graph_set_length (graph,
					   g_value_get_int (value));
      }
      break;
   default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
   }
}
/* ---------------------- Getters -------------------------------------*/
const gchar *
gtk_chartbox_graph_get_text (GtkChartBoxGraph *graph)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), NULL);
   return graph->priv->text;
}

gfloat
gtk_chartbox_graph_get_value (GtkChartBoxGraph * graph )
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);

   return graph->priv->value;

}

guint *
gtk_chartbox_graph_get_X (GtkChartBoxGraph * graph)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), NULL);

   return graph->priv->X;

}

gfloat *
gtk_chartbox_graph_get_Y (GtkChartBoxGraph * graph)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), NULL);

   return graph->priv->Y;
}

guint
gtk_chartbox_graph_get_length (GtkChartBoxGraph * graph)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), 0);

   return graph->priv->len;
}

GdkColor *
gtk_chartbox_graph_get_color (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), NULL);
    return &graph->priv->color;
}

gboolean 
gtk_chartbox_graph_get_hide (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
    return graph->priv->hide;
}

gboolean 
gtk_chartbox_graph_get_together (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
    return graph->priv->together;
}

gboolean 
gtk_chartbox_graph_get_smooth (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
    return graph->priv->smooth;
}

guint 
gtk_chartbox_graph_get_ma_factor (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
    return graph->priv->ma_factor;
}

gint
gtk_chartbox_graph_get_size (GtkChartBoxGraph * graph){
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
    return graph->priv->size;
}

static void
gtk_chartbox_graph_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec){
   GtkChartBoxGraph *graph = GTK_CHARTBOX_GRAPH (object);

   switch (property_id)
   {
   case GRAPH_COLOR:
      {
	 g_value_set_pointer (value, gtk_chartbox_graph_get_color (graph));
      }
      break;
   case GRAPH_SIZE:
      {
	 g_value_set_int (value, gtk_chartbox_graph_get_size (graph));
      }
      break;
   case GRAPH_HIDE:
      {
	 g_value_set_boolean (value, gtk_chartbox_graph_get_hide (graph));
      }
      break;
   case PROP_X:
      {
	 g_value_set_pointer (value, gtk_chartbox_graph_get_X (graph));
      }
      break;
   case PROP_Y:
      {
	 g_value_set_pointer (value, gtk_chartbox_graph_get_Y (graph));
      }
      break;
   case PROP_LEN:
      {
	 g_value_set_int (value, gtk_chartbox_graph_get_length (graph) );
      }
   default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
   }
}




gint 
gtk_chartbox_graph_calculate_extrema (GtkChartBoxGraph * graph, guint * min_x, guint * max_x, gfloat * min_y, gfloat * max_y){
   return GTK_CHARTBOX_GRAPH_GET_CLASS (graph)-> calculate_extrema (graph, min_x, max_x, min_y,  max_y);

}

gint 
gtk_chartbox_graph_calculate_value (GtkChartBoxGraph * graph, guint * time, gfloat *result){
   return GTK_CHARTBOX_GRAPH_GET_CLASS (graph)-> calculate_value (graph, time, result);

}
					     

  /* This function is called by GtkChartBox */
void gtk_chartbox_graph_draw (GtkChartBoxGraph * graph, GtkChartBox * box){
     if (!graph->priv->hide)
	 GTK_CHARTBOX_GRAPH_GET_CLASS (graph)->draw (graph, box);

}

static gint
gtk_chartbox_graph_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result ){
    g_return_val_if_fail (graph, -1);
    g_return_val_if_fail (time, -1);
    *time = 0;
    *result = 0.0;
    return 0;
}

static void
gtk_chartbox_graph_real_draw (GtkChartBoxGraph * graph,
			     GtkChartBox* box)
{
   g_return_if_fail (graph);
   g_return_if_fail (box);

   return;
}


static gint
gtk_chartbox_graph_real_calculate_extrema (GtkChartBoxGraph * graph,
					  guint * min_x, guint * max_x,
					  gfloat * min_y, gfloat * max_y)
{
   guint i;
   g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);
   g_return_val_if_fail (min_x, -1);
   g_return_val_if_fail (max_x, -1);
   g_return_val_if_fail (min_y, -1);
   g_return_val_if_fail (max_y, -1);
   g_return_val_if_fail (graph->priv->len, -1);
    /*если данные в этот график еще не были загружены то не надо учитывать его при расчете экстремм*/    
   if (graph->priv->len == 1 ) return -1;     

   *min_x = *max_x = graph->priv->X[0];
   *max_y = *min_y = graph->priv->Y[0];
/*  Если раскоментить, то ось значений трисуется с НУля, и очень узкий диапазон для отображения больших величин получитья 
    *min_y = 0.0;*/
   for (i = 1; i < graph->priv->len; ++i)
   {
      if (graph->priv->X[i] < *min_x)
	 *min_x = graph->priv->X[i];
      else if (graph->priv->X[i] > *max_x)
	 *max_x = graph->priv->X[i];
      if (graph->priv->Y[i] < *min_y)
	 *min_y = graph->priv->Y[i];
      else if (graph->priv->Y[i] > *max_y)
	 *max_y = graph->priv->Y[i];
//g_print("i: %d, minY %2.2f maxY %2.2f\n", i, *min_y, *max_y );		
   }
   return 0;
   
}
/*
    gint i;
    guint len = gtk_chartbox_graph_get_length (graph);
    guint *X = gtk_chartbox_graph_get_X (graph);
    gfloat *Y = gtk_chartbox_graph_get_Y (graph);
    for(i=1; i < len; i++)
	if ( (*time>X[i-1]) && (*time<X[i]) ){
	    guint x1,x2,x;
	    gfloat y1,y2;
	    x1 = X[i-1];
	    x2 = X[i];
	    y1 = Y[i-1];
	    y2 = Y[i];
	    x = *time;
	    *result = (((x-x1)*(y2-y1))/(x2-x1))+y1;
g_print("x1 %d, x2 %d, y1 %2.1f, y2 %2.1f, x %d, value=%2.2f \n\n", x1,x2,y1,y2,x, *result);

	}
*/	
