#ifndef __GTK_CHARTBOX_RULER_H__
#define __GTK_CHARTBOX_RULER_H__
#include "gtkchartbox.h"

G_BEGIN_DECLS

#define GTK_CHARTBOX_TYPE_RULER			(gtk_chartbox_ruler_get_type ())
#define GTK_CHARTBOX_RULER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_RULER, GtkChartBoxRuler))
#define GTK_CHARTBOX_RULER_CLASS(klass) 	(G_TYPE_CHECK_CLASS_CAST(klass, GTK_CHARTBOX_TYPE_RULER, GtkChartBoxRulerClass))
#define GTK_IS_CHARTBOX_RULER(obj) 		(G_TYPE_CHECK_INSTANCE_TYPE(obj, GTK_CHARTBOX_TYPE_RULER))

typedef struct _GtkChartBoxRulerClass		GtkChartBoxRulerClass;
typedef struct _GtkChartBoxRulerPrivate 	GtkChartBoxRulerPrivate;
    struct _GtkChartBoxRuler
    {
	GtkWidget widget;

	/* < private > */
	GtkChartBoxRulerPrivate *priv;

    };

    struct _GtkChartBoxRulerClass
    {
	GtkWidgetClass parent_class;

    };
GType 
gtk_chartbox_ruler_get_type (void)

  G_GNUC_CONST;    
    GtkWidget *gtk_chartbox_ruler_new (GtkOrientation orientation );

    void gtk_chartbox_ruler_set_type_time (GtkChartBoxRuler * ruler, gboolean type_time);
    gboolean gtk_chartbox_ruler_get_type_time (GtkChartBoxRuler * ruler );
    void gtk_chartbox_ruler_set_zoom (GtkChartBoxRuler * ruler, guint zoom);
    void gtk_chartbox_ruler_set_offset (GtkChartBoxRuler * ruler, gfloat offset);    
    
    void gtk_chartbox_ruler_set_orientation (GtkChartBoxRuler * ruler, GtkOrientation orientation);
    guint gtk_chartbox_ruler_get_position (GtkChartBoxRuler * ruler, gfloat *position);

    GtkOrientation gtk_chartbox_ruler_get_orientation (GtkChartBoxRuler * ruler);

    void gtk_chartbox_ruler_set_range (GtkChartBoxRuler * ruler,
				       gdouble lower,
				       gdouble upper, gdouble position);
    void gtk_chartbox_ruler_get_range (GtkChartBoxRuler * ruler,
				       gdouble * lower,
				       gdouble * upper, gdouble * position);

	/* Used by graph objects */
    cairo_surface_t* gtk_chartbox_ruler_get_backing_surface(GtkChartBoxRuler * ruler);

G_END_DECLS

#endif
