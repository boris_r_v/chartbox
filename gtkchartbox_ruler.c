#include "gtkchartbox_ruler.h"
#include <time.h>
#define RULER_SIZE  30
#define RULER_TIME_SIZE  40
#define TICKS  8
#define ZERO_OFF 10
#define LENGHT_OFF 20
#define ROUND(x) ((int) ((x) + 0.5))
static void gtk_chartbox_ruler_draw_ticks (GtkChartBoxRuler * ruler);
static void gtk_chartbox_ruler_draw_pos (GtkChartBoxRuler * ruler );

static void gtk_chartbox_ruler_make_surface (GtkChartBoxRuler * ruler);
static gint gtk_chartbox_ruler_motion_notify (GtkWidget * widget,GdkEventMotion * event);
static void gtk_chartbox_ruler_realize (GtkWidget * widget);
static void gtk_chartbox_ruler_unrealize (GtkWidget * widget);
#if (GTK_MAJOR_VERSION == 3)
static gboolean gtk_chartbox_ruler_draw (GtkWidget *widget, cairo_t   *cr);
static void gtk_chartbox_ruler_get_preferred_width (GtkWidget *widget, gint *minimum_size, gint *natural_size );
static void gtk_chartbox_ruler_get_preferred_height (GtkWidget *widget, gint *minimum_size, gint *natural_size );
#else
static gint gtk_chartbox_ruler_expose (GtkWidget * widget, GdkEventExpose * event);
static void gtk_chartbox_ruler_size_request(GtkWidget *widget, GtkRequisition *requisition);
#endif

static void gtk_chartbox_ruler_size_allocate (GtkWidget * widget, GtkAllocation * allocation);
static void gtk_chartbox_ruler_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec);
static void gtk_chartbox_ruler_get_property (GObject * object, guint prop_id, GValue * value, GParamSpec * pspec);
enum
{
   PROP_0,
   PROP_LOWER,
   PROP_UPPER,
   PROP_POSITION,
   PROP_MAX_LENGTH,
   PROP_ORIENTATION,
   PROP_TYPE_TIME,
   PROP_END_OF_LIST
};

struct _GtkChartBoxRulerPrivate
{
   cairo_surface_t *backing_surface;
   gint xsrc;
   gint ysrc;
   gdouble lower;
   gdouble upper;
   gdouble position;
   guint max_length;
   GtkOrientation orientation;
   gboolean type_time;
   guint weight;
   guint zoom;
   gfloat offset;
   gchar format_string[10];
   

};

G_DEFINE_TYPE (GtkChartBoxRuler, gtk_chartbox_ruler, GTK_TYPE_WIDGET)
GtkWidget *
gtk_chartbox_ruler_new (GtkOrientation orientation )
{
   return g_object_new (GTK_CHARTBOX_TYPE_RULER, "orientation", orientation, NULL);
}

static void gtk_chartbox_ruler_class_init (GtkChartBoxRulerClass * class)
{
   GObjectClass *gobject_class;
   GtkWidgetClass *widget_class;

   gobject_class = G_OBJECT_CLASS (class);
   widget_class = (GtkWidgetClass *) class;

   gobject_class->set_property = gtk_chartbox_ruler_set_property;
   gobject_class->get_property = gtk_chartbox_ruler_get_property;
   
   widget_class->realize = gtk_chartbox_ruler_realize;
   widget_class->unrealize = gtk_chartbox_ruler_unrealize;
   widget_class->size_allocate = gtk_chartbox_ruler_size_allocate;
#if (GTK_MAJOR_VERSION == 3)
    widget_class->draw = gtk_chartbox_ruler_draw;     
    widget_class->get_preferred_width  = gtk_chartbox_ruler_get_preferred_width;
    widget_class->get_preferred_height = gtk_chartbox_ruler_get_preferred_height;
#else
   widget_class->size_request = gtk_chartbox_ruler_size_request;
   widget_class->expose_event = gtk_chartbox_ruler_expose;
#endif
   widget_class->motion_notify_event = gtk_chartbox_ruler_motion_notify;

   g_object_class_install_property (gobject_class,
				    PROP_ORIENTATION,
				    g_param_spec_uint ("orientation",
						       "Orientation",
						       "Orientation of the ruler: horizontal or vertical",
						       GTK_ORIENTATION_HORIZONTAL,
						       GTK_ORIENTATION_VERTICAL,
						       GTK_ORIENTATION_HORIZONTAL,
						       G_PARAM_READWRITE |
						       G_PARAM_CONSTRUCT_ONLY));
   g_object_class_install_property (gobject_class,
				    PROP_TYPE_TIME,
				    g_param_spec_boolean ("type_time",
							 "Type_Time",
							 "View type to draw ticks label",
							 FALSE,
							 G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

}

static void
gtk_chartbox_ruler_init (GtkChartBoxRuler * ruler)
{
   ruler->priv = g_new0 (GtkChartBoxRulerPrivate, 1);
   ruler->priv->backing_surface = NULL;
   ruler->priv->xsrc = 0;
   ruler->priv->ysrc = 0;
   ruler->priv->lower = 0.;
   ruler->priv->upper = 100.;
   ruler->priv->position = 0.;
   ruler->priv->max_length = 6;
   ruler->priv->orientation = GTK_ORIENTATION_HORIZONTAL;
   ruler->priv->weight = RULER_SIZE;
   ruler->priv->type_time = FALSE;
   ruler->priv->zoom = 1;
   ruler->priv->offset = 0;   
   g_snprintf(ruler->priv->format_string, sizeof(ruler->priv->format_string) ,"%%.1f");
   
}

static gint
gtk_chartbox_ruler_motion_notify (GtkWidget * widget, GdkEventMotion * event){
   GtkChartBoxRuler *ruler;
   ruler = GTK_CHARTBOX_RULER (widget);
   GtkAllocation allocation;
   gtk_widget_get_allocation( widget, &allocation );    
   guint x;
   guint y;
   if (event->is_hint)
   {
#if (GTK_MAJOR_VERSION == 3)
      gdk_window_get_device_position(event->window, event->device, &x, &y, NULL );
#else
      gdk_window_get_pointer (gtk_widget_get_window(widget), &x, &y, NULL);
#endif      
   }
   else{
      x = event->x;
      y = event->y;
   }


   if (ruler->priv->orientation == GTK_ORIENTATION_HORIZONTAL){
      if (x<10)
        return FALSE;
      ruler->priv->position = ruler->priv->lower + (ruler->priv->upper - ruler->priv->lower) * ((( (x-10 + ruler->priv->offset) / ruler->priv->zoom ) ) / allocation.width );
   }
   else{
      ruler->priv->position = ruler->priv->upper - ((ruler->priv->upper - ruler->priv->lower) * (y+10) )/ allocation.height;
    }	
   /*  Make sure the ruler has been allocated already  */
   if (ruler->priv->backing_surface != NULL)
      gtk_chartbox_ruler_draw_pos (ruler);

   return FALSE;
}

static void 
gtk_chartbox_ruler_realize (GtkWidget * widget){
   GtkChartBoxRuler *ruler = GTK_CHARTBOX_RULER (widget);
   GdkWindowAttr attributes;
   GdkWindow* window;
   gint attributes_mask;
   GtkAllocation allocation;

   gtk_widget_set_realized( widget, TRUE );    
   gtk_widget_get_allocation( widget, &allocation );    

   attributes.window_type = GDK_WINDOW_CHILD;
   attributes.x = allocation.x;
   attributes.y = allocation.y;
   attributes.width = allocation.width;
   attributes.height = allocation.height;
   attributes.wclass = GDK_INPUT_OUTPUT;
   attributes.visual = gtk_widget_get_visual (widget);
#if (GTK_MAJOR_VERSION == 2)
   attributes.colormap = gtk_widget_get_colormap (widget);
#endif
   attributes.event_mask = gtk_widget_get_events (widget);
   attributes.event_mask |= (GDK_EXPOSURE_MASK );

#if (GTK_MAJOR_VERSION == 3)
   attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;
#else
   attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
#endif
     
   window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
   gtk_widget_set_window (widget, window);
   gdk_window_set_user_data (gtk_widget_get_window(widget), ruler);

#if (GTK_MAJOR_VERSION == 2)
   gtk_widget_set_style(widget, gtk_style_attach (gtk_widget_get_style (widget), window));
   gtk_style_set_background (gtk_widget_get_style(widget), gtk_widget_get_window(widget), GTK_STATE_ACTIVE);
#endif

   gtk_chartbox_ruler_make_surface (ruler);
}
static void 
gtk_chartbox_ruler_unrealize (GtkWidget * widget){
   GtkChartBoxRuler *ruler = GTK_CHARTBOX_RULER (widget);

   if (ruler->priv->backing_surface)
      cairo_surface_destroy (ruler->priv->backing_surface);

   g_free (ruler->priv);

   if (GTK_WIDGET_CLASS (gtk_chartbox_ruler_parent_class)->unrealize)
      (*GTK_WIDGET_CLASS (gtk_chartbox_ruler_parent_class)->unrealize) (widget);

}
static void 
gtk_chartbox_ruler_get_preferred_width (GtkWidget *widget, gint *minimum_size, gint *natural_size )
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(minimum_size != NULL);
  g_return_if_fail(natural_size != NULL);
  GtkAllocation allocation;
  gtk_widget_get_allocation( widget, &allocation );
  *minimum_size = 30;//allocation.width;
  *natural_size = 30+allocation.width; 
}

static void 
gtk_chartbox_ruler_get_preferred_height (GtkWidget *widget, gint *minimum_size, gint *natural_size)
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(minimum_size != NULL);
  g_return_if_fail(natural_size != NULL);
  GtkAllocation allocation;
  gtk_widget_get_allocation( widget, &allocation );
  *minimum_size = 40;//allocation.height;
  *natural_size = 40+allocation.height; 
}

static void 
gtk_chartbox_ruler_size_request(GtkWidget *widget, GtkRequisition *requisition){
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_CHARTBOX_RULER(widget));
  g_return_if_fail(requisition != NULL);

  requisition->width = GTK_CHARTBOX_RULER(widget)->priv->weight;
  requisition->height = GTK_CHARTBOX_RULER(widget)->priv->weight;
}

static void 
gtk_chartbox_ruler_size_allocate (GtkWidget * widget, GtkAllocation * allocation){
   GtkChartBoxRuler *ruler = GTK_CHARTBOX_RULER (widget);

   gtk_widget_set_allocation( widget, allocation );

   if ( gtk_widget_get_realized (widget) )
   {
      gdk_window_move_resize (gtk_widget_get_window(widget),
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);

      gtk_chartbox_ruler_make_surface (ruler);
   }

}
static void 
gtk_chartbox_ruler_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec)
{
   GtkChartBoxRuler *ruler = GTK_CHARTBOX_RULER (object);

   switch (prop_id)
   {
   case PROP_LOWER:
      gtk_chartbox_ruler_set_range (ruler, g_value_get_double (value),
				   ruler->priv->upper, ruler->priv->position);
      break;
   case PROP_UPPER:
      gtk_chartbox_ruler_set_range (ruler, ruler->priv->lower,
				   g_value_get_double (value),
				   ruler->priv->position);
      break;
   case PROP_POSITION:
      gtk_chartbox_ruler_set_range (ruler, ruler->priv->lower, ruler->priv->upper,
				   g_value_get_double (value));
      break;
   case PROP_TYPE_TIME:
      gtk_chartbox_ruler_set_type_time (ruler, g_value_get_boolean (value));
      break;
   case PROP_ORIENTATION:
      gtk_chartbox_ruler_set_orientation (ruler,
					 (GtkOrientation)
					 g_value_get_uint (value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
   }

}
static void 
gtk_chartbox_ruler_get_property (GObject * object, guint prop_id, GValue * value, GParamSpec * pspec)
{
   GtkChartBoxRuler *ruler = GTK_CHARTBOX_RULER (object);

   switch (prop_id)
   {
   case PROP_LOWER:
      g_value_set_double (value, ruler->priv->lower);
      break;
   case PROP_UPPER:
      g_value_set_double (value, ruler->priv->upper);
      break;
   case PROP_POSITION:
      g_value_set_double (value, ruler->priv->position);
      break;
   case PROP_MAX_LENGTH:
      g_value_set_uint (value, ruler->priv->max_length);
      break;
   case PROP_ORIENTATION:
      g_value_set_uint (value, ruler->priv->orientation);
      break;
   case PROP_TYPE_TIME:
      g_value_set_boolean (value, ruler->priv->type_time);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
   }

}

/* Функции рисования--------------------------------------------------*/
static void
gtk_chartbox_ruler_make_surface (GtkChartBoxRuler * ruler){
   GtkWidget *widget = GTK_WIDGET (ruler);
   GtkAllocation allocation;
   gtk_widget_get_allocation( widget, &allocation );    

   if (ruler->priv->backing_surface)
   {
      if ((cairo_image_surface_get_width(ruler->priv->backing_surface) == allocation.width) &&
	  (cairo_image_surface_get_height(ruler->priv->backing_surface) == allocation.height))
	 return;

      cairo_surface_destroy (ruler->priv->backing_surface);
   }
   ruler->priv->backing_surface = cairo_image_surface_create ( CAIRO_FORMAT_RGB24, allocation.width, allocation.height );
   ruler->priv->xsrc = 0;
   ruler->priv->ysrc = 0;
}

static 
gboolean gtk_chartbox_ruler_draw (GtkWidget *widget, cairo_t   *cr)
{
   GtkChartBoxRuler *ruler;
   GtkAllocation allocation;
   gtk_widget_get_allocation( widget, &allocation );    

   if (gtk_widget_is_drawable( widget ) )
   {
	ruler = GTK_CHARTBOX_RULER (widget);

	gtk_chartbox_ruler_draw_ticks (ruler);

	cairo_set_source_surface( cr, ruler->priv->backing_surface, 0, 0 );
	cairo_rectangle( cr, 0, 0, allocation.width, allocation.height);
	cairo_paint(cr);

	gtk_chartbox_ruler_draw_pos (ruler);
   }

   return FALSE;

}

static gint 
gtk_chartbox_ruler_expose (GtkWidget * widget, GdkEventExpose * event){
   GtkChartBoxRuler *ruler;
   GtkAllocation allocation;
   gtk_widget_get_allocation( widget, &allocation );    

   if (gtk_widget_is_drawable( widget ) )
   {
	ruler = GTK_CHARTBOX_RULER (widget);
	gtk_chartbox_ruler_draw_ticks (ruler);

/*      gdk_draw_drawable (gtk_widget_get_window(widget),
			 gtk_widget_get_style(widget)->fg_gc[GTK_WIDGET_STATE (ruler)],
			 ruler->priv->backing_surface,
			 0, 0, 
			 0, 0, widget->allocation.width, widget->allocation.height);
*/
	cairo_t* cr = gdk_cairo_create (gtk_widget_get_window(widget));
	cairo_set_source_surface( cr, ruler->priv->backing_surface, 0, 0 );
	cairo_rectangle( cr, 0, 0, allocation.width, allocation.height);
	cairo_fill(cr);
	cairo_destroy(cr);
	gtk_chartbox_ruler_draw_pos (ruler);
   }

   return FALSE;
}

static void 
gtk_chartbox_ruler_draw_ticks (GtkChartBoxRuler * ruler){
    if (!gtk_widget_is_drawable( GTK_WIDGET(ruler)) )
      return;

    GtkWidget* widget = GTK_WIDGET (ruler);
    GtkAllocation allocation;
    gtk_widget_get_allocation( widget, &allocation );    

    gint  _STEP=0;
    guint _TICKS = TICKS*ruler->priv->zoom;
    gfloat _DS = (ruler->priv->upper - ruler->priv->lower)/_TICKS;
    gboolean _isVert = FALSE;
    if(ruler->priv->orientation == GTK_ORIENTATION_VERTICAL ){     
	_STEP = (guint)(( allocation.height * ruler->priv->zoom) / _TICKS);
	_isVert = TRUE;
    }
    else{
	_STEP = (guint)(( allocation.width  * ruler->priv->zoom) /_TICKS); 
	_isVert = FALSE;
    }
    cairo_t *cr = cairo_create(ruler->priv->backing_surface);  
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width  (cr, 2 );
    if (_isVert){
	cairo_rectangle(cr, 0,0,ruler->priv->weight, allocation.height);
	cairo_translate(cr, 0, 0 - ruler->priv->offset); 
    }
    else{
	cairo_rectangle(cr, 0,0, allocation.width, ruler->priv->weight);
    	cairo_translate(cr, 0 - ruler->priv->offset, 0);
    }	
    cairo_stroke(cr);
    guint i, ii;
    gdk_cairo_set_source_color (cr, &gtk_widget_get_style(widget)->fg[gtk_widget_get_state(widget)]);
    cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size (cr, 10);
    gchar buff[10];
    if ( _isVert ){
	cairo_matrix_t cm;
	cairo_get_font_matrix(cr, &cm);
	cairo_matrix_rotate(&cm, 3*1.57);
	cairo_set_font_matrix(cr, &cm);
    }	
    double delta = ruler->priv->upper - ruler->priv->lower;
    for(i=0; i<=_TICKS; i++){
    /*Draw sticks*/
	if (_isVert){
	    cairo_move_to (cr, allocation.width, allocation.height-_STEP * i - ZERO_OFF );
	    cairo_line_to (cr, allocation.width-20, allocation.height-_STEP * i - ZERO_OFF );
	    cairo_move_to (cr, 10, allocation.height - _STEP * i - 15 );
	    //g_snprintf(buff, sizeof(buff), ruler->priv->format_string, _DS*i+ruler->priv->lower );
	    if ( delta > 100  ) g_snprintf(buff, sizeof(buff), "%.0f", _DS*i+ruler->priv->lower );
	    else if ( delta > 10  ) g_snprintf(buff, sizeof(buff), "%.1f", _DS*i+ruler->priv->lower );
	    else g_snprintf(buff, sizeof(buff), "%.2f", _DS*i+ruler->priv->lower );
	    cairo_show_text (cr, buff);
	}
	else{
	    cairo_move_to (cr, _STEP * i+ZERO_OFF, 0 );
	    cairo_line_to (cr, _STEP * i+ZERO_OFF, 14 );
	    if (ruler->priv->type_time){
		gchar buff2[10];
		struct tm tmt;
		time_t timer = ruler->priv->lower+_DS*i;
		localtime_r(&timer, &tmt );
		g_snprintf(buff, sizeof(buff),"%.2d:%.2d:%.2d", tmt.tm_hour, tmt.tm_min, tmt.tm_sec  );
		g_snprintf(buff2, sizeof(buff2),"%.2d/%.2d/%.2d", tmt.tm_mday, tmt.tm_mon+1, tmt.tm_year-100 );
		cairo_move_to (cr, _STEP * i+ZERO_OFF+2, 20 );
		cairo_show_text (cr, buff);
		cairo_move_to (cr, _STEP * i+ZERO_OFF+2, 30 );
		cairo_show_text (cr, buff2);
	    }
	    else{
		cairo_move_to (cr, _STEP * i+ZERO_OFF+5, 20 );
		g_snprintf(buff, sizeof(buff), ruler->priv->format_string, _DS*i+ruler->priv->lower );
		cairo_show_text (cr, buff);
	    }
	}
    /*Draw sub-sticks */
	for (ii=1; ii<=3; ii++){
	    if (_isVert){
		cairo_move_to (cr, allocation.width, allocation.height-_STEP * i - (_STEP/4)*ii - ZERO_OFF);
		cairo_line_to (cr, allocation.width-10, allocation.height-_STEP * i - (_STEP/4)*ii - ZERO_OFF);

	    }
	    else{
		cairo_move_to (cr, 2 +_STEP * i + (_STEP/4)*ii + ZERO_OFF, 0 );
		cairo_line_to (cr, 2 + _STEP * i + (_STEP/4)*ii + ZERO_OFF, 7 );
	    
	    }
	}
    }
    cairo_stroke(cr);	
    cairo_destroy(cr);


}
static void 
gtk_chartbox_ruler_draw_pos (GtkChartBoxRuler * ruler){
   GtkWidget *widget = GTK_WIDGET (ruler);
   GtkAllocation allocation;
   gtk_widget_get_allocation( widget, &allocation );    
   
   gint x, y;
   gint width, height;
   gint bs_width, bs_height;
   gint xthickness;
   gint ythickness;
   gint offset_y;
   guint size_offset;
   gdouble increment;
   if (ruler->priv->type_time){
   	offset_y = 13;
	size_offset = 3;
   }
   else{
	offset_y = 7;	
	size_offset = 2;
   }
   if (gtk_widget_is_drawable( GTK_WIDGET(ruler)) )
   {
      xthickness = gtk_widget_get_style(widget)->xthickness;
      ythickness = gtk_widget_get_style(widget)->ythickness;
      width = allocation.width - xthickness * 2;
      height = allocation.height - ythickness * 2;

      if (ruler->priv->orientation == GTK_ORIENTATION_HORIZONTAL)
      {
	 bs_width = height / size_offset + size_offset;
	 bs_width |= 1;		/* make sure it's odd */
	 bs_height = bs_width / size_offset + size_offset ;
      }
      else
      {
	 bs_height = width / size_offset + size_offset;
	 bs_height |= 1;	/* make sure it's odd */
	 bs_width = bs_height / size_offset + size_offset - 1;
      }

      if ((bs_width > 0) && (bs_height > 0))
      {
	 cairo_t *cr = gdk_cairo_create (gtk_widget_get_window(widget));

	 /*  If a backing store exists, restore the ruler  */
	 if (ruler->priv->backing_surface)
	 {
	    cairo_set_source_surface( cr, ruler->priv->backing_surface, 0, 0 );
	    cairo_rectangle( cr, ruler->priv->xsrc, ruler->priv->ysrc, bs_width, bs_height);
	    cairo_fill(cr);
	    
	 }
	 if (ruler->priv->orientation == GTK_ORIENTATION_HORIZONTAL)
	 {
	    x =  ROUND( allocation.width * ((ruler->priv->position - ruler->priv->lower) /(ruler->priv->upper - ruler->priv->lower)) );
	    x *= ruler->priv->zoom;
	    x -= ruler->priv->offset;
	    x +=2;
	    y = (height - bs_height) / 2 + ythickness - offset_y;
	    gdk_cairo_set_source_color (cr, &gtk_widget_get_style(widget)->fg[gtk_widget_get_state(widget)]);

	    cairo_move_to (cr, x, y + bs_height);
	    cairo_line_to (cr, x + bs_width / 2., y );
	    cairo_line_to (cr, x + bs_width, y + bs_height);

	 }
	 else
	 {
	    increment = (gdouble) height / (ruler->priv->upper - ruler->priv->lower);

	    x = (width + bs_width) / 2 + xthickness;
	    y = ROUND ((ruler->priv->upper - ruler->priv->position) * increment) +  (ythickness - bs_height) / 2 - 9;

	    gdk_cairo_set_source_color (cr, &gtk_widget_get_style(widget)->fg[gtk_widget_get_state(widget)]);

	    cairo_move_to (cr, x, y);
	    cairo_line_to (cr, x + bs_width, y + bs_height / 2.);
	    cairo_line_to (cr, x, y + bs_height);
	 }
	 cairo_fill (cr);

	 cairo_destroy (cr);

	 ruler->priv->xsrc = x;
	 ruler->priv->ysrc = y;
      }
   }
}

/*-Внешние функции-------------------*/
cairo_surface_t* 
gtk_chartbox_ruler_get_backing_surface(GtkChartBoxRuler * ruler){
    g_return_val_if_fail (GTK_IS_CHARTBOX_RULER (ruler), NULL);
    return ruler->priv->backing_surface;
    
}
void 
gtk_chartbox_ruler_set_zoom (GtkChartBoxRuler * ruler, guint zoom){
    g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler) );
    ruler->priv->zoom = zoom;
//надо вызвать фунцию перерисовки
    gtk_widget_queue_draw (GTK_WIDGET(ruler));
}

void 
gtk_chartbox_ruler_set_offset (GtkChartBoxRuler * ruler, gfloat offset){
    g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler) );
    ruler->priv->offset = offset;
//надо вызвать фунцию перерисовки
    gtk_widget_queue_draw (GTK_WIDGET(ruler));
}
void 
gtk_chartbox_ruler_set_range (GtkChartBoxRuler * ruler, gdouble lower, gdouble upper, gdouble position){
    g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler) );
    ruler->priv->lower = lower;
    ruler->priv->upper = upper;
    ruler->priv->position = position;
    if (upper < 10)
	g_snprintf(ruler->priv->format_string, sizeof(ruler->priv->format_string) ,"%%1.2f");
    else if (upper < 100)
	g_snprintf(ruler->priv->format_string, sizeof(ruler->priv->format_string) ,"%%2.1f");
    else if (upper < 1000)   	
    	g_snprintf(ruler->priv->format_string, sizeof(ruler->priv->format_string) ,"%%3.0f");
    	
}
void
gtk_chartbox_ruler_get_range (GtkChartBoxRuler * ruler, gdouble * lower, gdouble * upper, gdouble * position){
    g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler) );
   if (lower)
      *lower = ruler->priv->lower;
   if (upper)
      *upper = ruler->priv->upper;
   if (position)
      *position = ruler->priv->position;

}
guint
gtk_chartbox_ruler_get_position (GtkChartBoxRuler * ruler, gfloat *position){
    g_return_val_if_fail (GTK_IS_CHARTBOX_RULER (ruler), 0 );
    if (position)
      *position = ruler->priv->position;
    return ((guint)ruler->priv->position);    
}
void
gtk_chartbox_ruler_set_type_time (GtkChartBoxRuler * ruler, gboolean type_time){
    g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler) );
    GtkWidget *widget = GTK_WIDGET (ruler);
    if (ruler->priv->type_time != type_time){
	ruler->priv->type_time = type_time;
	if (type_time)
	     ruler->priv->weight = RULER_TIME_SIZE;
	else
	     ruler->priv->weight = RULER_SIZE;
    }
}

gboolean
gtk_chartbox_ruler_get_type_time (GtkChartBoxRuler * ruler ){
    g_return_val_if_fail (GTK_IS_CHARTBOX_RULER (ruler), 0 );
    return ruler->priv->type_time ;

}

void
gtk_chartbox_ruler_set_orientation (GtkChartBoxRuler * ruler, GtkOrientation orientation)
{
   g_return_if_fail (GTK_IS_CHARTBOX_RULER (ruler));
   GtkWidget *widget = GTK_WIDGET (ruler);
   if (ruler->priv->orientation != orientation)
   {
      ruler->priv->orientation = orientation;
      g_object_notify (G_OBJECT (ruler), "orientation");
   }
   if (gtk_widget_is_drawable( GTK_WIDGET(ruler)) )
      gtk_widget_queue_draw (GTK_WIDGET (ruler));
}
GtkOrientation
gtk_chartbox_ruler_get_orientation (GtkChartBoxRuler * ruler)
{

   g_return_val_if_fail (GTK_IS_CHARTBOX_RULER (ruler), -1);

   return ruler->priv->orientation;
}

