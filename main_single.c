#include <gtk/gtk.h>
#include "gtkchartbox.h"
#include "gtkchartbox_ruler.h"
#include "gtkchartbox_lines.h"
#include "gtkchartbox_points.h"
#include "gtkchartbox_bars.h"
#include "gtkchartbox_graph.h"
#include <stdlib.h>

static void NewVaL(GtkChartBoxGraph* graph, gpointer data){
    float val=0;
    val = gtk_chartbox_graph_get_value(graph );
    gchar buff[10];
    g_snprintf(buff, sizeof(buff), "%2.2f", val);
    gtk_entry_set_text(GTK_ENTRY(data), buff);
}

static void ZoomIn(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_in(GTK_CHARTBOX(data) );
}

static void ZoomOut(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_out(GTK_CHARTBOX(data) );
}

static void ZoomHome(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_home(GTK_CHARTBOX(data) );
}

static void SavePict(GtkWidget *graph, gpointer data){
    gtk_chartbox_save(GTK_CHARTBOX(data) );
}

typedef struct {
    GtkWidget *box, *graph;
} dataBack;

static void update_data (gpointer *_data ){
    int len=500;
    while ( 1 ){
printf("update_data len %d\n", len);
	guint *Xi;
	gfloat *Yi;
	Xi = gtk_chartbox_graph_get_X(GTK_CHARTBOX_GRAPH( ((dataBack*)_data)->graph ) );
	Yi = gtk_chartbox_graph_get_Y(GTK_CHARTBOX_GRAPH( ((dataBack*)_data)->graph )  );
	g_free ( Xi );
	g_free ( Yi );
	Xi = g_new0 (guint, len );
	Yi = g_new0 (gfloat, len );
	int ii=0;
	for (ii=0; ii<len; ii++){
	    Xi[ii] = 1255346042+ii;
	    Yi[ii] = drand48()*30;
//	    printf("\tXi[%d]=%d, Yi[%d]=%f\n", ii, Xi[ii], ii, Yi[ii]);	
	}
	gtk_chartbox_graph_set_length(GTK_CHARTBOX_GRAPH( ((dataBack*)_data)->graph ) , len );
	gtk_chartbox_graph_set_X(GTK_CHARTBOX_GRAPH( ((dataBack*)_data)->graph ) , Xi );
	gtk_chartbox_graph_set_Y(GTK_CHARTBOX_GRAPH( ((dataBack*)_data)->graph ) , Yi );
	gtk_chartbox_update(GTK_CHARTBOX( ((dataBack*)_data)->box ) );

//	len++;
	usleep(1000);
    }
}
int
main (int argc, char **argv)
{
	gtk_init (&argc, &argv);

	GtkWidget *window;
	GtkWidget *box, *table, *self_box, *sibl_box;
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	GdkColor c;
	
	gtk_chartbox_create_all(&box, &table, TRUE, TRUE, TRUE, TRUE );
	
	GtkWidget *val1 = gtk_entry_new (); 
	gtk_widget_set_size_request (val1, 20, -1);
	gtk_editable_set_editable (GTK_EDITABLE (val1), FALSE);

	guint* X = g_new0 (guint, 10);
	gfloat* Y = g_new0 (gfloat, 10);
	X[0]=1255346042; Y[0]=43.294;
	X[1]=1255346043; Y[1]=20.294;
	X[2]=1255346044; Y[2]=58.294;
	X[3]=1255346045; Y[3]=12.294;
	X[4]=1255346046; Y[4]=38.294;
	X[5]=1255346047; Y[5]=2.294;
	X[6]=1255346048; Y[6]=40.294;
	X[7]=1255346049; Y[7]=15.294;
	X[8]=1255346050; Y[8]=55.294;
	X[9]=1255346051; Y[9]=22.294;
/*	X[0]=0; Y[0]=43.294;
	X[1]=1; Y[1]=20.294;
	X[2]=2; Y[2]=58.294;
	X[3]=3; Y[3]=12.294;
	X[4]=4; Y[4]=38.294;
	X[5]=5; Y[5]=2.294;
	X[6]=6; Y[6]=40.294;
	X[7]=7; Y[7]=15.294;
	X[8]=8; Y[8]=55.294;
	X[9]=9; Y[9]=22.294;
*/	gdk_color_parse("#aa00cc", &c);
	GtkChartBoxGraph *graph = gtk_chartbox_lines_new(10, X, Y, &c, 2);
	g_signal_connect(G_OBJECT(graph),"value-change", G_CALLBACK(NewVaL), val1);	
	gtk_chartbox_graph_add(GTK_CHARTBOX(box), graph);
	
	dataBack *DB = malloc(sizeof( dataBack ) );
	DB->box = GTK_WIDGET( box );
	DB->graph = GTK_WIDGET( graph );
	
/*    	GError *_error;
	if (!g_thread_supported ()) g_thread_init (NULL);
	GThread*  _tread = g_thread_create(update_data, (gpointer)DB, TRUE, &_error );
	printf("err %s\n", _error->message );
*/	

	GtkWidget *hbox = gtk_hbox_new(0,0);
	GtkWidget *vbox = gtk_vbox_new(0,0);
	GtkWidget *zoomIn = gtk_button_new_from_stock(GTK_STOCK_ZOOM_IN);
	GtkWidget *zoomOut = gtk_button_new_from_stock(GTK_STOCK_ZOOM_OUT);
	GtkWidget *zoomHome = gtk_button_new_from_stock(GTK_STOCK_ZOOM_100);
	GtkWidget *save = gtk_button_new_from_stock(GTK_STOCK_SAVE);

	g_signal_connect(G_OBJECT(zoomIn),"clicked", G_CALLBACK(ZoomIn), box);
	g_signal_connect(G_OBJECT(zoomOut),"clicked", G_CALLBACK(ZoomOut), box);
	g_signal_connect(G_OBJECT(zoomHome),"clicked", G_CALLBACK(ZoomHome), box);
	g_signal_connect(G_OBJECT(save),"clicked", G_CALLBACK(SavePict), box);
	
	gtk_box_pack_start(GTK_BOX(vbox), zoomIn, FALSE, FALSE, 1);		
	gtk_box_pack_start(GTK_BOX(vbox), zoomOut, FALSE, FALSE, 1);		
	gtk_box_pack_start(GTK_BOX(vbox), zoomHome, FALSE, FALSE, 1);			
	gtk_box_pack_start(GTK_BOX(vbox), save, FALSE, FALSE, 1);				
	gtk_box_pack_start(GTK_BOX(vbox), val1, FALSE, FALSE, 1);			
	
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, FALSE, 1);	
	gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 1);
	gtk_container_add (GTK_CONTAINER (window), hbox);

	g_signal_connect (window, "destroy",
			G_CALLBACK (gtk_main_quit), NULL);

	gtk_widget_show_all (window);

	
	gtk_main ();
}

