#ifndef __GTK_CHARTBOX_TYPEDEFS_H__
#define __GTK_CHARTBOX_TYPEDEFS_H__

#include <gdk/gdk.h>

G_BEGIN_DECLS
    typedef struct _GtkChartBox GtkChartBox;
    typedef struct _GtkChartBoxGraph GtkChartBoxGraph;
    typedef struct _GtkChartBoxMarker GtkChartBoxMarker;    
    typedef struct _GtkChartBoxRuler GtkChartBoxRuler;
    
G_END_DECLS
#endif /* __GTK_CHARTBOX_TYPEDEFS_H__ */


