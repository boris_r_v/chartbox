#define gtk_hbox_new(a, b) gtk_box_new( GTK_ORIENTATION_HORIZONTAL, b );  
#define gtk_vbox_new(a, b) gtk_box_new( GTK_ORIENTATION_VERTICAL, b ) 
#define gtk_hseparator_new() gtk_separator_new(  GTK_ORIENTATION_HORIZONTAL )
#define gtk_vseparator_new() gtk_separator_new(  GTK_ORIENTATION_VERTICAL )
#define gtk_hscrollbar_new(a) gtk_scrollbar_new(  GTK_ORIENTATION_HORIZONTAL, a )
#define gtk_vscrollbar_new(a) gtk_scrollbar_new(  GTK_ORIENTATION_VERTICAL, a )
/*
GtkWidget* gtk_hbox_new(gboolean homogenious, gint spacing)
{
    GtkWidget* box = gtk_box_new( GTK_ORIENTATION_HORIZONTAL, spacing );  
    gtk_box_set_homogeneous ( GTK_BOX(box), homogenious );
    return box;
}

GtkWidget* gtk_vbox_new(gboolean homogenious, gint spacing)
{
    GtkWidget* box = gtk_box_new( GTK_ORIENTATION_VERTICAL, spacing );  
    gtk_box_set_homogeneous ( GTK_BOX(box), homogenious );
    return box;
}

*/
