#include "gtkchartbox_points.h"
#include <math.h>
static void gtk_chartbox_points_real_draw (GtkChartBoxGraph * points, GtkChartBox* box );
static gint gtk_chartbox_points_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result );

struct _GtkChartBoxPointsPrivate
{
   GdkPoint *data;
};

static gpointer parent_class = NULL;

static void
points_finalize (GObject * object)
{
   GtkChartBoxPoints *points = GTK_CHARTBOX_POINTS (object);

   g_free (points->priv->data);
   g_free (points->priv);

   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_chartbox_points_class_init (gpointer g_class /*, gpointer g_class_data */ )
{
   GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
   GtkChartBoxGraphClass *graph_class = GTK_CHARTBOX_GRAPH_CLASS (g_class);
   GtkChartBoxPointsClass *klass = GTK_CHARTBOX_POINTS_CLASS (g_class);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = points_finalize;

   graph_class->draw = gtk_chartbox_points_real_draw;
   graph_class->calculate_value = gtk_chartbox_points_real_calculate_value;
}

static void
gtk_chartbox_points_complete (GtkChartBoxPoints * points)
{
   points->priv->data = (GdkPoint*) g_renew (GdkPoint, points->priv->data, gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (points)));

}

static void
gtk_chartbox_points_instance_init (GTypeInstance * instance	/*,
								   gpointer g_class */ )
{
   GtkChartBoxPoints *points = GTK_CHARTBOX_POINTS (instance);

   points->priv = g_new0 (GtkChartBoxPointsPrivate, 1);
   points->priv->data = NULL;
   g_signal_connect (points, "notify::length",
		     G_CALLBACK (gtk_chartbox_points_complete), NULL);
}

GType
gtk_chartbox_points_get_type (void)
{
   static GType type = 0;

   if (type == 0)
   {
      static const GTypeInfo info = {
	 sizeof (GtkChartBoxPointsClass),
	 NULL,			/* base_init */
	 NULL,			/* base_finalize */
	 (GClassInitFunc) gtk_chartbox_points_class_init,	/* class_init */
	 NULL,			/* class_finalize */
	 NULL,			/* class_data */
	 sizeof (GtkChartBoxPoints),	/* instance_size */
	 0,			/* n_preallocs */
	 (GInstanceInitFunc) gtk_chartbox_points_instance_init,	/* instance_init */
	 NULL,			/* value_table */
      };
      type = g_type_register_static (GTK_CHARTBOX_TYPE_GRAPH,
				     "GtkChartBoxPoints", &info, 0);
   }

   return type;
}

GtkChartBoxGraph *
gtk_chartbox_points_new (guint len, guint * X, gfloat * Y,
		       GdkColor * color, guint size)
{
   GtkChartBoxPoints *points;
   g_return_val_if_fail (X, NULL);
   g_return_val_if_fail (Y, NULL);
   g_return_val_if_fail ((len > 0), NULL);

   points = g_object_new (GTK_CHARTBOX_TYPE_POINTS,
			 "X-Values", X,
			 "Y-Values", Y,
			 "length", len, "color", color, "size", size, NULL);

   return GTK_CHARTBOX_GRAPH (points);
}

static void
gtk_chartbox_points_real_draw (GtkChartBoxGraph * graph, GtkChartBox * box){
   GtkWidget *widget;
   GtkChartBoxPoints *points = GTK_CHARTBOX_POINTS (graph);
   GdkPoint *data;
   cairo_surface_t *surface;
   GdkColor *color;
   cairo_t *cr;
   guint i = 0;
   guint  *X;
   gfloat *Y;
   guint len;
   gint radius = 0;
   g_return_if_fail (GTK_IS_CHARTBOX_POINTS (points));
   g_return_if_fail (GTK_IS_CHARTBOX (box));

   widget = GTK_WIDGET(box);
    
   surface = gtk_chartbox_get_backing_surface (box);
   cr = gtk_chartbox_get_backing_content (box);
   
   len = gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (graph));
   color = gtk_chartbox_graph_get_color (GTK_CHARTBOX_GRAPH (graph));
   X = gtk_chartbox_graph_get_X (GTK_CHARTBOX_GRAPH (graph));
   Y = gtk_chartbox_graph_get_Y (GTK_CHARTBOX_GRAPH (graph));
   radius = gtk_chartbox_graph_get_size (GTK_CHARTBOX_GRAPH (graph ));
   data = points->priv->data;
    /*расчитываем значения координат точек */
    gtk_chartbox_values_to_pixels (box, len, X, Y, data);
    /*Рисуем график */
    gdk_cairo_set_source_color (cr, color);
   for (i = 0; i < len; i ++ ){
	cairo_move_to(cr, data[i].x, data[i].y );
	cairo_arc (cr, data[i].x, data[i].y, radius, 0, 2 * M_PI );
   }
    cairo_fill_preserve (cr);
    cairo_stroke(cr);
   return;
}
static gint
gtk_chartbox_points_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result ){
   g_return_val_if_fail (graph, -1);
   g_return_val_if_fail (time, -1);

    gint i;
    guint len = gtk_chartbox_graph_get_length (graph);
    guint *X = gtk_chartbox_graph_get_X (graph);
    gfloat *Y = gtk_chartbox_graph_get_Y (graph);
    for(i=1; i < len; i++)
	if ( (*time == X[i]) ){
	    *result = Y[i];
	    break;
	}

    return 0;
}

