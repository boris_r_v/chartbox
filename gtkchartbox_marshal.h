
#ifndef __gtk_chartbox_marshal_MARSHAL_H__
#define __gtk_chartbox_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS
/* VOID:VOID (gtkchartbox_marshal.list:1) */
#define gtk_chartbox_marshal_VOID__VOID		g_cclosure_marshal_VOID__VOID
/* VOID:POINTER (gtkchartbox_marshal.list:2) */
#define gtk_chartbox_marshal_VOID__POINTER	g_cclosure_marshal_VOID__POINTER
/* VOID:INT (gtkchartbox_marshal.list:3) */
#define gtk_chartbox_marshal_VOID__INT		g_cclosure_marshal_VOID__INT
/* VOID:INT (gtkchartbox_marshal.list:4) */
#define gtk_chartbox_marshal_VOID__FLOAT	g_cclosure_marshal_VOID__FLOAT

/*extern void gtk_chartbox_marshal_VOID__INT (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);
*/
   G_END_DECLS
#endif /* __gtk_chartbox_marshal_MARSHAL_H__ */
