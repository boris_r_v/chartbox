#ifndef __GTK_CHARTBOX_LINES_H__
#define __GTK_CHARTBOX_LINES_H__

#include "gtkchartbox_graph.h"

G_BEGIN_DECLS
#define GTK_CHARTBOX_TYPE_LINES		  (gtk_chartbox_lines_get_type ())
#define GTK_CHARTBOX_LINES(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_LINES, GtkChartBoxLines))
#define GTK_CHARTBOX_LINES_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass),  GTK_CHARTBOX_TYPE_LINES, GtkChartBoxLinesClass))
#define GTK_IS_CHARTBOX_LINES(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_CHARTBOX_TYPE_LINES ))
#define GTK_IS_CHARTBOX_LINES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_CHARTBOX_TYPE_LINES))
#define GTK_CHARTBOX_LINES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE_LINES, GtkChartBoxLinesClass))

   typedef struct _GtkChartBoxLines GtkChartBoxLines;

   typedef struct _GtkChartBoxLinesClass GtkChartBoxLinesClass;

   typedef struct _GtkChartBoxLinesPrivate GtkChartBoxLinesPrivate;

   struct _GtkChartBoxLines
   {
      /*< private >*/
      GtkChartBoxGraph parent;

      GtkChartBoxLinesPrivate *priv;
   };

   struct _GtkChartBoxLinesClass
   {
      GtkChartBoxGraphClass parent_class;
   };

   GType gtk_chartbox_lines_get_type (void);

   GtkChartBoxGraph *gtk_chartbox_lines_new (guint len, guint * X, gfloat * Y, GdkColor * color, guint size);


G_END_DECLS
#endif				/* __GTK_CHARTBOX_LINES_H__ */
