//#include <gtk/gtk.h>
#include "chartbox.h"
#include <cstdlib>
using namespace chart;
static unsigned long x;

gboolean
periodic ( gpointer _data )
{
    ++x;
    float y = drand48()*30; 
    chartbox* bb = static_cast<chartbox*>(_data);
    //printf( "x=%lu y=%f\n", x, y );
    y = (int)y%2==0 ? y : -y;
    bb->add_one_point_to_line( "line1", x, y );
    bb->update();
    return TRUE;
};


int
main (int argc, char **argv)
{
	gtk_init (&argc, &argv);
	x = 1338966369;
	GtkWidget* window = gtk_window_new (GTK_WINDOW_TOPLEVEL);   
	gtk_window_set_title ( GTK_WINDOW( window ), "...Тест обертки..." );

	chartbox chbox;
	chbox.init_with_stop_update_button(  );
	FV _markers;
	_markers.push_back( 7.7 );
	_markers.push_back( -2.3 );
	_markers.push_back( 2.3 );

	chartbox_line line1 ( chbox, _markers, "line1", "#cc0000", "Больших полосатых Попугаев" );
	chbox.add_line ( line1 );
	chbox.set_negative_value_flag ( true );

/*	chbox.add_one_point_to_line_update_chart ("line1", 1, 10 );	
	chbox.add_one_point_to_line_update_chart ("line1", 2, 40 );	
	chbox.add_one_point_to_line_update_chart ("line1", 3, 0 );	
	chbox.add_one_point_to_line_update_chart ("line1", 4, -10 );	
	chbox.add_one_point_to_line_update_chart ("line1", 5, -50 );	
	chbox.add_one_point_to_line_update_chart ("line1", 6, -10 );	
	chbox.add_one_point_to_line_update_chart ("line1", 7, 20 );	
*/	
	gtk_container_add ( GTK_CONTAINER( window), chbox.get_main_box() );
	gtk_widget_show_all( window );
	//line1.smooth_button_set_visible( false );
	g_timeout_add_seconds( 1, periodic, &chbox );
	gtk_main ();
}

