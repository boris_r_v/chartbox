#include "gtkchartbox_marker.h"
#include "gtkchartbox_marshal.h"
static void gtk_chartbox_marker_real_draw (GtkChartBoxMarker * marker, GtkChartBox * draw);


struct _GtkChartBoxMarkerPrivate
{
    gboolean hide;
    GdkColor color;
    guint size;
    guint zoom;    
    guint *X;
    gfloat *Y;
    gboolean line;
    gchar *text;
};

static gpointer parent_class = NULL;

static void
marker_finalize (GObject * object){

   GtkChartBoxMarker *marker = GTK_CHARTBOX_MARKER (object );
   g_free (marker->priv);
   
   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_chartbox_marker_class_init (gpointer g_class )
{
   GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
   GtkChartBoxMarkerClass *klass = GTK_CHARTBOX_MARKER_CLASS (g_class);
   GParamSpec *marker_param_spec;

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = marker_finalize;

   klass->draw = gtk_chartbox_marker_real_draw;


}

static void
gtk_chartbox_marker_instance_init (GTypeInstance * instance
				 /*, gpointer g_class */ )
{
   GtkChartBoxMarker *marker = GTK_CHARTBOX_MARKER (instance);

   marker->priv = g_new0 (GtkChartBoxMarkerPrivate, 1);
   
}

GType
gtk_chartbox_marker_get_type (void)
{
   static GType type = 0;

   if (type == 0)
   {
      static const GTypeInfo info = {
	 sizeof (GtkChartBoxMarkerClass),
	 NULL,			/* base_init */
	 NULL,			/* base_finalize */
	 (GClassInitFunc) gtk_chartbox_marker_class_init,	/* class_init */
	 NULL,			/* class_finalize */
	 NULL,			/* class_data */
	 sizeof (GtkChartBoxMarker),	/* instance_size */
	 0,			/* n_preallocs */
	 (GInstanceInitFunc) gtk_chartbox_marker_instance_init,	/* instance_init */
	 NULL,			/* value_table */
      };
      type = g_type_register_static (G_TYPE_OBJECT,
				     "GtkChartBoxMarker", &info, 0);
   }

   return type;
}
/*--------------------make----------------*/
 
GtkChartBoxMarker * 
gtk_chartbox_marker_new(guint * X, gfloat * Y, GdkColor * color, guint size, gchar *text){

    GtkChartBoxMarker *marker;
    g_return_val_if_fail (X, NULL);
    g_return_val_if_fail (Y, NULL);
    g_return_val_if_fail (text, NULL);

    marker = g_object_new (GTK_CHARTBOX_TYPE_MARKER, NULL );
    
    gtk_chartbox_marker_set_X (marker, X);
    gtk_chartbox_marker_set_Y (marker, Y);
    gtk_chartbox_marker_set_color (marker, color);        
    gtk_chartbox_marker_set_size (marker, size);
    gtk_chartbox_marker_set_text (marker, text);    
    gtk_chartbox_marker_set_hide (marker, FALSE);    
    gtk_chartbox_marker_set_line (marker, FALSE);    
    gtk_chartbox_marker_set_zoom (marker, 1);        


    
    return marker;
}

/*----------------Setters ----------------*/
void
gtk_chartbox_marker_set_text (GtkChartBoxMarker * marker, gchar  *text)
{
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER(marker));
   g_return_if_fail (text);
   marker->priv->text = text;
}
void
gtk_chartbox_marker_set_X (GtkChartBoxMarker * marker, guint  *X)
{
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER(marker));
   g_return_if_fail (X);
   marker->priv->X = X;
}
void
gtk_chartbox_marker_set_Y (GtkChartBoxMarker * marker, gfloat  *Y)
{
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER(marker));
   g_return_if_fail (Y);
   marker->priv->Y = Y;
}
void 
gtk_chartbox_marker_set_hide (GtkChartBoxMarker * marker, gboolean hide){
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER (marker));
   marker->priv->hide =  hide;
}
void 
gtk_chartbox_marker_set_line (GtkChartBoxMarker * marker, gboolean line){
    g_return_if_fail (GTK_IS_CHARTBOX_MARKER (marker));
    marker->priv->line =  line;
}
void 
gtk_chartbox_marker_set_color (GtkChartBoxMarker * marker, GdkColor * color){
    g_return_if_fail (GTK_IS_CHARTBOX_MARKER (marker));
    marker->priv->color = *color;
}
void
gtk_chartbox_marker_set_size (GtkChartBoxMarker * marker, guint size){
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER (marker));
   marker->priv->size =  size;
}
void
gtk_chartbox_marker_set_zoom (GtkChartBoxMarker * marker, guint zoom){
   g_return_if_fail (GTK_IS_CHARTBOX_MARKER (marker));
   marker->priv->zoom =  zoom;
}

/* ---------------------- Getters -------------------------------------*/
gchar *
gtk_chartbox_marker_get_text (GtkChartBoxMarker * marker)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), NULL);
   return marker->priv->text;
}
guint *
gtk_chartbox_marker_get_X (GtkChartBoxMarker * marker)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), NULL);
   return marker->priv->X;

}
gfloat *
gtk_chartbox_marker_get_Y (GtkChartBoxMarker * marker)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), NULL);
   return marker->priv->Y;
}

GdkColor *
gtk_chartbox_marker_get_color (GtkChartBoxMarker * marker){
    g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), NULL);
    return &marker->priv->color;
}
gboolean 
gtk_chartbox_marker_get_hide (GtkChartBoxMarker * marker){
    g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), -1);
    return marker->priv->hide;
}
gboolean 
gtk_chartbox_marker_get_line (GtkChartBoxMarker * marker){
    g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), -1);
    return marker->priv->line;
}
guint
gtk_chartbox_marker_get_size (GtkChartBoxMarker * marker){
    g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), -1);
    return marker->priv->size;
}


  /* This function is called by GtkChartBox */
void gtk_chartbox_marker_draw (GtkChartBoxMarker * marker, GtkChartBox * box){
     if (!marker->priv->hide)
	 GTK_CHARTBOX_MARKER_GET_CLASS (marker)->draw (marker, box);

}

static void
gtk_chartbox_marker_real_draw (GtkChartBoxMarker * marker,
			     GtkChartBox* box)
{
   g_return_if_fail (marker);
   g_return_if_fail (box);

    GtkWidget *widget = GTK_WIDGET(box);
    GdkPoint data;
    GtkAllocation allocation;
    cairo_surface_t *surface;
    cairo_t *cr;

    gtk_widget_get_allocation ( widget, &allocation );
    cr = gtk_chartbox_get_backing_content (box);
    /*Определим координаты точки по данным значений*/   
    gtk_chartbox_values_to_pixels (box, 1, marker->priv->X, marker->priv->Y, &data);
    gdk_cairo_set_source_color (cr, gtk_chartbox_marker_get_color(marker) );    
	/*Установим шрифт*/
//Еще надо установить центровку шрифта, если это вообще возможно	
    cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (cr, (double)marker->priv->size);
    if (marker->priv->line == 0){
    /*Если нужна текстовая метка на графике*/	
	/*Нарисуем круглый маркер*/
	cairo_move_to(cr, data.x, data.y );
	cairo_arc (cr, data.x, data.y, 1, 0, 2*3.145 );
	cairo_fill_preserve (cr);
	/*Выпровнием матрицу*/
	cairo_matrix_t cm;
	cairo_get_font_matrix(cr, &cm);
	cairo_matrix_rotate(&cm, 1.57);
	cairo_set_font_matrix(cr, &cm);
	/*Отобразим текст*/
	cairo_show_text (cr, marker->priv->text);
    }
    else{
    /*Если нужна линия предельного значения на графике*/	    
	/*Рисуем Линию*/
        cairo_set_line_width(cr, 1.0);
	cairo_move_to(cr, data.x, 0);
    	cairo_line_to(cr, data.x, allocation.width*marker->priv->zoom );	
	/*Отобразим Значение величины передела*/
	gchar val[30];
	g_snprintf(val, sizeof(val), "%2.1f%s",*(marker->priv->Y), marker->priv->text );
	cairo_move_to(cr, data.x+2, 2);
	//Выпровнием матрицу//
	cairo_matrix_t cm;
	cairo_get_font_matrix(cr, &cm);
	cairo_matrix_rotate(&cm, 1.57);
	cairo_set_font_matrix(cr, &cm);
	//Отобразим текст//	
	cairo_show_text (cr, val);
	
    }
    cairo_stroke(cr);
    
}


