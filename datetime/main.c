#include <gtk/gtk.h>

static void SavePict(GtkWidget *graph, gpointer data){

}

int
main (int argc, char **argv)
{
	gtk_init (&argc, &argv);

	GtkWidget *window;
	GtkWidget *box, *table;
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	


	GtkWidget *vbox = gtk_hbox_new(0,0);
	GtkWidget *edit = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(edit), 10 );
	gtk_entry_set_text(GTK_ENTRY(edit), "17:27:42" );
	gtk_entry_set_editable(GTK_ENTRY(edit), FALSE );
	
	
//	gtk_box_pack_start(GTK_BOX(vbox), edit, FALSE, FALSE, 1);
	
	GtkWidget *spDay = gtk_spin_button_new_with_range(1,31,1);
	GtkWidget *spMouns = gtk_spin_button_new_with_range(1,12,1);
	GtkWidget *spYears = gtk_spin_button_new_with_range(2009,2100,1);
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spDay), 0);
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spMouns), 0);
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spYears), 0);	

	gtk_box_pack_start(GTK_BOX(vbox), gtk_label_new("День"), FALSE, FALSE, 1);
	gtk_box_pack_start(GTK_BOX(vbox), spDay, FALSE, FALSE, 1);
	gtk_box_pack_start(GTK_BOX(vbox), gtk_label_new("Месяц"), FALSE, FALSE, 1);
	gtk_box_pack_start(GTK_BOX(vbox), spMouns, FALSE, FALSE, 1);	
	gtk_box_pack_start(GTK_BOX(vbox), gtk_label_new("Год"), FALSE, FALSE, 1);
	gtk_box_pack_start(GTK_BOX(vbox), spYears, FALSE, FALSE, 1);	
	gtk_container_add (GTK_CONTAINER (window), vbox);

	g_signal_connect (window, "destroy",
			G_CALLBACK (gtk_main_quit), NULL);

	gtk_widget_show_all (window);
	gtk_main ();
}

