#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char **argv)
{
    if (argc < 2){
	printf("Usage: fromUnixTime time in unix format.\nExample: fromUnixTime 1250865758\n");
	exit(-1);
    }
    time_t _in_time = atol(argv[1]);
    struct tm _tm_zone;
    localtime_r(&_in_time, &_tm_zone);
    printf("Timezone time: \n");
    printf("time: %.2d:%.2d:%.2d", _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec  );
    printf(" date: %.2d/%.2d/%.2d(%.4d)\n", _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900  );

    gmtime_r(&_in_time, &_tm_zone);
    printf("UTC time:\n");
    printf("time: %.2d:%.2d:%.2d", _tm_zone.tm_hour, _tm_zone.tm_min, _tm_zone.tm_sec  );
    printf(" date: %.2d/%.2d/%.2d(%.4d)\n", _tm_zone.tm_mday, _tm_zone.tm_mon+1, _tm_zone.tm_year-100, _tm_zone.tm_year + 1900  );


}
