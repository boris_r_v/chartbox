#include "gtkchartbox.h"
#include "gtkchartbox_ruler.h"
#include "gtkchartbox_graph.h"
#include "gtkchartbox_marker.h"
#include <math.h>
#include <string.h>
#include "gtkchartbox_marshal.h"

static void gtk_chartbox_class_init(GtkChartBoxClass *klass);
static void gtk_chartbox_init(GtkChartBox *cpu);
static void gtk_chartbox_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_chartbox_realize(GtkWidget *widget);
static void gtk_chartbox_unrealize (GtkWidget * widget);
#if (GTK_MAJOR_VERSION == 3)
static gboolean gtk_chartbox_draw (GtkWidget *widget, cairo_t   *cr);
static void gtk_chartbox_get_preferred_width (GtkWidget *widget, gint *minimum_size, gint *natural_size );
static void gtk_chartbox_get_preferred_height (GtkWidget *widget, gint *minimum_size, gint *natural_size);
#else
static void gtk_chartbox_size_request(GtkWidget *widget, GtkRequisition *requisition);
static gboolean gtk_chartbox_expose(GtkWidget *widget, GdkEventExpose *event);
#endif
static gint gtk_chartbox_motion_notify (GtkWidget * widget, GdkEventMotion * event);

static void gtk_chartbox_paint(GtkChartBox *);
static void gtk_chartbox_calculate_extrema(GtkChartBox *);
static void gtk_chartbox_create_backing_surface (GtkChartBox * box);
static void gtk_chartbox_ruler_update (GtkChartBox * box);
static void gtk_chartbox_calculate_factors(GtkChartBox * box);
static void gtk_chartbox_draw_pos(GtkChartBox *box, gint x );
static void gtk_chartbox_draw_time(GtkChartBox *box, gint x );
static void gtk_chartbox_calculate_values (GtkChartBox *, guint time );
static void gtk_chartbox_adjustment_value_changed (GtkChartBox * box);

static void gtk_chartbox_cairo_save (cairo_t *cr, GtkChartBox * box);
static void gtk_chartbox_dat_save (FILE *fd, GtkChartBox * box );
static void gtk_chartbox_csv_save (FILE *fd, GtkChartBox * box );

static void gtk_chartbox_process_save( GtkChartBox* box,  gchar const* filepath, GtkAllocation* allocation  );

/*for Vigil Graph*/
void gtk_chartbox_set_self_box(GtkChartBox *box, GtkChartBox *self_box );	
void gtk_chartbox_set_sibl_box(GtkChartBox *box, GtkChartBox *sibl_box );	

struct _GtkChartBoxPrivate
{
   cairo_surface_t *backing_surface;
   cairo_t *content;

   gchar const* store_path;   
   gchar const* label_text;   
   /* Total and visible limits (values, not pixels) */
   guint max_x;
   guint min_x;
   gfloat max_y;
   gfloat min_y;
   guint visible_x;
   gfloat visible_y;
   gdouble factor_x;
   gfloat factor_y;
   gfloat offset_x;
   
   gboolean draw_cross;
   gboolean negative_value;    
   /* Properties */
   GtkAdjustment *adj_x;
   GtkChartBoxRuler *ruler_x;
   GtkChartBoxRuler *ruler_y;

   /* Other private stuff */
   GList *graphs;
   GList *markers;   
   guint zoom_limit;
   guint zoom;
   gint xsrc, ysrc;
   guint ctime;
   gdouble cx;
/*For Vigil graph*/
    GtkChartBox* self_box;
    GtkChartBox* sibl_box;
    gchar const* text;
};
enum
{
    ZOOM,
    OFFSET,
    MOTION,
    LAST_SIGNAL
};
static gint gtk_chartbox_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (GtkChartBox, gtk_chartbox, GTK_TYPE_WIDGET)

GtkWidget * gtk_chartbox_new()
{
   return g_object_new (GTK_CHARTBOX_TYPE, NULL);
 
}

static void 
gtk_chartbox_class_init(GtkChartBoxClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (class);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
   
    widget_class->realize = gtk_chartbox_realize;
    widget_class->unrealize = gtk_chartbox_unrealize;    
    widget_class->size_allocate = gtk_chartbox_size_allocate;
#if (GTK_MAJOR_VERSION == 3)
    widget_class->draw = gtk_chartbox_draw;
    widget_class->get_preferred_width  = gtk_chartbox_get_preferred_width;
    widget_class->get_preferred_height = gtk_chartbox_get_preferred_height;
#else
    widget_class->size_request = gtk_chartbox_size_request;
    widget_class->expose_event = gtk_chartbox_expose;
#endif
    widget_class->motion_notify_event = gtk_chartbox_motion_notify;

/*--------------------------------Signal description-------------------------------*/
   gtk_chartbox_signals[ZOOM] = 
      g_signal_new ("zoom", 
            G_TYPE_FROM_CLASS (gobject_class), 
            G_SIGNAL_RUN_FIRST, 
            G_STRUCT_OFFSET (GtkChartBoxClass, zoom), 
            NULL,	/* accumulator */
	    NULL,	/* accumulator_data */
	    gtk_chartbox_marshal_VOID__INT,
	    G_TYPE_NONE, 1, 
	    G_TYPE_INT);
    class->zoom = NULL;

   gtk_chartbox_signals[OFFSET] = 
      g_signal_new ("offset", 
            G_TYPE_FROM_CLASS (gobject_class), 
            G_SIGNAL_RUN_FIRST, 
            G_STRUCT_OFFSET (GtkChartBoxClass, offset), 
            NULL,	/* accumulator */
	    NULL,	/* accumulator_data */
	    gtk_chartbox_marshal_VOID__FLOAT,
	    G_TYPE_NONE, 1, 
	    G_TYPE_FLOAT);
    class->offset = NULL;

   gtk_chartbox_signals[MOTION] = 
      g_signal_new ("motion", 
            G_TYPE_FROM_CLASS (gobject_class), 
            G_SIGNAL_RUN_FIRST, 
            G_STRUCT_OFFSET (GtkChartBoxClass, motion), 
            NULL,	/* accumulator */
	    NULL,	/* accumulator_data */
	    gtk_chartbox_marshal_VOID__POINTER,
	    G_TYPE_NONE, 1, 
	    G_TYPE_POINTER);
    class->motion = NULL;

}

static void 
gtk_chartbox_init(GtkChartBox *box){
    box->priv = g_new0 (GtkChartBoxPrivate, 1);
    box->priv->backing_surface = NULL;
    box->priv->adj_x = NULL;
    box->priv->ruler_x = NULL;    
    box->priv->ruler_y = NULL;
    box->priv->graphs = NULL;
    box->priv->markers = NULL;    
    box->priv->factor_x = 0;
    box->priv->factor_y = 0;
    box->priv->zoom_limit = 20;
    box->priv->zoom = 1;
    box->priv->offset_x = 0;
    box->priv->ctime = 0;
    box->priv->cx = 0.0;
    box->priv->draw_cross = TRUE;
    box->priv->negative_value = FALSE;
    box->priv->store_path = NULL;
    box->priv->label_text = NULL;
    gtk_chartbox_set_adjustment_x (box, NULL);

    /*For Vigil graph*/
    box->priv->self_box = NULL;
    box->priv->sibl_box = NULL;    
    box->priv->text = NULL;        
}

static gint
gtk_chartbox_motion_notify (GtkWidget * widget, GdkEventMotion * event){
    g_return_val_if_fail(GTK_IS_CHARTBOX(widget), -1);

    gfloat pos_val=0.0;
    gfloat pos_time=0.0;
    guint time=0;
    if (GTK_CHARTBOX_RULER(GTK_CHARTBOX(widget)->priv->ruler_x))
	time = gtk_chartbox_ruler_get_position(GTK_CHARTBOX(widget)->priv->ruler_x, &pos_time );
    if (GTK_CHARTBOX_RULER(GTK_CHARTBOX(widget)->priv->ruler_y))
	gtk_chartbox_ruler_get_position(GTK_CHARTBOX(widget)->priv->ruler_y, &pos_val );    

    /*Сохраним значение времени*/
    GTK_CHARTBOX(widget)->priv->ctime = time;
    GTK_CHARTBOX(widget)->priv->cx = event->x;

    /*нарисуем легенду - время возле вертикальной линии*/
    gtk_chartbox_draw_time(GTK_CHARTBOX(widget), event->x );
    /*нарисуем линию значений*/
    gtk_chartbox_draw_pos(GTK_CHARTBOX(widget), event->x );
    /*Расчитаем значение по графикам в указанный момент времени*/
    gtk_chartbox_calculate_values(GTK_CHARTBOX(widget), time );

    g_signal_emit (G_OBJECT (widget), gtk_chartbox_signals[MOTION], 0, event );
    
    return 0;
}
void gtk_chartbox_set_motion_event(GtkChartBox *box, GdkEventMotion* event ){
    g_return_if_fail( box );

    /*Сгенерить события на осях значения и времени*/
    if (GTK_CHARTBOX_RULER(box->priv->ruler_y))
	gtk_widget_event(GTK_WIDGET(box->priv->ruler_y), (GdkEvent*)(event) );
    if (GTK_CHARTBOX_RULER(box->priv->ruler_x))
	gtk_widget_event(GTK_WIDGET(box->priv->ruler_x), (GdkEvent*)(event) );

    gfloat pos_val=0.0;
    gfloat pos_time=0.0;
    guint time=0;
    if (GTK_CHARTBOX_RULER(box->priv->ruler_x))
	time = gtk_chartbox_ruler_get_position(box->priv->ruler_x, &pos_time );
    if (GTK_CHARTBOX_RULER(box->priv->ruler_y))
	gtk_chartbox_ruler_get_position(box->priv->ruler_y, &pos_val );    

    /*нарисуем легенду*/
    gtk_chartbox_draw_time(box, event->x );
    /*нарисуем линию значеинй*/
    gtk_chartbox_draw_pos(box, event->x );
    /*Расчитаем значение по графикам в указанный момент времени*/
    gtk_chartbox_calculate_values(box, time );

}
static void
gtk_chartbox_draw_pos(GtkChartBox *box, gint x){
    if (!gtk_widget_is_drawable( GTK_WIDGET(box)))
      return;
    if (x < 10)
	return;
    GtkWidget *widget = GTK_WIDGET(box);

    GtkAllocation allocation;
    gtk_widget_get_allocation( widget, &allocation );

    gint bs_width = 2;
    gint bs_height = allocation.height - 2;

    cairo_t *cr = gdk_cairo_create ( gtk_widget_get_window(widget) );

    if (box->priv->backing_surface)
    {
	cairo_set_source_surface(cr, box->priv->backing_surface, 0, 0 );
	cairo_rectangle( cr, box->priv->xsrc-1, box->priv->ysrc, bs_width, bs_height );
	cairo_fill(cr);
    }
    
    gint x_draw = x;
    gdk_cairo_set_source_color (cr, &gtk_widget_get_style(widget)->fg[gtk_widget_get_state(widget)]);
    cairo_set_line_width(cr, 1);
    cairo_move_to (cr, x_draw, 1);
    cairo_line_to (cr, x_draw, bs_height );
    cairo_stroke(cr);
    cairo_destroy(cr);
    box->priv->xsrc = x_draw;
    box->priv->ysrc = 1;

}
static void
gtk_chartbox_draw_time(GtkChartBox *box, gint x ){
    if (!gtk_widget_is_drawable( GTK_WIDGET(box) ) )
      return;
    if ( !box->priv->ruler_y ) 
	return;
    if (x < 10)
	return;

    GtkWidget *widget = GTK_WIDGET(box);
    cairo_t *cr = gdk_cairo_create (gtk_widget_get_window(widget));
    
    gint bs_width = 60;
    gint bs_height = 20;
    gint offset = -52;	
    if (x < bs_width ){
	offset = 1;
    }	

    if (box->priv->backing_surface){
	cairo_set_source_surface(cr, box->priv->backing_surface, 0, 0 );
	cairo_rectangle( cr, box->priv->xsrc-1-bs_width, box->priv->ysrc, 2*bs_width, bs_height);
	cairo_fill(cr);
    }
    
    gint x_draw = x; 
    gdk_cairo_set_source_color (cr, &gtk_widget_get_style(widget)->fg[gtk_widget_get_state(widget)]);
    cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size (cr, 10);
    cairo_move_to (cr, x+offset, 15 );
    struct tm tmt;
    float flt;
    time_t timer = box->priv->ctime;
    localtime_r(&timer, &tmt );
    gchar buff[15];
    g_snprintf(buff, sizeof(buff),"%.2d:%.2d:%.2d", tmt.tm_hour, tmt.tm_min, tmt.tm_sec  );
    cairo_show_text (cr, buff );
    cairo_stroke(cr);
    cairo_destroy(cr);
}
static void
gtk_chartbox_calculate_values(GtkChartBox *box, guint time){
    g_return_if_fail(GTK_IS_CHARTBOX(box));

    GList *list = g_list_last (box->priv->graphs);
    while (list){
      if (list->data){
        gfloat result = 0; 
	gtk_chartbox_graph_calculate_value (GTK_CHARTBOX_GRAPH (list->data), &time, &result );
	gtk_chartbox_graph_set_value(GTK_CHARTBOX_GRAPH (list->data), &result);
      }
      list = g_list_previous (list);
    }

}

static void 
gtk_chartbox_get_preferred_width (GtkWidget *widget, gint *minimum_size, gint *natural_size ){
  g_return_if_fail(widget != NULL);
  g_return_if_fail(minimum_size != NULL);
  g_return_if_fail(natural_size != NULL);
  GtkAllocation allocation;
  gtk_widget_get_allocation( widget, &allocation );
  *minimum_size = allocation.width;
  *natural_size = allocation.width; 
}

static void 
gtk_chartbox_get_preferred_height (GtkWidget *widget, gint *minimum_size, gint *natural_size){
  g_return_if_fail(widget != NULL);
  g_return_if_fail(minimum_size != NULL);
  g_return_if_fail(natural_size != NULL);
  GtkAllocation allocation;
  gtk_widget_get_allocation( widget, &allocation );
  *minimum_size = allocation.height;
  *natural_size = allocation.height; 
}

static void 
gtk_chartbox_size_request(GtkWidget *widget, GtkRequisition *requisition){
  g_return_if_fail(widget != NULL);
  g_return_if_fail(requisition != NULL);

  GtkAllocation allocation;
  gtk_widget_get_allocation( widget, &allocation );
  requisition->width = allocation.width;
  requisition->height = allocation.height;
}

static void 
gtk_chartbox_size_allocate(GtkWidget *widget, GtkAllocation *allocation){
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_CHARTBOX(widget));
    g_return_if_fail(allocation != NULL);

    gtk_widget_set_allocation ( widget, allocation );

    if ( gtk_widget_get_realized (widget) )  
    {
	 gdk_window_move_resize( gtk_widget_get_window(widget), allocation->x, allocation->y, allocation->width, allocation->height );
	 gtk_chartbox_create_backing_surface( GTK_CHARTBOX(widget));
    }
}

static void 
gtk_chartbox_realize(GtkWidget *widget){
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_CHARTBOX(widget));

    GdkWindowAttr attributes;
    guint attributes_mask;
    GdkWindow *window;
    GtkAllocation allocation;

    gtk_widget_set_realized (widget, TRUE );
    gtk_widget_get_allocation( widget, &allocation );


    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.visual = gtk_widget_get_visual (widget);
#if (GTK_MAJOR_VERSION == 2)
    attributes.colormap = gtk_widget_get_colormap (widget);
#endif      
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.event_mask |= (GDK_EXPOSURE_MASK | GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK);
#if (GTK_MAJOR_VERSION == 3)
    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;
#else
    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
#endif  
    window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
    gtk_widget_set_window (widget, window);
    gdk_window_set_user_data(gtk_widget_get_window(widget), widget );

#if (GTK_MAJOR_VERSION == 2)
    gtk_widget_set_style(widget, gtk_style_attach (gtk_widget_get_style (widget), window));
    gtk_style_set_background(gtk_widget_get_style(widget), gtk_widget_get_window(widget), GTK_STATE_NORMAL);
#endif      

    gtk_chartbox_create_backing_surface(GTK_CHARTBOX(widget));
/*    gdk_window_set_cursor (gtk_widget_get_window(widget), gdk_cursor_new (GDK_CROSS));
*/
}


static void 
gtk_chartbox_create_backing_surface (GtkChartBox * box){

    GtkWidget *widget = GTK_WIDGET (box);
    GtkAllocation allocation;
    gtk_widget_get_allocation( widget, &allocation );

    if (box->priv->backing_surface)
    {
      if ((cairo_image_surface_get_width(box->priv->backing_surface) == allocation.width) &&
	  (cairo_image_surface_get_height(box->priv->backing_surface) == allocation.height))
	 return;

      cairo_surface_destroy (box->priv->backing_surface);
    }

    box->priv->backing_surface = cairo_image_surface_create ( CAIRO_FORMAT_RGB24, allocation.width, allocation.height );
    box->priv->xsrc = 0;
    box->priv->ysrc = 0;
}


static void 
gtk_chartbox_unrealize (GtkWidget * widget){
   GtkChartBox *box = GTK_CHARTBOX (widget);

   if (box->priv->backing_surface)
      cairo_surface_destroy(box->priv->backing_surface);
   if (box->priv->adj_x)
      g_object_unref (box->priv->adj_x);
   if (box->priv->graphs)
      g_list_free (box->priv->graphs);

   g_free (box->priv);

   if (GTK_WIDGET_CLASS (gtk_chartbox_parent_class)->unrealize)
      (*GTK_WIDGET_CLASS (gtk_chartbox_parent_class)->unrealize) (widget);

}
#if (GTK_MAJOR_VERSION == 3)
static gboolean
gtk_chartbox_draw (GtkWidget *widget, cairo_t *cr)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_CHARTBOX(widget), FALSE);
    
    GtkChartBox *box = GTK_CHARTBOX (widget);
    /*пересчитываем факторы трансляции */
    gtk_chartbox_calculate_factors(box);
    /*рисуем графы в области */
    gtk_chartbox_paint( box );
    /*копируем картинку из box->surface в окно */
    cairo_set_source_surface (cr, GTK_CHARTBOX(widget)->priv->backing_surface, 0, 0);
    cairo_paint (cr);

    /*Восстановим курсорную линию и метку сремени*/
    /*нарисуем легенду - время возле вертикальной линии*/
    gtk_chartbox_draw_time(GTK_CHARTBOX(widget), GTK_CHARTBOX(widget)->priv->cx );
    /*Нарисуем курсорную  линию значений*/
    gtk_chartbox_draw_pos(GTK_CHARTBOX(widget), GTK_CHARTBOX(widget)->priv->cx );

  return FALSE;
}
#else
static gboolean 
gtk_chartbox_expose(GtkWidget *widget, GdkEventExpose *event){
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_CHARTBOX(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);
    GtkChartBox *box = GTK_CHARTBOX (widget);
    /*пересчитываем факторы трансляции */
    gtk_chartbox_calculate_factors(box);
    /*рисуем графы в области */
    gtk_chartbox_paint( box );
    
    /*копируем картинку из box->surface в окно */
    cairo_t *cr = gdk_cairo_create (gtk_widget_get_window(widget));
    cairo_set_source_surface (cr, GTK_CHARTBOX(widget)->priv->backing_surface, 0, 0);
    gdk_cairo_rectangle (cr, &event->area);
    cairo_fill (cr);
    cairo_destroy (cr);	

    /*Восстановим курсорную линию и метку сремени*/
    /*нарисуем легенду - время возле вертикальной линии*/
    gtk_chartbox_draw_time(GTK_CHARTBOX(widget), GTK_CHARTBOX(widget)->priv->cx );
    /*Нарисуем курсорную  линию значений*/
    gtk_chartbox_draw_pos(GTK_CHARTBOX(widget), GTK_CHARTBOX(widget)->priv->cx );

  return FALSE;
}
#endif
static void 
gtk_chartbox_paint(GtkChartBox *box ){

    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );

    box->priv->content = cairo_create(box->priv->backing_surface); 
    cairo_set_source_rgb(box->priv->content, 0.5, 0.5, 0.5);
    GdkColor _color;
    gdk_color_parse("#bfbfbf", &_color);
    gdk_cairo_set_source_color (box->priv->content, &_color);
    cairo_paint(box->priv->content);
    cairo_translate (box->priv->content, 10 - box->priv->offset_x, allocation.height - 10 );
    cairo_rotate (box->priv->content, M_PI*1.5);
    /*нарисуем оси координат */
    if (box->priv->draw_cross){	
	cairo_set_source_rgb(box->priv->content, 0., 0., 0.1);
	guint zero_Y = 0;
	/*Правильно нарисуем линию Y, в случае минусовых значений.*/
	if ( box->priv->min_y < 0 && ( box->priv->min_y != -1 && box->priv->max_y != -1 ) ){
	    zero_Y = ( (-1*box->priv->min_y)/(box->priv->visible_y - box->priv->min_y) ) * allocation.height; 
	}
	cairo_move_to(box->priv->content, zero_Y, -10);
	cairo_line_to(box->priv->content, zero_Y, allocation.width * box->priv->zoom );
	cairo_move_to(box->priv->content, -10, 0);
	cairo_line_to(box->priv->content, allocation.height, 0);
	cairo_stroke(box->priv->content);
    }
    /*Нарисуем графики*/
    GList *list = g_list_last (box->priv->graphs);
    while (list){
      if (list->data)
      {
	 gtk_chartbox_graph_draw (GTK_CHARTBOX_GRAPH (list->data), box);
      }
      list = g_list_previous (list);
    }
    /*Нариcуем маркеры*/
    list = g_list_last (box->priv->markers);
    while (list){
      if (list->data)
      {
	 gtk_chartbox_marker_draw (GTK_CHARTBOX_MARKER (list->data), box);
      }
      list = g_list_previous (list);
    }
    /*Напишем текст по середине*/
    if ( box->priv->label_text )
    {
	GdkRGBA rgba;
	//rgba.alpha=1;rgba.red=0;rgba.green=0;rgba.blue=1;
	gdk_rgba_parse( &rgba, "#8200ff" );
        gdk_cairo_set_source_rgba (box->priv->content, &rgba );    
	cairo_select_font_face (box->priv->content, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
        cairo_set_font_size (box->priv->content, 16);
	cairo_move_to(box->priv->content, allocation.height*0.05, allocation.width*0.05 );
	/*Выпровнием матрицу*/
	cairo_matrix_t cm;
	cairo_get_font_matrix(box->priv->content, &cm);
	cairo_matrix_rotate(&cm, 1.57);
	cairo_set_font_matrix(box->priv->content, &cm);
	cairo_show_text (box->priv->content, box->priv->label_text);

	cairo_stroke(box->priv->content);
    }

    cairo_destroy(box->priv->content);    
}
static void
gtk_chartbox_ruler_update (GtkChartBox * box)
{
   if (box->priv->ruler_x)
   {
      gtk_chartbox_ruler_set_range (
            GTK_CHARTBOX_RULER (box->priv->ruler_x),
               (gfloat)box->priv->min_x,
               (gfloat)box->priv->visible_x,
               (gfloat)(0.5 * (box->priv->visible_x - box->priv->min_x)+box->priv->min_x));
   }

   if (box->priv->ruler_y)
   {
      gtk_chartbox_ruler_set_range (
            GTK_CHARTBOX_RULER (box->priv->ruler_y),
	      box->priv->min_y,
	      box->priv->visible_y,
              0.5 * (box->priv->visible_y - box->priv->min_y)+box->priv->min_y);	      
   }
}

/*-----внешние функции-----------*/
void 
gtk_chartbox_set_store_path( GtkChartBox *box, gchar const* str )
{
    g_return_if_fail (GTK_IS_CHARTBOX(box));
    box->priv->store_path = str;
}	


void 
gtk_chartbox_set_negative_value(GtkChartBox *box, gboolean nv ){
    g_return_if_fail (GTK_IS_CHARTBOX(box));
    box->priv->negative_value = nv;
}
gboolean 
gtk_chartbox_get_negative_value(GtkChartBox *box ){
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), FALSE );
    return box->priv->negative_value;
}
cairo_surface_t* 
gtk_chartbox_get_backing_surface(GtkChartBox * box){
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), NULL);
    return box->priv->backing_surface;
    
}
cairo_t* 
gtk_chartbox_get_backing_content(GtkChartBox * box){
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), NULL);
    return box->priv->content;
    
}
void
gtk_chartbox_set_label_text (GtkChartBox * box, gchar const *text)
{
    g_return_if_fail (GTK_IS_CHARTBOX(box));
    g_return_if_fail (text);
    box->priv->label_text = text;
}

void
gtk_chartbox_set_text (GtkChartBox * box, gchar const *text)
{
   g_return_if_fail (GTK_IS_CHARTBOX(box));
   g_return_if_fail (text);
   /*Уберем пробелы если они есть*/
   GString *label = g_string_new (NULL);
   const gchar* cp = text;
   while (*cp){
	if ((*cp) != ' ' )
	    g_string_append_c (label, *cp);
	cp++;	
   }
   box->priv->text = g_string_free (label, FALSE);
}
gchar const*
gtk_chartbox_get_text (GtkChartBox* box)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX(box), NULL);
   return box->priv->text;
}

/* -----------Линия прокрутки ----------*/
void
gtk_chartbox_set_adjustment_x (GtkChartBox * box, GtkAdjustment * adj){
   if (!adj)
      adj = GTK_ADJUSTMENT(gtk_adjustment_new (0, 0, 0, 0, 0, 0));
   g_return_if_fail (GTK_IS_CHARTBOX (box));
   g_return_if_fail (GTK_IS_ADJUSTMENT (adj));

   box->priv->adj_x = adj;
   g_object_ref (box->priv->adj_x);

   /* We always scroll from 0 to 1.0 */
   gtk_adjustment_set_lower( box->priv->adj_x, 0 );
   gtk_adjustment_set_value( box->priv->adj_x, 0 );   
   gtk_adjustment_set_upper( box->priv->adj_x, 1 );
   gtk_adjustment_set_page_size( box->priv->adj_x, 1 );
   gtk_adjustment_set_step_increment ( box->priv->adj_x, gtk_adjustment_get_page_size( box->priv->adj_x ) / 20 );
   gtk_adjustment_set_page_increment ( box->priv->adj_x, gtk_adjustment_get_page_size( box->priv->adj_x ) * 0.9 );
   gtk_adjustment_changed (box->priv->adj_x);

   g_signal_connect_swapped (G_OBJECT (box->priv->adj_x), "value_changed",
			     G_CALLBACK
			     (gtk_chartbox_adjustment_value_changed), box);
}

GtkAdjustment *
gtk_chartbox_get_adjustment_x (GtkChartBox * box){
   g_return_val_if_fail (GTK_IS_CHARTBOX (box), NULL);
   g_return_val_if_fail (GTK_IS_ADJUSTMENT (box->priv->adj_x), NULL);
    return box->priv->adj_x;
}
/*--------------Рисование осей Х У ---------------*/
void gtk_chartbox_draw_cross( GtkChartBox *box, gboolean _dr ){
    g_return_if_fail (GTK_IS_CHARTBOX (box));
    GTK_CHARTBOX(box)->priv->draw_cross = _dr;
}

/*--------------Скроллинг графика-----------------*/
static void 
gtk_chartbox_adjustment_value_changed (GtkChartBox * box){
    g_return_if_fail (GTK_IS_CHARTBOX(box) );

    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );
    GTK_CHARTBOX(box)->priv->offset_x = allocation.width * ( gtk_adjustment_get_value( box->priv->adj_x ) / ( gtk_adjustment_get_upper( box->priv->adj_x) - 1 ) );
    GTK_CHARTBOX(box)->priv->offset_x *= (GTK_CHARTBOX(box)->priv->zoom-1);
    
    if (box->priv->ruler_x){
	/*Передать сдвиг в ось времени */
	gtk_chartbox_ruler_set_offset (GTK_CHARTBOX(box)->priv->ruler_x, GTK_CHARTBOX(box)->priv->offset_x );    
    }


/*Обновим графики для дежурного режима*/
    if (box->priv->self_box){
	box->priv->self_box->priv->offset_x = GTK_CHARTBOX(box)->priv->offset_x;
	gtk_widget_queue_draw (GTK_WIDGET(box->priv->self_box));
    }	
    if (box->priv->sibl_box){
	box->priv->sibl_box->priv->offset_x = GTK_CHARTBOX(box)->priv->offset_x;
	gtk_widget_queue_draw (GTK_WIDGET(box->priv->sibl_box));
    }
    g_signal_emit (G_OBJECT (box), gtk_chartbox_signals[OFFSET], 0, gtk_adjustment_get_value( box->priv->adj_x ) );
    gtk_widget_queue_draw (GTK_WIDGET(box));

}

void 
gtk_chartbox_set_offset(GtkChartBox *box, gfloat offset ){
    g_return_if_fail (GTK_IS_CHARTBOX(box) );

    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );
/*Пересчитаем смещение графика из значения положения полосы прокрутки*/
    box->priv->offset_x = allocation.width * ( offset / ( gtk_adjustment_get_upper( box->priv->adj_x ) - 1) );    
    box->priv->offset_x *= (box->priv->zoom-1);
/*Обновим значение положения полосы прокрутки*/    
    gtk_adjustment_set_value( box->priv->adj_x, offset );
    gtk_adjustment_changed (box->priv->adj_x);    
    
    if (box->priv->ruler_x){
	/*Передать сдвиг в ось времени */
	gtk_chartbox_ruler_set_offset (GTK_CHARTBOX(box)->priv->ruler_x, GTK_CHARTBOX(box)->priv->offset_x );    
    }
	/*Обновим графики для дежурного режима*/
    if (box->priv->self_box)
	gtk_chartbox_set_offset(GTK_CHARTBOX(box)->priv->self_box, GTK_CHARTBOX(box)->priv->offset_x );
    if (box->priv->sibl_box)
	gtk_chartbox_set_offset(GTK_CHARTBOX(box)->priv->sibl_box, GTK_CHARTBOX(box)->priv->offset_x );

    gtk_widget_queue_draw (GTK_WIDGET(box));

}
gfloat
gtk_chartbox_get_offset(GtkChartBox *box){
    g_return_if_fail (GTK_IS_CHARTBOX(box) );
    return gtk_adjustment_get_value( box->priv->adj_x );
}
/*-----------Операции масштабирование---------*/
gint
gtk_chartbox_get_zoom(GtkChartBox *box){
    g_return_if_fail (GTK_IS_CHARTBOX(box) );
    return GTK_CHARTBOX(box)->priv->zoom;
}

void 
gtk_chartbox_set_zoom(GtkChartBox *box, gint zoom ){
    g_return_if_fail (GTK_IS_CHARTBOX(box) );
    if (box->priv->zoom != zoom)
	box->priv->zoom = zoom;

/*Обновить данные по zoom в маркерах */
    GList *list = g_list_last (box->priv->markers);
    while (list){
      if (list->data)
      {
	 gtk_chartbox_marker_set_zoom (GTK_CHARTBOX_MARKER (list->data), box->priv->zoom );
      }
      list = g_list_previous (list);
    }

/*Обновить полосу прокрутки*/
    gtk_adjustment_set_value( box->priv->adj_x, 0 );
    gtk_adjustment_set_upper( box->priv->adj_x, (gfloat)box->priv->zoom );
    gtk_adjustment_changed (box->priv->adj_x);    
/*Обновить ось значений*/
    box->priv->offset_x = 0.0;	
    if (box->priv->ruler_x){
	gtk_chartbox_ruler_set_offset (GTK_CHARTBOX(box)->priv->ruler_x, box->priv->offset_x );
    /*Надо вызвать ExposeEvent на box->priv->ruler_x */
	gtk_chartbox_ruler_set_zoom (box->priv->ruler_x, box->priv->zoom);
	gtk_widget_queue_draw (GTK_WIDGET(box->priv->ruler_x));
    }

/*Вызовим ExposeEvent */
    gtk_widget_queue_draw (GTK_WIDGET(box));
    
/*Обновим графики для дежурного режима*/
    if (box->priv->self_box)
	gtk_chartbox_set_zoom(box->priv->self_box, zoom);
    if (box->priv->sibl_box)
	gtk_chartbox_set_zoom(box->priv->sibl_box, zoom);
}
void 
gtk_chartbox_zoom_in(GtkChartBox *box){
   g_return_if_fail (GTK_IS_CHARTBOX (box));

    if (box->priv->zoom < box->priv->zoom_limit)
	box->priv->zoom++;
    
    gtk_chartbox_set_zoom(box, box->priv->zoom );

/*Пошлем сигнал*/
   g_signal_emit (G_OBJECT (box), gtk_chartbox_signals[ZOOM], 0, box->priv->zoom );

}
void 
gtk_chartbox_zoom_out(GtkChartBox *box){
   g_return_if_fail (GTK_IS_CHARTBOX (box));
    if (box->priv->zoom > 1)
	box->priv->zoom--;

    gtk_chartbox_set_zoom(box, box->priv->zoom );

/*Пошлем сигнал*/
   g_signal_emit (G_OBJECT (box), gtk_chartbox_signals[ZOOM], 0, box->priv->zoom );
}
void 
gtk_chartbox_zoom_home(GtkChartBox *box){
   g_return_if_fail (GTK_IS_CHARTBOX (box));
	box->priv->zoom = 1;

    gtk_chartbox_set_zoom(box, box->priv->zoom );

/*Пошлем сигнал*/
   g_signal_emit (G_OBJECT (box), gtk_chartbox_signals[ZOOM], 0, box->priv->zoom );
}
/*------------Обновление графика---------------*/
void 
gtk_chartbox_update(GtkChartBox *box){
    g_return_if_fail (GTK_IS_CHARTBOX (box));
    gtk_chartbox_calculate_extrema(box);
    gtk_chartbox_ruler_update (box);
    gtk_widget_queue_draw (GTK_WIDGET(box->priv->ruler_x));
    gtk_widget_queue_draw (GTK_WIDGET(box->priv->ruler_y));
    gtk_widget_queue_draw (GTK_WIDGET(box));

}

/*-----------Оси по вертикале и горизонтале ------*/
void
gtk_chartbox_set_ruler (GtkChartBox * box, GtkChartBoxRuler * ruler)
{
   g_return_if_fail (GTK_IS_CHARTBOX (box));
   g_return_if_fail (ruler == NULL || GTK_IS_CHARTBOX_RULER (ruler));

   GtkOrientation _orient;
   _orient = gtk_chartbox_ruler_get_orientation(ruler);

   if (!box->priv->ruler_x && (_orient == GTK_ORIENTATION_HORIZONTAL) )
   {
     box->priv->ruler_x = ruler;

      g_signal_connect_swapped (box, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box->priv->ruler_x)-> motion_notify_event),
				G_OBJECT (box->priv->ruler_x) );
    }

   if (!box->priv->ruler_y && (_orient == GTK_ORIENTATION_VERTICAL) )
   {
     box->priv->ruler_y = ruler;


      g_signal_connect_swapped (box, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box->priv->ruler_y)-> motion_notify_event),
				G_OBJECT (box->priv->ruler_y) );
    }
    gtk_chartbox_ruler_update (box);
}
/*Операции с маркерами-------------------------------------------------*/
gint
gtk_chartbox_marker_add (GtkChartBox * box, GtkChartBoxMarker * marker)
{
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);
    g_return_val_if_fail (GTK_IS_CHARTBOX_MARKER (marker), -1);

    box->priv->markers = g_list_append (box->priv->markers, marker);
    return (box->priv->markers == NULL) ? -1 : 0;
}
gint
gtk_chartbox_marker_remove (GtkChartBox * box, GtkChartBoxMarker * marker)
{
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (marker), -1);
    GList *list;
    list = g_list_find (box->priv->markers, marker);
    g_return_val_if_fail (list, -1);

    box->priv->markers = g_list_delete_link (box->priv->markers, list);
    return 0;
}
gint
gtk_chartbox_marker_remove_all (GtkChartBox * box)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);
   g_list_free (box->priv->markers);

   box->priv->markers = 0;
   return 0;
}

/*Операции с графиками-------------------------------------------------*/
gint
gtk_chartbox_graph_add (GtkChartBox * box, GtkChartBoxGraph * graph)
{
    g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);

    box->priv->graphs = g_list_append (box->priv->graphs, graph);
    gtk_chartbox_calculate_extrema(box);
    gtk_chartbox_ruler_update (box);        
    return (box->priv->graphs == NULL) ? -1 : 0;
}
gint
gtk_chartbox_graph_remove (GtkChartBox * box, GtkChartBoxGraph * graph)
{
   GList *list;

    g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);
    g_return_val_if_fail (GTK_IS_CHARTBOX_GRAPH (graph), -1);

    list = g_list_find (box->priv->graphs, graph);
    g_return_val_if_fail (list, -1);


    box->priv->graphs = g_list_delete_link (box->priv->graphs, list);

    gtk_chartbox_calculate_extrema(box);
    gtk_chartbox_ruler_update (box);
    
   return 0;
}
gint
gtk_chartbox_graph_remove_all (GtkChartBox * box)
{
   g_return_val_if_fail (GTK_IS_CHARTBOX (box), -1);

   g_list_free (box->priv->graphs);

   box->priv->graphs = 0;

   return 0;
}
void
gtk_chartbox_values_to_pixels (GtkChartBox * box, guint len,  const guint * values_x,   const gfloat * values_y,  GdkPoint * pixels)
{
    guint i;
    for (i = 0; i < len; ++i, ++values_x, ++values_y, ++pixels){
	 pixels->y = (gint)((*values_x - box->priv->min_x) * box->priv->factor_x * box->priv->zoom);
	 pixels->x = (*values_y - box->priv->min_y) * box->priv->factor_y; 
    }

}

static void 
gtk_chartbox_calculate_factors(GtkChartBox * box){
    g_return_if_fail(GTK_IS_CHARTBOX(box) );
    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );

    /*пересчитываем фактор перевода pix2val*/
    if ((box->priv->visible_x - box->priv->min_x) != 0 )
	box->priv->factor_x = (gfloat)(allocation.width)/(gfloat)(box->priv->visible_x - box->priv->min_x); 
    else 	
	box->priv->factor_x = 1;
    if ((box->priv->visible_y - box->priv->min_y) != 0 )
        box->priv->factor_y = allocation.height / (box->priv->visible_y - box->priv->min_y); 
    else 
    	box->priv->factor_y = 1;
}

static void 
gtk_chartbox_calculate_extrema(GtkChartBox * box){
    g_return_if_fail (GTK_IS_CHARTBOX (box));

    box->priv->max_y=0.0;
    box->priv->min_y=0.0;
    box->priv->max_x=0;
    box->priv->min_x=0;
/*расчет экстрем на основании данных графиков*/    
    GList *list = g_list_last (box->priv->graphs);
    gfloat min_y, max_y;
    guint min_x, max_x;
    gboolean init_extr = FALSE; /*Прежде чем начать считать эектремы надо инициализировать их не нулями, а эекстремами последнего графика*/
    while (list){
	if (list->data){
	    if (!gtk_chartbox_graph_get_hide(GTK_CHARTBOX_GRAPH (list->data))){
	        if ( !init_extr ){
		    if ( !gtk_chartbox_graph_calculate_extrema (GTK_CHARTBOX_GRAPH (list->data), &box->priv->min_x, &box->priv->max_x, &box->priv->min_y, &box->priv->max_y ) )
		        init_extr = TRUE;
		}
		else{
		    if ( !gtk_chartbox_graph_calculate_extrema (GTK_CHARTBOX_GRAPH (list->data), &min_x, &max_x, &min_y, &max_y ) ){	
			if (box->priv->min_x > min_x)
			    box->priv->min_x = min_x;
			if (box->priv->max_x < max_x)
			    box->priv->max_x = max_x;

			if (box->priv->min_y > min_y)
			    box->priv->min_y = min_y;
			if (box->priv->max_y < max_y)
			    box->priv->max_y = max_y;
		    }
		}

	    }
        }
      list = g_list_previous (list);
    }
/*расчет экстрем на основании данных линий маркеров*/    
    GList *mark = g_list_last (box->priv->markers);
    while (mark){
	if (mark->data){
	    if (!gtk_chartbox_marker_get_hide(GTK_CHARTBOX_MARKER (mark->data)) && gtk_chartbox_marker_get_line(GTK_CHARTBOX_MARKER (mark->data)) ){
		gfloat *Y = gtk_chartbox_marker_get_Y(GTK_CHARTBOX_MARKER (mark->data));

		if (box->priv->max_y < *Y)
		    box->priv->max_y = *Y;	    
		if (box->priv->min_y > *Y)
		    box->priv->min_y = *Y;
	    }
	}
	mark = g_list_previous (mark);
    }
    /*Если min_y оказался меньше нуля и не допустимо наличие минусовых значений на графике - то сделать равным 0*/
    if(box->priv->min_y < 0 && !box->priv->negative_value ){
	    box->priv->min_y = 0.0;	
    }	
    else if (box->priv->min_y > 0){
	box->priv->min_y = 0.99*box->priv->min_y;

    }
	
    			
    /*Делаем запас по отображаемому значению*/
    box->priv->visible_x=(0.03*(box->priv->max_x - box->priv->min_x )) + box->priv->max_x;
    box->priv->visible_y=(0.1*(box->priv->max_y - box->priv->min_y )) + box->priv->max_y;   
    /*если запас получаесятся больше 50(box->priv->visible_x-box->priv->max_x)
    * То все сворачиваестя в линию которая расположена на оси значений
    */
    if ((box->priv->visible_x - box->priv->max_x) > 50){
	box->priv->visible_x = box->priv->max_x + 50;
    }
//g_print("minY %2.2f maxY %2.2f, visibleY %2.2f\n",box->priv->min_y, box->priv->max_y, box->priv->visible_y);		        
//g_print("minX %lu maxX %lu, visibleX %lu diff(vis-max): %d diff(max-min): %d\n",box->priv->min_x, box->priv->max_x, box->priv->visible_x, box->priv->visible_x-box->priv->max_x, box->priv->max_x-box->priv->min_x );   

}
/*---------Saving func--------------*/
void
gtk_chartbox_save(GtkChartBox *box){

    g_return_if_fail (GTK_IS_CHARTBOX (box));
    gchar fname[200];
    g_snprintf( fname, sizeof(fname), "trend_%lu", time(NULL) );

    GtkWidget *widget = GTK_WIDGET(box);
    GtkAllocation allocation;
    gtk_widget_get_allocation( widget, &allocation );

    if  (box->priv->store_path )
    {
	gchar buff[2048];
	snprintf(buff,sizeof(buff), "%s/%s.png", box->priv->store_path, fname );
    	gtk_chartbox_process_save(box, buff, &allocation );   
     	return;
    }

//Создается диалог выбора места сохранения тренда
    GtkWidget *_dialog = gtk_file_chooser_dialog_new("Сохранить",  NULL, GTK_FILE_CHOOSER_ACTION_SAVE, 
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, 
                                    			GTK_STOCK_OK, GTK_RESPONSE_NONE, NULL);							

    gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (_dialog), fname );//"Untitled");
//Добавляем фильты по типу файлов
    GtkFileFilter *ffilter = gtk_file_filter_new();
    ffilter = gtk_file_filter_new();
    gtk_file_filter_set_name( ffilter, "Рисунок PNG");
    gtk_file_filter_add_pattern( ffilter, "*.png");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER (_dialog), ffilter);
    
    ffilter = gtk_file_filter_new();
    gtk_file_filter_set_name( ffilter, "Файл данных MathLab");
    gtk_file_filter_add_pattern( ffilter, "*.dat");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER (_dialog), ffilter);

    ffilter = gtk_file_filter_new();
    gtk_file_filter_set_name( ffilter, "Файл данных Excel");
    gtk_file_filter_add_pattern( ffilter, "*.csv");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER (_dialog), ffilter);
//Отлавливаем конец выбора и работаем
    gtk_window_set_keep_above( GTK_WINDOW( _dialog ), TRUE );
    gint result = gtk_dialog_run (GTK_DIALOG (_dialog) );
    gchar buff[2048];
    gchar *fil = NULL; 
    gchar *dir = NULL;
    gchar *filename = NULL;
    if (result == GTK_RESPONSE_NONE){
	    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (_dialog));
	    fil =(char*) gtk_file_filter_get_name( gtk_file_chooser_get_filter(GTK_FILE_CHOOSER (_dialog)) );
	    dir = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (_dialog)); 
	    if (!strcmp(fil, "Рисунок PNG" ) ) 
	        snprintf(buff,sizeof(buff), "%s.png", filename );
	    else if (!strcmp(fil, "Файл данных MathLab" ) ) 
	        snprintf(buff,sizeof(buff), "%s.dat", filename );
	    else if (!strcmp(fil, "Файл данных Excel" ) ) 	
		snprintf(buff,sizeof(buff), "%s.csv", filename );
	    else
	        snprintf(buff,sizeof(buff), "%s.png", filename );

    }
/*Если юзер передумал сохраняться*/
    else{
	    gtk_widget_destroy (_dialog);
	    return;
    }
    FILE *fp;
    if ((fp=fopen(buff, "w")) == NULL ){
	    gchar buff[300];
	    gchar *homedir = (gchar*)getenv("HOME");
	    snprintf(buff, sizeof(buff), "Отказано в доступе.\nСохранить файл в %s  не удалось. \nВыберите, другой каталог, \nнапример: %s",filename, homedir );
	    GtkWidget* dialog = gtk_message_dialog_new (GTK_WINDOW (NULL),
				      (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT), GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
				      "%s", buff );
	    gtk_dialog_run (GTK_DIALOG (dialog));
	    gtk_widget_destroy (dialog);
    }
    else{
	    if ( !strcmp(fil, "Файл данных MathLab" )  ) gtk_chartbox_dat_save (fp,  box );
	    if ( !strcmp(fil, "Файл данных Excel" )  ) gtk_chartbox_csv_save (fp,  box );
	    
	    fclose(fp);
	    if (!strcmp(fil, "Рисунок PNG" ) ) gtk_chartbox_process_save(box, buff, &allocation );   
    }
    gtk_widget_destroy (_dialog);
}

static void 
gtk_chartbox_process_save( GtkChartBox* box,  gchar const* filepath, GtkAllocation* allocation  )
{
/*Сохраняем картинку*/
		cairo_surface_t *surface;
		if (box->priv->self_box)
		{
		    GtkAllocation allocation_self_box;
		    gtk_widget_get_allocation( GTK_WIDGET(box->priv->self_box), &allocation_self_box );
		    GtkAllocation allocation_sibl_box;
		    gtk_widget_get_allocation( GTK_WIDGET(box->priv->sibl_box), &allocation_self_box );
		    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, allocation->width+30 , allocation->height + 44 + allocation_self_box.height + allocation_sibl_box.height );
		}		    
		else
		    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, allocation->width+30 , allocation->height+40 );
	
		cairo_t *cr_save = cairo_create (surface);
		gtk_chartbox_cairo_save(cr_save, box );
		cairo_destroy (cr_save);
		cairo_surface_write_to_png (surface, filepath);
		cairo_surface_destroy (surface);
/*Сохраняем данные*/	    
}

static guint 
get_min_size_line( GList* ml ){
    GList *list = g_list_last ( ml );
    guint min_ret = gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data));
    while ( list ){
	if ( min_ret > gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data)))
	     min_ret = gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data));
	list = g_list_previous (list); 
    }
    return min_ret;
}
//#include <time.h>
static void 
get_time_str( time_t _t, char* _b, size_t _b_size ){
    struct tm tmt;
    localtime_r(&_t, &tmt );
    snprintf(_b, _b_size, "%.2d/%.2d/%.2d %.2d:%.2d:%.2d;",tmt.tm_mday, tmt.tm_mon+1, tmt.tm_year-100, tmt.tm_hour, tmt.tm_min, tmt.tm_sec  );
}
#define MAX_SIZE_LINE 1000
static void
gtk_chartbox_csv_save (FILE *fd, GtkChartBox * box ){
    g_return_if_fail (GTK_IS_CHARTBOX (box));
/*сохраняем значения точек графика*/
    guint data_size = get_min_size_line( box->priv->graphs );
    guint i = 0;	//Счетчик точек графика
    char buff[100];
    char one_line[MAX_SIZE_LINE];
    GList *list = g_list_last (box->priv->graphs);
    g_snprintf(one_line, sizeof(one_line), "Time;" );
    while ( list && strlen( one_line ) < MAX_SIZE_LINE ){
	g_snprintf( buff, sizeof(buff), "%s;", gtk_chartbox_graph_get_text(GTK_CHARTBOX_GRAPH(list->data)) );
	strcat( one_line, buff );
	list = g_list_previous (list);
    }
    
    strcat( one_line, "\n" );
    fwrite(one_line, strlen(one_line), 1, fd);
    
    for ( i = 0; i< data_size; ++i ){
	list = g_list_last (box->priv->graphs);			
	//g_snprintf( one_line, sizeof(one_line), "%i;", gtk_chartbox_graph_get_X( GTK_CHARTBOX_GRAPH(list->data) )[i] );
	get_time_str( gtk_chartbox_graph_get_X( GTK_CHARTBOX_GRAPH(list->data) )[i], one_line, sizeof(one_line) );
	
        while ( list && strlen( one_line ) < MAX_SIZE_LINE ){
    	    g_snprintf( buff, sizeof(buff), "%f;", gtk_chartbox_graph_get_Y( GTK_CHARTBOX_GRAPH(list->data) )[i] );
	    strcat( one_line, buff );
	    list = g_list_previous (list);
	}
        strcat( one_line, "\n" );
        fwrite(one_line, strlen(one_line), 1, fd);
    }    
}
static void
gtk_chartbox_cairo_save (cairo_t *cr, GtkChartBox * box){

    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );
    cairo_set_source_rgb(cr, 1.0,1.0,1.0);
    cairo_paint(cr);
    if (box->priv->self_box){
	GtkAllocation allocation;
/*Найдем размер графика занятия соседней РЦ*/		
	gtk_widget_get_allocation( GTK_WIDGET(box->priv->sibl_box), &allocation );
	gdouble _offset = (gdouble)allocation.height + 2.;
/*Перенесем картинку соседней рельсовой цепи*/
	cairo_set_source_surface(cr, gtk_chartbox_get_backing_surface(box->priv->sibl_box), 30., 0.);
	cairo_paint(cr);
/*Перенесем картинку своей рельсовой цепи*/
	cairo_set_source_surface(cr, gtk_chartbox_get_backing_surface(box->priv->self_box), 30., _offset );
	cairo_paint(cr);
/*Нарисуем текст с названиями занимамых РЦ*/
	cairo_set_source_rgb(cr, 0.9,0.0,0.0);
	cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size (cr, 14);
	cairo_move_to (cr, 2, 0.5*_offset  );
	cairo_show_text (cr, gtk_chartbox_get_text(GTK_CHARTBOX(box->priv->sibl_box)) );
	cairo_move_to (cr, 2, 1.5*_offset  );
	cairo_show_text (cr, gtk_chartbox_get_text(GTK_CHARTBOX(box->priv->self_box)) );
	cairo_stroke(cr);
	cairo_set_source_rgb(cr, 1.0,1.0,1.0);
/*Найдем размер графика занятия своей РЦ*/	
	gtk_widget_get_allocation( GTK_WIDGET(box->priv->self_box), &allocation );	
	_offset += (gdouble)allocation.height + 2.;
/*Отрисуем оси и картинку трендов*/
	cairo_set_source_surface(cr, gtk_chartbox_ruler_get_backing_surface(box->priv->ruler_y), 0., _offset);
	cairo_paint(cr);
	cairo_set_source_surface(cr, gtk_chartbox_get_backing_surface(box), 30., _offset);
	cairo_paint(cr);
	gtk_widget_get_allocation( GTK_WIDGET(box), &allocation );	
	cairo_set_source_surface(cr, gtk_chartbox_ruler_get_backing_surface(box->priv->ruler_x), 30., (gdouble)allocation.height + _offset );
	cairo_paint(cr);    
    }
    else{
	cairo_set_source_surface(cr, gtk_chartbox_ruler_get_backing_surface(box->priv->ruler_y), 0., 0.);
	cairo_paint(cr);
	cairo_set_source_surface(cr, gtk_chartbox_get_backing_surface(box), 30., 0.);
	cairo_paint(cr);
	GtkWidget *wid = GTK_WIDGET(box);
	gtk_widget_get_allocation( wid, &allocation );	
	cairo_set_source_surface(cr, gtk_chartbox_ruler_get_backing_surface(box->priv->ruler_x), 30., (gdouble)allocation.height );
	cairo_paint(cr);
    }

}

static void
gtk_chartbox_dat_save (FILE *fd, GtkChartBox * box ){
    g_return_if_fail (GTK_IS_CHARTBOX (box));
/*сохраняем установленные маркеры*/
//    пока не сделано...
/*сохраняем графики занятия/освобождения соседней рельсовой цепи*/
    if (box->priv->sibl_box){
	GList *list = g_list_last (box->priv->sibl_box->priv->graphs);
	char buff[50];
	while (list){
    	    g_snprintf(buff, sizeof(buff), "Sibling RC busy/empty graph data: %s\n", gtk_chartbox_get_text(GTK_CHARTBOX(box->priv->sibl_box)) );
	    fwrite(buff, strlen(buff), 1, fd);
	    if (list->data){
		guint len, *X, i;
		gfloat *Y;
		len = gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data));
		X = gtk_chartbox_graph_get_X(GTK_CHARTBOX_GRAPH(list->data));
		Y = gtk_chartbox_graph_get_Y(GTK_CHARTBOX_GRAPH(list->data));
		for(i=0; i< len; i++){
		    g_snprintf(buff, sizeof(buff), "X[%d]=%.10d; Y[%d]=%3.3f;\n",i ,X[i],i , Y[i] );
		    fwrite(buff, strlen(buff), 1, fd);
		}
	    }
        list = g_list_previous (list);
	}
    }
/*сохраняем графики занятия/освобождения своей рельсовой цепи*/
    if (box->priv->self_box){
	GList *list = g_list_last (box->priv->self_box->priv->graphs);
	char buff[50];
	while (list){
    	    g_snprintf(buff, sizeof(buff), "Self RC busy/empty graph data: %s\n", gtk_chartbox_get_text(GTK_CHARTBOX(box->priv->self_box)) );
	    fwrite(buff, strlen(buff), 1, fd);
	    if (list->data){
		guint len, *X, i;
		gfloat *Y;
		len = gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data));
		X = gtk_chartbox_graph_get_X(GTK_CHARTBOX_GRAPH(list->data));
		Y = gtk_chartbox_graph_get_Y(GTK_CHARTBOX_GRAPH(list->data));
		for(i=0; i< len; i++){
		    g_snprintf(buff, sizeof(buff), "X[%d]=%.10d; Y[%d]=%3.3f;\n",i ,X[i],i , Y[i] );
		    fwrite(buff, strlen(buff), 1, fd);
		}
	    }
        list = g_list_previous (list);
	}
    }
/*сохраняем значения точек графика*/
    GList *list = g_list_last (box->priv->graphs);
    char buff[50];
    while (list){
    	g_snprintf(buff, sizeof(buff), "Graph data: %s\n", gtk_chartbox_graph_get_text(GTK_CHARTBOX_GRAPH(list->data)) );
	fwrite(buff, strlen(buff), 1, fd);
	if (list->data){
	    guint len, *X, i;
	    gfloat *Y;
	    len = gtk_chartbox_graph_get_length(GTK_CHARTBOX_GRAPH(list->data));
	    X = gtk_chartbox_graph_get_X(GTK_CHARTBOX_GRAPH(list->data));
	    Y = gtk_chartbox_graph_get_Y(GTK_CHARTBOX_GRAPH(list->data));
	    for(i=0; i< len; i++){
		g_snprintf(buff, sizeof(buff), "X[%d]=%.10d; Y[%d]=%3.3f;\n",i ,X[i],i , Y[i] );
		fwrite(buff, strlen(buff), 1, fd);
	    }
	}
      list = g_list_previous (list);
    }
}
#if (GTK_MAJOR_VERSION == 3)
#include "portable_layer.h"
#endif
/*---------------Создать все -------------------*/
void 
gtk_chartbox_create_all (GtkWidget ** p_box, GtkWidget ** p_table, gboolean scrollbar_x, gboolean ruler_x, gboolean ruler_y, gboolean time_ruler_x){

   GtkTable *table;
   GtkChartBox *box;
   GtkWidget *scrollbar;
   GtkWidget *ruler;

   *p_table = gtk_table_new (3, 3, FALSE);
   *p_box = gtk_chartbox_new ();
   box = GTK_CHARTBOX (*p_box);
   table = GTK_TABLE (*p_table);

   GtkAllocation _alloc;
   _alloc.x=0; _alloc.y=0; 
   _alloc.width=500; _alloc.height=300;
   gtk_widget_size_allocate( GTK_WIDGET( box ),  &_alloc );

   gtk_table_attach (table, GTK_WIDGET (box), 1, 2, 0, 1,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

   if (scrollbar_x)
   {
      scrollbar = gtk_hscrollbar_new ( NULL);
      gtk_chartbox_set_adjustment_x (box, gtk_range_get_adjustment (GTK_RANGE(scrollbar)));
      gtk_table_attach (table, scrollbar, 1, 2, 2, 3,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
   }

   if (ruler_x)
   {
      ruler = gtk_chartbox_ruler_new (GTK_ORIENTATION_HORIZONTAL);
#if (GTK_MAJOR_VERSION == 3)
      GtkAllocation _alloc;
      _alloc.x=0; _alloc.y=0; _alloc.width=500; _alloc.height=40;
      gtk_widget_size_allocate( GTK_WIDGET( ruler ),  &_alloc );
#endif
      gtk_table_attach (table, ruler, 1, 2, 1, 2,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
      if(time_ruler_x)
	gtk_chartbox_ruler_set_type_time (GTK_CHARTBOX_RULER( ruler ), TRUE );
      gtk_chartbox_set_ruler (box, GTK_CHARTBOX_RULER (ruler));
   }

   if (ruler_y)
   {
      ruler = gtk_chartbox_ruler_new (GTK_ORIENTATION_VERTICAL);
#if (GTK_MAJOR_VERSION == 3)
      GtkAllocation _alloc;
      _alloc.x=0; _alloc.y=0; _alloc.width=30; _alloc.height=300;
      gtk_widget_size_allocate( GTK_WIDGET( ruler ),  &_alloc );
#endif
      gtk_table_attach (table, ruler, 0, 1, 0, 1, GTK_FILL,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);
      gtk_chartbox_set_ruler (box, GTK_CHARTBOX_RULER (ruler));
   }

}

/*---------------Создать тренды для рисования дежурного режима -------------------*/
void 
gtk_chartbox_set_self_box(GtkChartBox *_box, GtkChartBox *_self_box ){
	g_return_if_fail (GTK_IS_CHARTBOX (_box));
	g_return_if_fail (GTK_IS_CHARTBOX (_self_box));
	_box->priv->self_box = _self_box;
}	
void 
gtk_chartbox_set_sibl_box(GtkChartBox *_box, GtkChartBox *_sibl_box ){
	g_return_if_fail (GTK_IS_CHARTBOX (_box));
	g_return_if_fail (GTK_IS_CHARTBOX (_sibl_box));
	_box->priv->sibl_box = _sibl_box;
}	

void 
gtk_chartbox_create_vigil (GtkWidget ** p_box, GtkWidget ** p_table, 
			    GtkWidget ** self_box, // рисование моментов зания и освобождения своей секциии
			    GtkWidget ** sibl_box, // рисование моментов зания и освобождения сеседней секциии
			    const char *self_label, // название своей РЦ
			    const char *sibl_label //название соседней РЦ
			    ){
   GtkTable *table;
   GtkChartBox *box;
   GtkChartBox *box_self;
   GtkChartBox *box_sibl;
   GtkWidget *scrollbar;
   GtkWidget *ruler;

   *p_table = gtk_table_new (3, 7, FALSE);
   *p_box = gtk_chartbox_new ();
   *self_box = gtk_chartbox_new ();   
   *sibl_box = gtk_chartbox_new ();   
   box = GTK_CHARTBOX (*p_box);
   box_self = GTK_CHARTBOX (*self_box);
   box_sibl = GTK_CHARTBOX (*sibl_box);
   gtk_chartbox_set_self_box(box, box_self );	
   gtk_chartbox_set_sibl_box(box, box_sibl );	
   gtk_chartbox_set_text (box_sibl, (gchar*)sibl_label );
   gtk_chartbox_set_text (box_self, (gchar*)self_label );
     
  /*Событие движения мыши по основному графику размножим на графики занятия/освобождение РЦ*/
    g_signal_connect_swapped (box, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box_self)-> motion_notify_event),
				G_OBJECT (box_self) );
    g_signal_connect_swapped (box, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box_sibl)-> motion_notify_event),
				G_OBJECT (box_sibl) );

    g_signal_connect_swapped (box_sibl, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box)-> motion_notify_event),
				G_OBJECT (box) );
    g_signal_connect_swapped (box_sibl, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box_self)-> motion_notify_event),
				G_OBJECT (box_self) );

    g_signal_connect_swapped (box_self, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box)-> motion_notify_event),
				G_OBJECT (box) );
    g_signal_connect_swapped (box_self, "motion_notify_event",
				G_CALLBACK (GTK_WIDGET_GET_CLASS(box_sibl)-> motion_notify_event),
				G_OBJECT (box_sibl) );

    gchar *markup_self,  *markup_sibl;
  /*Создадим подписи рельсовых цепей*/
    guint tsize = 1000*(16-strlen(gtk_chartbox_get_text (box_self)));
    tsize= tsize > 10000 ? 10000 : tsize;	
    markup_self = g_markup_printf_escaped ("<span size=\"%d\" weight=\"bold\" >%s</span>", tsize, gtk_chartbox_get_text (box_self ) );
    GtkWidget *self_lab = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(self_lab), markup_self);
    gtk_label_set_angle(GTK_LABEL(self_lab), 90.);
    
    tsize = 1000*(16-strlen(gtk_chartbox_get_text (box_sibl)));	
    tsize= tsize > 10000 ? 10000 : tsize;	    
    markup_sibl = g_markup_printf_escaped ("<span size=\"%d\" weight=\"bold\" >%s</span>", tsize, gtk_chartbox_get_text (box_sibl ) );
    GtkWidget *sibl_lab = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(sibl_lab), markup_sibl);
    gtk_label_set_angle(GTK_LABEL(sibl_lab), 90.);
    /*Скажем не рисовать перекрестие осей*/
    gtk_chartbox_draw_cross(box_self, FALSE);
    gtk_chartbox_draw_cross(box_sibl, FALSE);    
/*Устанавлдиваем размеры окон и расположение*/
    GtkAllocation _alloc;
    _alloc.x=0; _alloc.y=0; 
    _alloc.width=500; _alloc.height=300;
    gtk_widget_size_allocate(GTK_WIDGET( box ),  &_alloc);
    _alloc.x=0; _alloc.y=0; 
    _alloc.width=500; _alloc.height=50;
    gtk_widget_size_allocate(GTK_WIDGET( box_self ),  &_alloc);
    gtk_widget_size_allocate(GTK_WIDGET( box_sibl ),  &_alloc);

    table = GTK_TABLE (*p_table);
    gtk_table_attach (table, GTK_WIDGET (box_sibl), 1, 2, 0, 1,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		     GTK_FILL , 0, 0);

    gtk_table_attach (table, gtk_hseparator_new(), 1, 2, 1, 2,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
    gtk_table_attach (table, gtk_hseparator_new(), 0, 1, 1, 2,
			GTK_FILL, GTK_FILL, 0, 0);

    gtk_table_attach (table, GTK_WIDGET (box_self), 1, 2, 2, 3,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		     GTK_FILL , 0, 0);

    gtk_table_attach (table, gtk_hseparator_new(), 1, 2, 3, 4,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
    gtk_table_attach (table, gtk_hseparator_new(), 0, 1, 3, 4,
			GTK_FILL, GTK_FILL, 0, 0);

    gtk_table_attach (table, GTK_WIDGET (box), 1, 2, 4, 5,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		     GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);

    gtk_table_attach (table, self_lab, 0, 1, 2, 3, GTK_FILL,
			GTK_FILL , 0, 0);

    gtk_table_attach (table, sibl_lab, 0, 1, 0, 1, GTK_FILL,
			GTK_FILL , 0, 0);

/*  (scrollbar_x)*/
    scrollbar = gtk_hscrollbar_new (NULL);
    gtk_chartbox_set_adjustment_x (box, gtk_range_get_adjustment (GTK_RANGE(scrollbar)));
    gtk_table_attach (table, scrollbar, 1, 2, 6, 7,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
   
/*    (ruler_x)*/
    ruler = gtk_chartbox_ruler_new (GTK_ORIENTATION_HORIZONTAL);
#if (GTK_MAJOR_VERSION == 3)
      _alloc.x=0; _alloc.y=0; _alloc.width=500; _alloc.height=40;
      gtk_widget_size_allocate( GTK_WIDGET( ruler ),  &_alloc );
#endif
    gtk_table_attach (table, ruler, 1, 2, 5, 6,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
    gtk_chartbox_ruler_set_type_time (GTK_CHARTBOX_RULER( ruler ), TRUE );
    gtk_chartbox_set_ruler (box, GTK_CHARTBOX_RULER (ruler));
   

/*   (ruler_y)*/
    ruler = gtk_chartbox_ruler_new (GTK_ORIENTATION_VERTICAL);
#if (GTK_MAJOR_VERSION == 3)
      _alloc.x=0; _alloc.y=0; _alloc.width=30; _alloc.height=300;
      gtk_widget_size_allocate( GTK_WIDGET( ruler ),  &_alloc );
#endif
    gtk_table_attach (table, ruler, 0, 1, 4, 5, GTK_FILL,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0);
    gtk_chartbox_set_ruler (box, GTK_CHARTBOX_RULER (ruler));
       
}
