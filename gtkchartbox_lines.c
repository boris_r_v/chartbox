#include "gtkchartbox_lines.h"
#include <math.h>
static void gtk_chartbox_lines_real_draw (GtkChartBoxGraph * lines, GtkChartBox* box );
static void gtk_chartbox_lines_real_draw_curve (GtkChartBoxGraph * lines, GtkChartBox* box );
static gint gtk_chartbox_lines_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result );
struct _GtkChartBoxLinesPrivate
{
   GdkPoint *data;
};

static gpointer parent_class = NULL;

static void
lines_finalize (GObject * object)
{
   GtkChartBoxLines *lines = GTK_CHARTBOX_LINES (object);

   g_free (lines->priv->data);
   g_free (lines->priv);

   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_chartbox_lines_class_init (gpointer g_class /*, gpointer g_class_data */ )
{
   GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
   GtkChartBoxGraphClass *graph_class = GTK_CHARTBOX_GRAPH_CLASS (g_class);
   GtkChartBoxLinesClass *klass = GTK_CHARTBOX_LINES_CLASS (g_class);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = lines_finalize;

   graph_class->draw = gtk_chartbox_lines_real_draw;
   graph_class->calculate_value = gtk_chartbox_lines_real_calculate_value;
}

static void
gtk_chartbox_lines_complete (GtkChartBoxLines * lines)
{
   lines->priv->data =  (GdkPoint*)g_renew(GdkPoint, lines->priv->data, gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (lines)) );

}

static void
gtk_chartbox_lines_instance_init (GTypeInstance * instance ){
   GtkChartBoxLines *lines = GTK_CHARTBOX_LINES (instance);

   lines->priv = g_new0 (GtkChartBoxLinesPrivate, 1);
   lines->priv->data = NULL;    
   g_signal_connect (lines, "notify::length",
		     G_CALLBACK (gtk_chartbox_lines_complete), NULL);
}

GType
gtk_chartbox_lines_get_type (void)
{
   static GType type = 0;

   if (type == 0)
   {
      static const GTypeInfo info = {
	 sizeof (GtkChartBoxLinesClass),
	 NULL,			/* base_init */
	 NULL,			/* base_finalize */
	 (GClassInitFunc) gtk_chartbox_lines_class_init,	/* class_init */
	 NULL,			/* class_finalize */
	 NULL,			/* class_data */
	 sizeof (GtkChartBoxLines),	/* instance_size */
	 0,			/* n_preallocs */
	 (GInstanceInitFunc) gtk_chartbox_lines_instance_init,	/* instance_init */
	 NULL,			/* value_table */
      };
      type = g_type_register_static (GTK_CHARTBOX_TYPE_GRAPH,
				     "GtkChartBoxLines", &info, 0);
   }

   return type;
}

GtkChartBoxGraph *
gtk_chartbox_lines_new (guint len, guint * X, gfloat * Y,
		       GdkColor * color, guint size)
{

   GtkChartBoxLines *lines;
   g_return_val_if_fail (X, NULL);
   g_return_val_if_fail (Y, NULL);
   g_return_val_if_fail ((len > 0), NULL);

   lines = g_object_new (GTK_CHARTBOX_TYPE_LINES,
			 "X-Values", X,
			 "Y-Values", Y,
			 "length", len, "color", color, "size", size, NULL);

   return GTK_CHARTBOX_GRAPH (lines);
}

static void
gtk_chartbox_lines_real_draw (GtkChartBoxGraph * graph, GtkChartBox * box){
   GtkWidget *widget;
   GtkChartBoxLines *lines = GTK_CHARTBOX_LINES (graph);
   GdkPoint *data, *data_dot=NULL;
   cairo_surface_t *surface;
   GdkColor *color;
   cairo_t *cr;
   guint i = 0;
   guint  *X;
   gfloat *Y;
   guint len;
   gint size = 0;
   g_return_if_fail (GTK_IS_CHARTBOX_LINES (lines));
   g_return_if_fail (GTK_IS_CHARTBOX (box));

   widget = GTK_WIDGET(box);
    
   surface = gtk_chartbox_get_backing_surface (box);
   cr = gtk_chartbox_get_backing_content (box);
   
   len = gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (graph));
   color = gtk_chartbox_graph_get_color (GTK_CHARTBOX_GRAPH (graph));
   X = gtk_chartbox_graph_get_X (GTK_CHARTBOX_GRAPH (graph));
   Y = gtk_chartbox_graph_get_Y (GTK_CHARTBOX_GRAPH (graph));
   size = gtk_chartbox_graph_get_size (GTK_CHARTBOX_GRAPH (graph ));
   data = lines->priv->data;
   gboolean tog = gtk_chartbox_graph_get_together (GTK_CHARTBOX_GRAPH (graph));
    /*расчитываем значения координат точек */
    /*Выясним сглажен ли график*/
    gboolean smooth = gtk_chartbox_graph_get_smooth (GTK_CHARTBOX_GRAPH (graph));
    if (smooth){ //график сглажен
	gfloat sigma = 1.1;
	gfloat *Y_smooth = g_new0(gfloat, len );
	data_dot = g_new0(GdkPoint, len );
	guint mw = gtk_chartbox_graph_get_ma_factor (GTK_CHARTBOX_GRAPH (graph)); //период усреднения
	guint inr = (mw-1)/2; //половина периода усреднения
	int i=0;
	for (i=0; i < len; i++){
	    if (i>inr && i<len-inr)
	    {
		gfloat summ = 0.0;
		int ii = -inr;
		for ( ii = -inr; ii != inr+1; ++ii )
		    summ += Y[i-ii];
		Y_smooth[i] = summ / mw;
		//Y_smooth[i] = (Y[i-2] + Y[i-1] + Y[i] + Y[i+1] + Y[i+2])/5;
	    }
	    else
		Y_smooth[i] = Y[i];
	}
	gtk_chartbox_values_to_pixels (box, len, X, Y_smooth, data);	
	gtk_chartbox_values_to_pixels (box, len, X, Y, data_dot);	
	g_free(Y_smooth);
    }
    else{
	//график не сглажен
	gtk_chartbox_values_to_pixels (box, len, X, Y, data);
    }
    /*Рисуем график */
    gdk_cairo_set_source_color (cr, color);
    cairo_set_line_width(cr, (gdouble)size);
    cairo_move_to (cr, data[0].x, data[0].y);
    for (i = 1; i < len; i ++ ){
	if (!tog)
	{
	    if (!smooth)
	    {
		if (abs(X[i] - X[i-1]) > 1 )
		{
		    cairo_move_to (cr, data[i].x, data[i].y);
		    cairo_arc (cr, data[i].x, data[i].y, 1, 0, 2 * M_PI );
		}
		else
		    cairo_line_to (cr, data[i].x, data[i].y );
	    }
	    else
	    {
		cairo_line_to (cr, data[i].x, data[i].y );
		cairo_move_to (cr, data_dot[i].x, data_dot[i].y);
		//cairo_arc (cr, data_dot[i].x, data_dot[i].y, 0.2, 0, 2 * M_PI );
		cairo_move_to (cr, data[i].x, data[i].y);
	    }
	}
	else
	{
	    cairo_line_to (cr, data[i].x, data[i].y );
	    if (smooth)
	    {
	    	cairo_move_to (cr, data_dot[i].x, data_dot[i].y);
		cairo_arc (cr, data_dot[i].x, data_dot[i].y, 0.5, 0, 2 * M_PI );
		cairo_move_to (cr, data[i].x, data[i].y);
	    }
	}
    }
    g_free (data_dot);
    cairo_stroke(cr);
    return;
}

static gint
gtk_chartbox_lines_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result ){
   g_return_val_if_fail (graph, -1);
   g_return_val_if_fail (time, -1);

//printf("time %d\n", *time );
    gint i;
    guint len = gtk_chartbox_graph_get_length (graph);
    guint *X = gtk_chartbox_graph_get_X (graph);
    gfloat *Y = gtk_chartbox_graph_get_Y (graph);
    for(i=0; i < len; i++){
	if (*time == X[i]){
	    *result = Y[i];
	    return 1;
	}
// Закоменитирвано так как ось Х в int, - и всегда есть значение Y для каждого X, поэтому вычислеять не нужно ничего.
//    Когда ось Х будет во float(double) - тогда имеет смысл что-то вычислять.
    if (gtk_chartbox_graph_get_smooth (GTK_CHARTBOX_GRAPH (graph)))
	if ( i!=len ) if ( (*time>X[i-1]) && (*time<X[i]) ){
    	    if (abs(X[i-1] - X[i]) > 1 ){
		//guint x1,x2,x;
		//gfloat y1,y2;
		gdouble x1,x2,x,y1,y2;
	        x1 = X[i-1];
		x2 = X[i];
		y1 = Y[i-1];
		y2 = Y[i];
		x = *time;
		*result = (((x-x1)*(y2-y1))/(x2-x1))+y1;
		//printf ("resutl %.2f  %d\n", *result, abs(X[i+1] - X[i])  );
		return 1;
	    }
	    else{
	        *result = 0.0;
		return 1;
	    }
	}
	    
  }
    *result = 0.0;
    return 0;
}


static void
gtk_chartbox_lines_real_draw_curve (GtkChartBoxGraph * graph, GtkChartBox * box){
   GtkWidget *widget;
   GtkChartBoxLines *lines = GTK_CHARTBOX_LINES (graph);
   GdkPoint *data, *data_dot=NULL;
   cairo_surface_t *surface;
   GdkColor *color;
   cairo_t *cr;
   guint i = 0;
   guint  *X;
   gfloat *Y;
   guint len;
   gint size = 0;
   g_return_if_fail (GTK_IS_CHARTBOX_LINES (lines));
   g_return_if_fail (GTK_IS_CHARTBOX (box));

   widget = GTK_WIDGET(box);
    
   surface = gtk_chartbox_get_backing_surface (box);
   cr = gtk_chartbox_get_backing_content (box);
   
   len = gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (graph));
   color = gtk_chartbox_graph_get_color (GTK_CHARTBOX_GRAPH (graph));
   X = gtk_chartbox_graph_get_X (GTK_CHARTBOX_GRAPH (graph));
   Y = gtk_chartbox_graph_get_Y (GTK_CHARTBOX_GRAPH (graph));
   size = gtk_chartbox_graph_get_size (GTK_CHARTBOX_GRAPH (graph ));
   data = lines->priv->data;
    /*Выясним рисовать ли разрывы графика*/
   gboolean tog = gtk_chartbox_graph_get_together (GTK_CHARTBOX_GRAPH (graph));
    /*Выясним сглажен ли график*/
    gboolean smooth = gtk_chartbox_graph_get_smooth (GTK_CHARTBOX_GRAPH (graph));
    /*расчитываем значения координат точек */
    gtk_chartbox_values_to_pixels (box, len, X, Y, data);

    /*Рисуем график */
    gdk_cairo_set_source_color (cr, color);
    cairo_set_line_width(cr, (gdouble)size);
    cairo_move_to (cr, data[0].x, data[0].y);
    for (i = 1; i < len; ++i )
    {
        if (abs(X[i] - X[i-1]) > 1 )
        {/*Есть разрыв*/
            if ( smooth )
	    {/*график сглажен рисуем кривыю Безье по трем точкам, в месте разрыва*/
		cairo_move_to (cr, data[i-1].x, data[i-1].y);
		cairo_curve_to(cr, data[i-1].x, data[i-1].y, data[i].x, data[i].y, data[i+1].x, data[i+1].y );
		cairo_move_to (cr, data[i-1].x, data[i-1].y);
		cairo_arc (cr, data[i-1].x, data[i-1].y, 1, 0, 2 * M_PI );
		cairo_move_to (cr, data[i].x, data[i].y);
		cairo_arc (cr, data[i].x, data[i].y, 1, 0, 2 * M_PI );
		cairo_move_to (cr, data[i+1].x, data[i+1].y);
		cairo_arc (cr, data[i+1].x, data[i+1].y, 1, 0, 2 * M_PI );
		++i;
		cairo_move_to (cr, data[i].x, data[i].y);
	    }
	    else
	    {
		if (tog)
		{
		    cairo_line_to (cr, data[i].x, data[i].y );   
		}
		else
		{
		    /*график не сглажен и рисуется с разрывами, рисуем точку в начале новой линии*/
		    cairo_move_to (cr, data[i].x, data[i].y);
        	    cairo_arc (cr, data[i].x, data[i].y, 1, 0, 2 * M_PI );
        	    cairo_move_to (cr, data[i].x, data[i].y);
        	}
	    }
        }
        else/*нет разрыва*/
	{
            cairo_line_to (cr, data[i].x, data[i].y );
            if ( smooth )
	    {
	    	cairo_move_to (cr, data[i].x, data[i].y);
		cairo_arc (cr, data[i].x, data[i].y, 1, 0, 2 * M_PI );
		cairo_move_to (cr, data[i].x, data[i].y);
	    }
	}
    }
	       
    cairo_stroke(cr);
    return;
}
