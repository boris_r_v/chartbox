#ifndef __GTK_CHARTBOX_H__
#define __GTK_CHARTBOX_H__
#include <gtk/gtk.h>
#include "gtkchartbox_typedefs.h"

G_BEGIN_DECLS

#define GTK_CHARTBOX_TYPE		(gtk_chartbox_get_type ())
#define GTK_CHARTBOX(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE, GtkChartBox))
#define GTK_CHARTBOX_CLASS(klass) 	(G_TYPE_CHECK_CLASS_CAST(klass, GTK_CHARTBOX_TYPE, GtkChartBoxClass))
#define GTK_CHARTBOX_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE, GtkChartBoxClass))
#define GTK_IS_CHARTBOX(obj) 		(G_TYPE_CHECK_INSTANCE_TYPE(obj, GTK_CHARTBOX_TYPE))


typedef struct _GtkChartBoxClass	GtkChartBoxClass;
typedef struct _GtkChartBoxPrivate 	GtkChartBoxPrivate;
    struct _GtkChartBox
    {
	GtkWidget widget;

	/* < private > */
	GtkChartBoxPrivate *priv;

    };

    struct _GtkChartBoxClass
    {
	GtkWidgetClass parent_class;
	void (*zoom) (GtkChartBox * box );
	void (*offset) (GtkChartBox * box );
	void (*motion) (GtkChartBox * box );
    };
    GType gtk_chartbox_get_type (void);

    GtkWidget *gtk_chartbox_new (void);
	/* Adjastment */
    void gtk_chartbox_set_adjustment_x (GtkChartBox * box, GtkAdjustment * adj);
    GtkAdjustment *gtk_chartbox_get_adjustment_x (GtkChartBox * box);
	/* Graph operationd */
    gint gtk_chartbox_graph_add (GtkChartBox * box, GtkChartBoxGraph * graph);
    gint gtk_chartbox_graph_remove (GtkChartBox * box, GtkChartBoxGraph * graph);
    gint gtk_chartbox_graph_remove_all (GtkChartBox * box);

	/* Markers operations*/
    gint gtk_chartbox_marker_add (GtkChartBox * box, GtkChartBoxMarker * mark);
    gint gtk_chartbox_marker_remove (GtkChartBox * box, GtkChartBoxMarker * mark);
    gint gtk_chartbox_marker_remove_all (GtkChartBox * box);

	/* Zoomed operations */
    void gtk_chartbox_zoom_in(GtkChartBox *box);
    void gtk_chartbox_zoom_out(GtkChartBox *box);
    void gtk_chartbox_zoom_home(GtkChartBox *box);

	/*Установка значения перемещения  при связанныех ChBox */
    void gtk_chartbox_set_offset(GtkChartBox *box, gfloat offset );
    gfloat gtk_chartbox_get_offset(GtkChartBox *box );
	/*Установка значения параметра zoom  при связанныех ChBox */
    void gtk_chartbox_set_zoom(GtkChartBox *box, gint zoom );
    gint gtk_chartbox_get_zoom(GtkChartBox *box );
	/*Установка GdkMotionEvent при связанныех ChBox*/	
    void gtk_chartbox_set_motion_event(GtkChartBox *box, GdkEventMotion* );	
    
	/*Draw or not cross*/
    void gtk_chartbox_draw_cross(GtkChartBox *box, gboolean _dr );
	/*Save data to file*/
    void gtk_chartbox_save(GtkChartBox *box);
	/*Update chartbox */
    void gtk_chartbox_update(GtkChartBox *box);    
	/*Set/get title for graph box*/
    void gtk_chartbox_set_text (GtkChartBox* box, gchar const* text);    
    const gchar* gtk_chartbox_get_text (GtkChartBox* box );
    /*Текст который бедут отображаться на самом графике посередине*/
    void gtk_chartbox_set_label_text (GtkChartBox* box, gchar const* text);    
	/*Возможно наличиче минусовых значений на графике */
    void gtk_chartbox_set_negative_value(GtkChartBox *box, gboolean nv );
    gboolean gtk_chartbox_get_negative_value(GtkChartBox *box );
	/*Установить папку куда писать файлы*/
    void gtk_chartbox_set_store_path(GtkChartBox *box, gchar const* );	
	/* Used by graph objects */
    cairo_surface_t* gtk_chartbox_get_backing_surface(GtkChartBox * box);
    cairo_t* gtk_chartbox_get_backing_content(GtkChartBox * box);

	/* Auto construct chartbox */
   void gtk_chartbox_create_all (GtkWidget ** p_box,
				GtkWidget ** p_table,
				gboolean scrollbar_x,
				gboolean ruler_x,
				gboolean ruler_y,
				gboolean time_ruler_x
				);
/*For Vigil Graph*/
   void gtk_chartbox_create_vigil (GtkWidget ** p_box,
				    GtkWidget ** p_table,
				    GtkWidget ** self_box,
				    GtkWidget ** sibl_box,
				    const char *self_label,
				    const char *sibl_label
				    );
	
G_END_DECLS

#endif
