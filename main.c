#include <gtk/gtk.h>
#include "gtkchartbox.h"
#include "gtkchartbox_ruler.h"
#include "gtkchartbox_lines.h"
#include "gtkchartbox_points.h"
#include "gtkchartbox_bars.h"
#include "gtkchartbox_graph.h"

static void NewVaL(GtkChartBoxGraph* graph, gpointer data){
    float val=0;
    val = gtk_chartbox_graph_get_value(graph );
    gchar buff[10];
    g_snprintf(buff, sizeof(buff), "%2.2f", val);
    gtk_entry_set_text(GTK_ENTRY(data), buff);
}

static void ZoomIn(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_in(GTK_CHARTBOX(data) );
}

static void ZoomOut(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_out(GTK_CHARTBOX(data) );
}

static void ZoomHome(GtkWidget *graph, gpointer data){
    gtk_chartbox_zoom_home(GTK_CHARTBOX(data) );
}

static void SavePict(GtkWidget *graph, gpointer data){


    gtk_chartbox_save(GTK_CHARTBOX(data) );
}

static void Toggether(GtkToggleButton *but, gpointer data){
    gtk_chartbox_graph_set_together (GTK_CHARTBOX_GRAPH(data),  gtk_toggle_button_get_active(but) );
    gtk_chartbox_update(GTK_CHARTBOX_GRAPH(data));    
}

static void Smooth(GtkToggleButton *but, gpointer data){
    gtk_chartbox_graph_set_smooth (GTK_CHARTBOX_GRAPH(data), gtk_toggle_button_get_active(but));
    gtk_chartbox_update(GTK_CHARTBOX_GRAPH(data));    
}


int
main (int argc, char **argv)
{
	gtk_init (&argc, &argv);

	GtkWidget *window;
	GtkWidget *box, *table, *self_box, *sibl_box;
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	GdkColor c;
	
	gtk_chartbox_create_vigil(&box, &table, &self_box, &sibl_box, "1СП", "102CП" );
//	gtk_chartbox_create_all(&box, &table, TRUE, TRUE, TRUE, TRUE );
	
	GtkWidget *val1 = gtk_entry_new (); gtk_widget_set_size_request (val1, 20, -1);gtk_editable_set_editable (GTK_EDITABLE (val1), FALSE);

	guint* X = g_new0 (guint, 45);
	gfloat* Y = g_new0 (gfloat, 45);
X[0]=1255346042; Y[0]=33.294;
X[1]=1255346043; Y[1]=33.430;
X[2]=1255346044; Y[2]=33.388;
X[3]=1255346045; Y[3]=45.589;
X[4]=1255346046; Y[4]=54.669;
X[5]=1255346047; Y[5]=54.429;
X[6]=1255346048; Y[6]=54.481;
X[7]=1255346049; Y[7]=44.545;
X[8]=1255346050; Y[8]=55.087;
X[9]=1255346051; Y[9]=55.191;
X[10]=1255346052; Y[10]=26.959;
X[11]=1255346053; Y[11]=27.961;
X[12]=1255346054; Y[12]=53.302;
X[13]=1255346055; Y[13]=52.926;
X[14]=1255346056; Y[14]=57.404;
X[15]=1255346057; Y[15]=71.681;
X[16]=1255346058; Y[16]=71.389;
X[17]=1255346059; Y[17]=71.963;
X[18]=1255346060; Y[18]=71.754;
X[19]=1255346061; Y[19]=71.358;
X[20]=1255346062; Y[20]=70.732;
X[21]=1255346063; Y[21]=70.565;
X[22]=1255346064; Y[22]=83.016;
X[23]=1255346065; Y[23]=83.444;
X[24]=1255346066; Y[24]=83.099;
X[25]=1255346067; Y[25]=83.120;
X[26]=1255346068; Y[26]=82.891;
X[27]=1255346069; Y[27]=83.079;
X[28]=1255346070; Y[28]=83.684;
X[29]=1255346071; Y[29]=82.849;
X[30]=1255346072; Y[30]=74.823;
X[31]=1255346073; Y[31]=75.094;
X[32]=1255346074; Y[32]=74.750;
X[33]=1255346075; Y[33]=60.702;
X[34]=1255346076; Y[34]=62.403;
X[35]=1255346077; Y[35]=62.633;
X[36]=1255346078; Y[36]=57.518;
/*
X[37]=1255346079; Y[37]=57.424;
X[38]=1255346080; Y[38]=50.233;
X[39]=1255346081; Y[39]=50.400;
X[40]=1255346082; Y[40]=38.805;
*/
X[37]=1255346083; Y[37]=39.066;
X[38]=1255346084; Y[38]=28.107;
X[39]=1255346085; Y[39]=28.410;
X[40]=1255346086; Y[40]=28.441;

	guint* X_s = g_new0 (guint, 6);
	gfloat* Y_s = g_new0 (gfloat, 6);
X_s[0]=1255346042; Y_s[0]=0;
X_s[1]=1255346051; Y_s[1]=0;
X_s[2]=1255346051; Y_s[2]=1;
X_s[3]=1255346079; Y_s[3]=1;
X_s[4]=1255346079; Y_s[4]=0;
X_s[5]=1255346086; Y_s[5]=0;
	gdk_color_parse("#000000", &c);
	GtkChartBoxGraph *graph_s = gtk_chartbox_lines_new(6, X_s, Y_s, &c, 2);
	gtk_chartbox_graph_set_together( graph_s, TRUE);
	gtk_chartbox_graph_add(GTK_CHARTBOX(self_box), graph_s);
	gtk_chartbox_graph_add(GTK_CHARTBOX(sibl_box), graph_s);


	gdk_color_parse("#aa00cc", &c);
	GtkChartBoxGraph *graph = gtk_chartbox_lines_new(40, X, Y, &c, 2);
	gtk_chartbox_graph_set_text( graph, "female" );
	g_signal_connect(G_OBJECT(graph),"value-change", G_CALLBACK(NewVaL), val1);	
	gtk_chartbox_graph_add(GTK_CHARTBOX(box), graph);

	gdk_color_parse("#ffffcc", &c);
	graph = gtk_chartbox_lines_new(10, X, Y, &c, 7);
	gtk_chartbox_graph_set_text( graph, "male" );
	g_signal_connect(G_OBJECT(graph),"value-change", G_CALLBACK(NewVaL), val1);	
	gtk_chartbox_graph_add(GTK_CHARTBOX(box), graph);


	gdk_color_parse("#ff0000", &c);
	GtkChartBoxMarker *mark = gtk_chartbox_marker_new(&X[14], &Y[26], &c, 20, "test label" );
	gtk_chartbox_marker_add(GTK_CHARTBOX(box), mark);

	//gtk_chartbox_set_store_path(GTK_CHARTBOX(box), "/tmp/");
	gtk_chartbox_set_label_text(GTK_CHARTBOX(box), "/tmp/");

	gdk_color_parse("#0000ff", &c);
	GtkChartBoxMarker *mark1 = gtk_chartbox_marker_new(&X[44], &Y[2], &c, 20, "Хомяков");
	gtk_chartbox_marker_set_line(mark1, TRUE);
	gtk_chartbox_marker_set_size(mark1, 15);	
	gtk_chartbox_marker_add(GTK_CHARTBOX(box), mark1);


	GtkWidget *hbox = gtk_hbox_new(0,0);

	GtkWidget *vbox = gtk_vbox_new(0,0);
	GtkWidget *zoomIn = gtk_button_new_from_stock(GTK_STOCK_ZOOM_IN);
	GtkWidget *zoomOut = gtk_button_new_from_stock(GTK_STOCK_ZOOM_OUT);
	GtkWidget *zoomHome = gtk_button_new_from_stock(GTK_STOCK_ZOOM_100);
	GtkWidget *save = gtk_button_new_from_stock(GTK_STOCK_SAVE);
	GtkWidget *tog = gtk_check_button_new_with_label("Без разрывов");
	GtkWidget *smooth = gtk_check_button_new_with_label("Сгладить");
	
	g_signal_connect(G_OBJECT(zoomIn),"clicked", G_CALLBACK(ZoomIn), box);
	g_signal_connect(G_OBJECT(zoomOut),"clicked", G_CALLBACK(ZoomOut), box);
	g_signal_connect(G_OBJECT(zoomHome),"clicked", G_CALLBACK(ZoomHome), box);
	g_signal_connect(G_OBJECT(save),"clicked", G_CALLBACK(SavePict), box);
	g_signal_connect(G_OBJECT(tog),"toggled", G_CALLBACK(Toggether), graph);	
	g_signal_connect(G_OBJECT(smooth),"toggled", G_CALLBACK(Smooth), graph);		
	
	
	gtk_box_pack_start(GTK_BOX(vbox), zoomIn, FALSE, FALSE, 1);		
	gtk_box_pack_start(GTK_BOX(vbox), zoomOut, FALSE, FALSE, 1);		
	gtk_box_pack_start(GTK_BOX(vbox), zoomHome, FALSE, FALSE, 1);			
	gtk_box_pack_start(GTK_BOX(vbox), save, FALSE, FALSE, 1);				
	gtk_box_pack_start(GTK_BOX(vbox), val1, FALSE, FALSE, 1);			

	gtk_box_pack_start(GTK_BOX(vbox), tog, FALSE, FALSE, 1);			
	gtk_box_pack_start(GTK_BOX(vbox), smooth, FALSE, FALSE, 1);			
	
	gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, FALSE, 1);	
	gtk_container_add (GTK_CONTAINER (window), hbox);

	g_signal_connect (window, "destroy",
			G_CALLBACK (gtk_main_quit), NULL);

	gtk_widget_show_all (window);

	
	gtk_main ();
}

