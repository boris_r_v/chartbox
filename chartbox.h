#ifndef __chartboxmm__
#define __chartboxmm__

#include "gtkchartbox.h"
#include "gtkchartbox_graph.h"
#include "gtkchartbox_ruler.h"
#include "gtkchartbox_lines.h"
#include "gtkchartbox_points.h"
#include "gtkchartbox_bars.h"
#include "gtkchartbox_marker.h"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#define RET_OK 1
#define RET_ERROR 0
#define DEFAULT_LIMITS_VISIBLE FALSE

extern void chartbox_line_lineShowHide(GtkWidget* _w , gpointer _data);
extern void chartbox_line_limitsShowHide(GtkWidget* _w , gpointer _data);
extern void chartbox_line_lineSmooth(GtkWidget* _w , gpointer _data);
extern void chartbox_line_eventBoxPressButton( GtkWidget *widget, GdkEvent *event, gpointer data );
extern void chartbox_line_menu_selectLineColor(GtkMenuItem* _mi, gpointer _p);
extern void chartbox_line_menu_hideLine (GtkCheckMenuItem *checkmenuitem, gpointer data );
extern void chartbox_line_menu_smoothLine(GtkCheckMenuItem *checkmenuitem, gpointer data );
extern void chartbox_line_menu_hideLimitsLines(GtkCheckMenuItem *checkmenuitem, gpointer data );
namespace chart
{
    typedef std::vector <unsigned long> LV;
    typedef std::vector <float> FV;
    class chartbox;
    	
    class chartbox_line
    {
	    std::string title, color_str, mark_title;
	    const chartbox& chbox;
	    guint line_weigth;

	    GtkChartBoxGraph* line;
	    GtkWidget* frame;
	    GtkWidget* edit;
	    GtkWidget* smooth;
	    GtkWidget* limits;
	    GtkWidget* show_hide;
	    GtkWidget* label;
	    GtkWidget* event_box;
	    
	    unsigned len;
	    guint* X;
	    gfloat* Y;
	    
	    unsigned markers_len;
	    gfloat* Ym;
	    guint Xm;
	    
	    GdkColor color;
	    std::vector <GtkChartBoxMarker*> markers_line;

	    void make_box( bool limit );    
	    void make_line( bool limit );
	        
//	    void line_show_hide ( gboolean );
//	    void smooth_line ( gboolean );
//	    void limits_show_hide ( gboolean );
	    void event_box_press_button ( GtkWidget *widget, GdkEvent *event );
	    friend void ::chartbox_line_lineShowHide(GtkWidget* _w , gpointer _data);
	    friend void ::chartbox_line_limitsShowHide(GtkWidget* _w , gpointer _data);
	    friend void ::chartbox_line_lineSmooth(GtkWidget* _w , gpointer _data);
	    friend void ::chartbox_line_eventBoxPressButton(GtkWidget *widget, GdkEvent *event, gpointer data );
	    friend void ::chartbox_line_menu_selectLineColor(GtkMenuItem* _mi, gpointer data );
	    friend void ::chartbox_line_menu_hideLine(GtkCheckMenuItem *checkmenuitem, gpointer data );
	    friend void ::chartbox_line_menu_smoothLine(GtkCheckMenuItem *checkmenuitem, gpointer data );
	    friend void ::chartbox_line_menu_hideLimitsLines(GtkCheckMenuItem *checkmenuitem, gpointer data );
	    void select_line_color();
	    void make_control_menu();

	public:
	    void line_show_hide ( gboolean );
	    void set_ma_factor ( guint );
	    guint get_ma_factor ( );
	    void smooth_line ( gboolean );
	    void limits_show_hide ( gboolean );
	    /*Линиия графика без пороговых значений, использовать когда НЕ известны данные которые надо отрисовать, рисуем текущие измернеия.
	    * params:
	    * @ chartbox - ссылка на окно графиков
	    * @ _title - название  графика
	    * @ _color - цвет линии в формате RGB (#rrggbb)
	    * @ _line_weigth - толщина линии(необязательный параметр)
	    */
	    chartbox_line( const chartbox&, const std::string& _title, const std::string& _color, unsigned _line_weigth=2 );
	    /*Линиия графика с пороговыми значениями, использовать когда НЕ известны данные которые надо отрисовать, рисуем текущие измернеия.
	    * params:
	    * @ chartbox - ссылка на окно графиков
	    * @ _markers - данные по оси ординат для горизонтальной линии порогового значения
	    * @ _title - название  графика
	    * @ _color - цвет линии в формате RGB (#rrggbb)
	    * @ _mt - подпись линии пороговых значений, например Вольт, Ампер, вобщем размерность данных
	    * @ _line_weigth - толщина линии(необязательный параметр)
	    */
	    chartbox_line( const chartbox&, const FV& _markers, const std::string& _title, const std::string& _color, const std::string& _mt, unsigned _line_weigth=2 );
	    /*Линиия графика без пороговых значений, использовать когда известны данные которые надо отрисовать, например для чтения протоколов
	    * params:
	    * @ chartbox - ссылка на окно графиков
	    * @ _dataX - данные по оси абсцисс
	    * @ _dataY - данные по оси ординат
	    * @ _title - название  графика
	    * @ _color - цвет линии в формате RGB (#rrggbb)
	    * @ _line_weigth - толщина линии(необязательный параметр)
	    */
	    chartbox_line( const chartbox&, const LV& _dataX, const FV& _dataY, const std::string& _title, const std::string& _color, unsigned _line_weigth=2 );
	    /*Линиия графика с пороговыми значениями, использовать когда известны данные которые надо отрисовать, например для чтения протоколов
	    * params:
	    * @ chartbox - ссылка на окно графиков
	    * @ _dataX - данные по оси абсцисс
	    * @ _dataY - данные по оси ординат
	    * @ _markers - данные по оси ординат для горизонтальной линии порогового значения
	    * @ _title - название  графика
	    * @ _color - цвет линии в формате RGB (#rrggbb)
	    * @ _mt - подпись линии пороговых значений, например Вольт, Ампер, вобщем размерность данных
	    * @ _line_weigth - толщина линии(необязательный параметр)
	    */
	    chartbox_line( const chartbox&, const LV& _dataX, const FV& _dataY, const FV& _markers, const std::string& _title, const std::string& _color, const std::string& _mt, unsigned _line_weigth=2 );
	    chartbox_line( const chartbox_line& );
	    virtual ~chartbox_line( );
	    chartbox_line& operator= ( const chartbox_line& ); 
	    
	    /*Внешние методы*/
	    GtkWidget* get_main_box() const;
	    GtkChartBoxGraph* get_line() const;
	    /*Возвращает название графика*/
	    const std::string& get_title() const;
	    /*Подвязывает синал графика, "текущая величина"*/
	    void connect_signals();
	    /*Очищает массивы данных графика*/
	    void clear();
	    /*Удалает массивы данных когда разрушен будет chartbox*/
	    void destroy() const;
            /*Обновляет массив данных графика, добавляет новую точку*/
	    bool add_one_point( unsigned long _x, float _y );
            /*Обновляет массив данных графика, добавляет новый массива данных*/
	    bool add_points( unsigned long* _x, float* _y, unsigned size );
	    /**
	    * Скрывает кнопку сглаживания графика, default - button shown
	    * 	params:
	    *   true - show, default
	    *   false - hide
	    */
	    void smooth_button_set_visible ( gboolean );
	    /**
	    * Устанавливает цвет графика и надписи
	    */
	    void set_color( const GdkColor& );
	    bool have_markers();
	    const std::vector<GtkChartBoxMarker*> get_markers();
    };
    class chartbox
    {
	    GtkWidget* main_box;   	// Контейнер куда запихнем все три панели, левую центральную и правую
	    GtkWidget* left_box;   	// Контейнер куда будут класться окошки текущих значений
	    GtkWidget* central_box; 	// Контейнер-таблица в которой создан GtkDataBox	    
	    GtkWidget* right_box; 	// Контейнер куда сунутся кнопки управления
	    
	    GtkWidget* graph_box; 	// Контейнер рисования графиков
	    GtkWidget* self_box;  	// Контейнер рисования графика занятия освобождения своей РЦ
	    GtkWidget* sibl_box;  	// Контейнер рисования графика занятия освобождения соседней РЦ
	    
	    bool redraw_state;		// Флаг разрешения(true)/запрета(false) - перерисовки графика

	    void make_buttons( bool need_redraw_button );
	    void make_simple( bool is_time_x_ruller );
	    void make_vigil( const std::string& _self_tip, const std::string& _sibl_tip );
	    
	    std::map<std::string, chartbox_line> _in_lines;
	    
	    /*Перерисовывает окно графика, для реакции на нажатие кнопок "Скрыть" "Сгладить и т.д."*/
	    void self_update() const;
	    
        public:
    	    chartbox();
    	    chartbox( const chart::chartbox& );
    	    virtual ~chartbox( );
    	    chartbox& operator=(const chart::chartbox&);
    	    
    	    /**
    	    * Создает одиночное окно с графиками, без кнопки запрета перерисовки, и с осью Х созданной как ось времени
    	    * Если необязательный параметр будет установлен в false - то ось Х создасться как ось чисел 
    	    * Отрицательные значения по оси Y -запрещены, визуально график уйдет вниз, за границу видимой обсласти
    	    */
    	    void init_simple( bool is_time_x_ruller = true  );
    	    /**
    	    * Создает кно с графиками для дежурного режима, ось временная ось
    	    * Отрицательные значения по оси Y -запрещены, визуально график уйдет вниз, за границу видимой обсласти
    	    */
    	    void init_vigil( const std::string& _self_tip, const std::string& _sibl_tip );
    	    /**
    	    * Создает одиночное окно с графиками, без кнопки запрета перерисовки, и с осью Х созданной как ось времени
    	    * Если необязательный параметр будет установлен в false - то ось Х создасться как ось чисел 
    	    * Отрицательные значения по оси Y -запрещены, визуально график уйдет вниз, за границу видимой обсласти
    	    */
    	    void init_with_stop_update_button( bool is_time_x_ruller = true );
	    /**
	    * Возращает главный контейнер для паковки
	    */
	    GtkWidget* get_main_box();
	    /**
	    * Перерисовывает окно графика
	    */
	    void update() const;
	    /**
	    * Проверяет есть ли график с указанным именем с списке графиков
	    * Вренет true - если граифк уже есть в списке, false - если нет
	    */
	    bool has_line( const std::string& _name );
	    /**
	    * Добавляет график величины
	    */
	    void add_line( const chartbox_line& );
	    std::map<std::string, chartbox_line>& get( );
	    /**
	    * Добавляет график занятия освобождения своей РЦ для дежурного режима, если проинициализировали объект через init_vigil
	    */
	    void add_self_line( const chartbox_line& );
	    /**
	    * Добавляет график занятия освобождения соседней РЦ для дежурного режима, если проинициализировали объект через init_vigil
	    */
	    void add_sibl_line( const chartbox_line& );
	    /**
	    * Добавляет новую точку измерения в график графике
	    * params:
	    * @ _title - имя графика, указаное при создании
	    * @ _x - точка по оси абсцисс
	    * @ _y - точка по оси ординат
	    * return value
	    * @ true (1) - add successfull
	    * @ else (0) - add fault
	    */
	    bool add_one_point_to_line( const std::string& _title, unsigned long _x, float _y );
	    
	    /**
	    * Добавляет массив новых точек на график
	    * params:
	    * @ _title - имя графика, указаное при создании
	    * @ *_x - указатель на началало массива точек по оси абсцисс
	    * @ *_y - указатель на началало массива точек по оси ординат
	    * @ _size - длинна добавляемых массивов
	    * return value
	    * @ true (1) - add successfull
	    * @ else (0) - add fault
	    */
	    bool add_points_to_line( const std::string& _title, unsigned long* _x, float* _y, unsigned _size );
	    /**
	    * То эже что и предудыщий метод, но еще и вызывает перерисовку графика после добавления
	    */
	    bool add_one_point_to_line_update_chart( const std::string& _title, unsigned long _x, float _y );
	    /**
	    * Устанавливает флаг разрешения/запрета перерисовки
	    */
	    void set_redraw_state( bool );
	    /**
	    * Устанавливает/считывает значение флага разрешения отрицательных значений на графике
	    * Может быть вызваны в любое время, но после исполнения инициализации объекта. (after invoke init_????)
	    */
	    void set_negative_value_flag( bool );
	    bool get_negative_value_flag( );
    	    /**
    	    * Удалает данные графиков, но не сами графики, после этот их надо снова набить данными
    	    */
	    void clear();
	    /**
	    * Скрывает кнопки сглаживания графиков, default - button shown
	    * 	params:
	    *   true - show, default
	    *   false - hide
	    */
	    void lines_smooth_buttons_set_visible ( bool );
	    /**
	    * Установить путь куда писать файлы
	    */
	    void set_store_path( std::string const& );
	    /**
	    * Установить текст в chartbox
	    * - текст будет всегда рисоваться в центре полотна и красным цветом
	    */
	    void set_label_text( std::string const& );

	    friend class chartbox_line;
    };

};


#endif //__chartboxmm__