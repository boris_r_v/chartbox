#ifndef __GTK_CHARTBOX_MARKER_H__
#define __GTK_CHARTBOX_MARKER_H__

#include "gtkchartbox.h"

G_BEGIN_DECLS
#define GTK_CHARTBOX_TYPE_MARKER		  (gtk_chartbox_marker_get_type ())
#define GTK_CHARTBOX_MARKER(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_CHARTBOX_TYPE_MARKER, GtkChartBoxMarker))
#define GTK_CHARTBOX_MARKER_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_CHARTBOX_TYPE_MARKER, GtkChartBoxMarkerClass))
#define GTK_IS_CHARTBOX_MARKER(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_CHARTBOX_TYPE_MARKER))
#define GTK_IS_CHARTBOX_MARKER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_CHARTBOX_TYPE_MARKER))
#define GTK_CHARTBOX_MARKER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_CHARTBOX_TYPE_MARKER, GtkChartBoxMarkerClass))

typedef struct _GtkChartBoxMarkerClass GtkChartBoxMarkerClass;
typedef struct _GtkChartBoxMarkerPrivate GtkChartBoxMarkerPrivate;

struct _GtkChartBoxMarker
   {
      /*< private >*/
      GObject parent;

      GtkChartBoxMarkerPrivate *priv;
   };

struct _GtkChartBoxMarkerClass
   {
      GObjectClass parent_class;

      /*
       * public virtual drawing function 
       */
	void (*draw) (GtkChartBoxMarker * marker, GtkChartBox * box);

   };
    GType gtk_chartbox_marker_get_type (void);
    GtkChartBoxMarker * gtk_chartbox_marker_new(guint * X, gfloat * Y, GdkColor * color, guint size, gchar *text);

    void gtk_chartbox_marker_set_hide (GtkChartBoxMarker * marker, gboolean hide);
    gboolean gtk_chartbox_marker_get_hide (GtkChartBoxMarker * marker);

    void gtk_chartbox_marker_set_color (GtkChartBoxMarker * marker, GdkColor * color);
    GdkColor *gtk_chartbox_marker_get_color (GtkChartBoxMarker * marker);

    void gtk_chartbox_marker_set_size (GtkChartBoxMarker * marker, guint size);
    guint gtk_chartbox_marker_get_size (GtkChartBoxMarker * marker);

    void gtk_chartbox_marker_set_zoom (GtkChartBoxMarker * marker, guint zoom);

    void gtk_chartbox_marker_set_Y (GtkChartBoxMarker * marker, gfloat *Y);    
    gfloat* gtk_chartbox_marker_get_Y (GtkChartBoxMarker * marker );

    void gtk_chartbox_marker_set_X (GtkChartBoxMarker * marker, guint *X);    
    guint* gtk_chartbox_marker_get_X (GtkChartBoxMarker * marker );

    void gtk_chartbox_marker_set_line (GtkChartBoxMarker * marker, gboolean line);    
    gboolean gtk_chartbox_marker_get_line (GtkChartBoxMarker * marker );

    void gtk_chartbox_marker_set_text (GtkChartBoxMarker * marker, gchar *text);    
    gchar* gtk_chartbox_marker_get_text (GtkChartBoxMarker * marker );


   /* This function is called by GtkChartBox */
    void gtk_chartbox_marker_draw (GtkChartBoxMarker * marker, GtkChartBox * box);
/**
* Маркеры доступны только когда ось Х - это ось времени, иначе юзайте gtkdatabox
* Компонент может быть как текстом на графике так и линией предельного зачения
* Текст:
* 	Указатать где поставить метку и указать какой текст, и указать что это не линия(это по умолчанию)
*	    X: gtk_chartbox_marker_set_X
*	    Y: gtk_chartbox_marker_set_Y
*       текст: gtk_chartbox_marker_set_text
*	
* Линия:
*	Все тоже самое, что и для текста, только можно не указывать X, и базтельно указать что это линия
	Линия: gtk_chartbox_marker_set_line
	Вначале предельной линии подписывается значение предела.
	строка формируется так: значение Y + значение text, т.е. в аттрибуте тект в этом случае храниться тип величины Вольты, Амперы, Хомячки и т.д.
	Максимальная длина всей строки 20 символов
*/

G_END_DECLS
#endif				/* __GTK_CHARTBOX_MARKER_H__ */
