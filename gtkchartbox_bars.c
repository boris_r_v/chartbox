#include "gtkchartbox_bars.h"

static void gtk_chartbox_bars_real_draw (GtkChartBoxGraph * bars, GtkChartBox* box );
static gint gtk_chartbox_bars_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result );

struct _GtkChartBoxBarsPrivate
{
   GdkPoint *data;
};

static gpointer parent_class = NULL;

static void
bars_finalize (GObject * object)
{
   GtkChartBoxBars *bars = GTK_CHARTBOX_BARS (object);

   g_free (bars->priv->data);
   g_free (bars->priv);

   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_chartbox_bars_class_init (gpointer g_class /*, gpointer g_class_data */ )
{
   GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
   GtkChartBoxGraphClass *graph_class = GTK_CHARTBOX_GRAPH_CLASS (g_class);
   GtkChartBoxBarsClass *klass = GTK_CHARTBOX_BARS_CLASS (g_class);

   parent_class = g_type_class_peek_parent (klass);

   gobject_class->finalize = bars_finalize;

   graph_class->draw = gtk_chartbox_bars_real_draw;
   graph_class->calculate_value = gtk_chartbox_bars_real_calculate_value;
}

static void
gtk_chartbox_bars_complete (GtkChartBoxBars * bars)
{
   bars->priv->data = (GdkPoint*) g_renew (GdkPoint, bars->priv->data, gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (bars)));

}

static void
gtk_chartbox_bars_instance_init (GTypeInstance * instance	/*,
								   gpointer g_class */ )
{
   GtkChartBoxBars *bars = GTK_CHARTBOX_BARS (instance);

   bars->priv = g_new0 (GtkChartBoxBarsPrivate, 1);
   bars->priv->data = NULL;
   g_signal_connect (bars, "notify::length",
		     G_CALLBACK (gtk_chartbox_bars_complete), NULL);
}

GType
gtk_chartbox_bars_get_type (void)
{
   static GType type = 0;

   if (type == 0)
   {
      static const GTypeInfo info = {
	 sizeof (GtkChartBoxBarsClass),
	 NULL,			/* base_init */
	 NULL,			/* base_finalize */
	 (GClassInitFunc) gtk_chartbox_bars_class_init,	/* class_init */
	 NULL,			/* class_finalize */
	 NULL,			/* class_data */
	 sizeof (GtkChartBoxBars),	/* instance_size */
	 0,			/* n_preallocs */
	 (GInstanceInitFunc) gtk_chartbox_bars_instance_init,	/* instance_init */
	 NULL,			/* value_table */
      };
      type = g_type_register_static (GTK_CHARTBOX_TYPE_GRAPH,
				     "GtkChartBoxBars", &info, 0);
   }

   return type;
}

GtkChartBoxGraph *
gtk_chartbox_bars_new (guint len, guint * X, gfloat * Y,
		       GdkColor * color, guint size)
{
   GtkChartBoxBars *bars;
   g_return_val_if_fail (X, NULL);
   g_return_val_if_fail (Y, NULL);
   g_return_val_if_fail ((len > 0), NULL);

   bars = g_object_new (GTK_CHARTBOX_TYPE_BARS,
			 "X-Values", X,
			 "Y-Values", Y,
			 "length", len, "color", color, "size", size, NULL);

   return GTK_CHARTBOX_GRAPH (bars);
}

static void
gtk_chartbox_bars_real_draw (GtkChartBoxGraph * graph, GtkChartBox * box){


   GtkWidget *widget;
   GtkChartBoxBars *bars = GTK_CHARTBOX_BARS (graph);
   GdkPoint *data;
   cairo_surface_t *surface;
   GdkColor *color;
   cairo_t *cr;
   guint i = 0;
   guint  *X;
   gfloat *Y;
   guint len;
   gint size = 0;
   g_return_if_fail (GTK_IS_CHARTBOX_BARS (bars));
   g_return_if_fail (GTK_IS_CHARTBOX (box));

   widget = GTK_WIDGET(box);
    
   surface = gtk_chartbox_get_backing_surface (box);
   cr = gtk_chartbox_get_backing_content (box);
   
   len = gtk_chartbox_graph_get_length (GTK_CHARTBOX_GRAPH (graph));
   color = gtk_chartbox_graph_get_color (GTK_CHARTBOX_GRAPH (graph));
   X = gtk_chartbox_graph_get_X (GTK_CHARTBOX_GRAPH (graph));
   Y = gtk_chartbox_graph_get_Y (GTK_CHARTBOX_GRAPH (graph));
   size = gtk_chartbox_graph_get_size (GTK_CHARTBOX_GRAPH (graph ));
   data = bars->priv->data;
    /*расчитываем значения координат точек */
    gtk_chartbox_values_to_pixels (box, len, X, Y, data);
    /*Рисуем график */
    cairo_set_line_width(cr, (double)size);
    gdk_cairo_set_source_color (cr, color);
   for (i = 0; i < len; i ++ ){
	cairo_move_to(cr, 0, data[i].y );
	cairo_line_to(cr, data[i].x, data[i].y );
   }
    cairo_stroke(cr);
   return;
}
static gint
gtk_chartbox_bars_real_calculate_value (GtkChartBoxGraph * graph, guint *time, gfloat *result ){
   g_return_val_if_fail (graph, -1);
   g_return_val_if_fail (time, -1);

    gint i;
    guint len = gtk_chartbox_graph_get_length (graph);
    guint *X = gtk_chartbox_graph_get_X (graph);
    gfloat *Y = gtk_chartbox_graph_get_Y (graph);
    for(i=1; i < len; i++)
	if ( (*time == X[i]) ){
	    *result = Y[i];
	    continue;
	}

    return 0;
}

