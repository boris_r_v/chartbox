#!/bin/bash

PRG=./main
OPT="--leak-resolution=med   --show-reachable=yes  "
OPTK="-v --tool=memcheck --show-reachable=yes --leak-resolution=high --num-callers=20 --track-origins=yes "

export GLIBCPP_FORCE_NEW=1 
export GLIBCXX_FORCE_NEW=1 
/usr/bin/valgrind --leak-check=full  $PRG $OPT --log-file=leak.log

